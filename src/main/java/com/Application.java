
package com;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
//@EnableAutoConfiguration(exclude={DataSourceAutoConfiguration.class})
//@EnableScheduling
public class Application {

    public static void main(String[] args) {
        SpringApplication.run(Application.class);
    }



    @Bean
    String lookup(wsdlClient wsdlClient) {
        String response = wsdlClient.getQuote();
        System.err.println(response);
        return response;
    }

//	@Bean
//	String lookup(FileCreator FileCreator) {
//		String response = FileCreator.getQuote();
//		System.err.println(response);
//		return response;
//	}

//	@Bean
//	String lookup(AttacMentRecovery attacMentRecovery) {
//		String response = attacMentRecovery.getQuote();
//		System.err.println(response);
//		return response;
//	}
}
