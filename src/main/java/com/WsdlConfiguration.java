
package com;

import com.longbridge.formmfetcher.controllers.HashGenerator;
import org.apache.tomcat.jdbc.pool.DataSource;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;

import java.io.IOException;
import java.security.NoSuchAlgorithmException;

@Configuration
public class WsdlConfiguration {

	@Value("${spring.datasource.driverClassName}")
	private String databaseDriverClassName;

	@Value("${spring.datasource.url}")
	private String datasourceUrl;

	@Value("${spring.datasource.username}")
	private String databaseUsername;
	@Value("${spring.datasource.password}")
	private String databasePassword;

	@Bean
	public Jaxb2Marshaller marshaller() {
		Jaxb2Marshaller marshaller = new Jaxb2Marshaller();
		marshaller.setContextPath("com.wsdl");
		return marshaller;
	}



	@Bean
	public wsdlClient quoteClient(Jaxb2Marshaller marshaller) {
		wsdlClient client = new wsdlClient();
		client.setDefaultUri("https://app.ncs.gov.ng/axis2/services/FormMService");
		client.setMarshaller(marshaller);
		client.setUnmarshaller(marshaller);
		return client;
	}

	@Bean
	public DataSource datasource() throws IOException {
		org.apache.tomcat.jdbc.pool.DataSource ds = new org.apache.tomcat.jdbc.pool.DataSource();
		ds.setDriverClassName(databaseDriverClassName);
		ds.setUrl(datasourceUrl);
		ds.setUsername(databaseUsername);
		try {
			ds.setPassword(new HashGenerator().getHash(databasePassword));
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}

		return ds;
	}

//	@Bean
//	public FileCreator quoteClient(Jaxb2Marshaller marshaller) {
//		FileCreator client = new FileCreator();
//		client.setMarshaller(marshaller);
//		client.setUnmarshaller(marshaller);
//		return client;
//	}

//	@Bean
//	public AttacMentRecovery quoteClient(Jaxb2Marshaller marshaller) {
//		AttacMentRecovery client = new AttacMentRecovery();
//		client.setDefaultUri("https://app.ncs.gov.ng/axis2/services/FormMService");
//		client.setMarshaller(marshaller);
//		client.setUnmarshaller(marshaller);
//		return client;
//	}
}
