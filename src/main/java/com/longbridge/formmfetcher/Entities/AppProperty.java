package com.longbridge.formmfetcher.Entities;

import java.sql.Timestamp;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "app_property", schema = "custom", catalog = "")
public class AppProperty {
    private long id;
    private String propKey;
    private String propValue;
    private Timestamp lastPersistDate;
    private Timestamp firstPersistDate;
    private String excludeAttchmnt;
    private String bankCode;
    private String bankDesc;
    private String status;
    private String lastResponse;
    private String docGenStatus;
    private int formFetchInterval;


    @javax.persistence.Id
    @javax.persistence.Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "prop_key", nullable = true)
    public String getPropKey() {
        return propKey;
    }

    public void setPropKey(String propKey) {
        this.propKey = propKey;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "prop_value", nullable = true)
    public String getPropValue() {
        return propValue;
    }

    public void setPropValue(String propValue) {
        this.propValue = propValue;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "last_persisted_date", nullable = true)
    public Timestamp getLastPersistDate() {
        return lastPersistDate;
    }

    public void setLastPersistDate(Timestamp lastPersistDate) {
        this.lastPersistDate = lastPersistDate;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "bank_code", nullable = false)
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "bank_desc", nullable = false)
    public String getBankDesc() {
        return bankDesc;
    }

    public void setBankDesc(String bankDesc) {
        this.bankDesc = bankDesc;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "first_persisted_date", nullable = false)
    public Timestamp getFirstPersistDate() {
        return firstPersistDate;
    }

    public void setFirstPersistDate(Timestamp firstPersistDate) {
        this.firstPersistDate = firstPersistDate;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "status", nullable = false)
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "last_response", nullable = true)
    public String getLastResponse() {
        return lastResponse;
    }

    public void setLastResponse(String lastResponse) {
        this.lastResponse = lastResponse;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "Doc_Gen_Status", nullable = true)
    public String getDocGenStatus() {
        return docGenStatus;
    }

    public void setDocGenStatus(String docGenStatus) {
        this.docGenStatus = docGenStatus;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "EXCLUDE_ATTCHMNT", nullable = true,length=10)
    public String getExcludeAttchmnt() {
        return excludeAttchmnt;
    }

    public void setExcludeAttchmnt(String excludeAttchmnt) {
        this.excludeAttchmnt = excludeAttchmnt;
    }



    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORM_FETCH_INTERVAL", nullable = true)
    public int getFormFetchInterval() {
        return formFetchInterval;
    }
    public void setFormFetchInterval(int formFetchInterval) {
        this.formFetchInterval = formFetchInterval;
    }


    @Override
    public String toString() {
        return "AppProperty{" +
                "id=" + id +
                ", propKey='" + propKey + '\'' +
                ", propValue='" + propValue + '\'' +
                ", lastPersistDate=" + lastPersistDate +
                ", firstPersistDate=" + firstPersistDate +
                ", excludeAttchmnt='" + excludeAttchmnt + '\'' +
                ", bankCode='" + bankCode + '\'' +
                ", bankDesc='" + bankDesc + '\'' +
                ", status='" + status + '\'' +
                ", lastResponse='" + lastResponse + '\'' +
                ", docGenStatus='" + docGenStatus + '\'' +
                ", formFetchInterval=" + formFetchInterval +
                '}';
    }
}
