package com.longbridge.formmfetcher.Entities;

import java.sql.Timestamp;
import javax.persistence.Id;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@Entity
@Table(name = "FORM_M_MASTER_AUDIT_TBL", schema = "custom", catalog = "")
public class FinFormMMasterAudit {

    private long id;
    private String formMNum;
    private String formMType;
    private String bankID;
    private String entityCrFlg;
    private String delFlg;
    private String forexFlg;
    private Timestamp expiryDate;
    private String applNum;
    private String formPrefix;
    private String year;
    private String bankNumbering;
    private String adBankCode;
    private String adBankBranch;
    private String infDate;
    private String infUser;
    private String infOpr;
    private String infMsg;
    private long tsCnt;

    @Basic
    @Column(name = "FormM_NUM", nullable = true)
    public String getFormMNum() {
        return formMNum;
    }

    public void setFormMNum(String formMNum) {
        this.formMNum = formMNum;
    }

    @Id
    @Basic
    @Column(name = "id", nullable = true)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }
    @Basic
    @Column(name = "ts_cnt", nullable = true)
    public long getTsCnt() {
        return tsCnt;
    }

    public void setTsCnt(long tsCnt) {
        this.tsCnt = tsCnt;
    }

    @Basic
    @Column(name = "FORMM_TYPE", nullable = true)
    public String getFormMType() {
        return formMType;
    }

    public void setFormMType(String formMType) {
        this.formMType = formMType;
    }
    @Basic
    @Column(name = "BANK_ID", nullable = true)
    public String getBankID() {
        return bankID;
    }

    public void setBankID(String bankID) {
        this.bankID = bankID;
    }

    @Basic
    @Column(name = "ENTITY_CRE_FLG", nullable = true)
    public String getEntityCrFlg() {
        return entityCrFlg;
    }

    public void setEntityCrFlg(String entityCrFlg) {
        this.entityCrFlg = entityCrFlg;
    }

    @Basic
    @Column(name = "DEL_FLG", nullable = true)
    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    @Basic
    @Column(name = "FOREX_FLG", nullable = true)
    public String getForexFlg() {
        return forexFlg;
    }

    public void setForexFlg(String forexFlg) {
        this.forexFlg = forexFlg;
    }

    @Basic
    @Column(name = "EXP_DATE", nullable = true)
    public Timestamp getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Timestamp expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Basic
    @Column(name = "APP_NUM", nullable = true)
    public String getApplNum() {
        return applNum;
    }

    public void setApplNum(String applNum) {
        this.applNum = applNum;
    }

    @Basic
    @Column(name = "FORM_REFIX", nullable = true)
    public String getFormPrefix() {
        return formPrefix;
    }

    public void setFormPrefix(String formPrefix) {
        this.formPrefix = formPrefix;
    }

    @Basic
    @Column(name = "YEAR", nullable = true)
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Basic
    @Column(name = "BANK_NUMB", nullable = true)
    public String getBankNumbering() {
        return bankNumbering;
    }

    public void setBankNumbering(String bankNumbering) {
        this.bankNumbering = bankNumbering;
    }

    @Basic
    @Column(name = "AD_BANK_CODE", nullable = true)
    public String getAdBankCode() {
        return adBankCode;
    }

    public void setAdBankCode(String adBankCode) {
        this.adBankCode = adBankCode;
    }

    @Basic
    @Column(name = "AD_BANK_BRANCH", nullable = true)
    public String getAdBankBranch() {
        return adBankBranch;
    }

    public void setAdBankBranch(String adBankBranch) {
        this.adBankBranch = adBankBranch;
    }

    @Basic
    @Column(name = "INF_DATE", nullable = true)
    public String getInfDate() {
        return infDate;
    }

    public void setInfDate(String infDate) {
        this.infDate = infDate;
    }

    @Basic
    @Column(name = "INF_USER", nullable = true)
    public String getInfUser() {
        return infUser;
    }

    public void setInfUser(String infUser) {
        this.infUser = infUser;
    }

    @Basic
    @Column(name = "INF_OPR", nullable = true)
    public String getInfOpr() {
        return infOpr;
    }

    public void setInfOpr(String infOpr) {
        this.infOpr = infOpr;
    }

    @Basic
    @Column(name = "INF_MSG", nullable = true, length = 4000)
    public String getInfMsg() {
        return infMsg;
    }

    public void setInfMsg(String infMsg) {
        this.infMsg = infMsg;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinFormMMasterAudit that = (FinFormMMasterAudit) o;

        if (forexFlg != null ? !forexFlg.equals(that.forexFlg) : that.forexFlg != null) return false;
        if (expiryDate != null ? !expiryDate.equals(that.expiryDate) : that.expiryDate != null) return false;
        if (applNum != null ? !applNum.equals(that.applNum) : that.applNum != null) return false;
        if (formPrefix != null ? !formPrefix.equals(that.formPrefix) : that.formPrefix != null) return false;
        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        if (bankNumbering != null ? !bankNumbering.equals(that.bankNumbering) : that.bankNumbering != null)
            return false;
        if (adBankCode != null ? !adBankCode.equals(that.adBankCode) : that.adBankCode != null) return false;
        if (adBankBranch != null ? !adBankBranch.equals(that.adBankBranch) : that.adBankBranch != null) return false;
        if (infDate != null ? !infDate.equals(that.infDate) : that.infDate != null) return false;
        if (infUser != null ? !infUser.equals(that.infUser) : that.infUser != null) return false;
        if (infOpr != null ? !infOpr.equals(that.infOpr) : that.infOpr != null) return false;
        return infMsg != null ? infMsg.equals(that.infMsg) : that.infMsg == null;
    }


    @Override
    public int hashCode() {
        int result = forexFlg != null ? forexFlg.hashCode() : 0;
        result = 31 * result + (expiryDate != null ? expiryDate.hashCode() : 0);
        result = 31 * result + (applNum != null ? applNum.hashCode() : 0);
        result = 31 * result + (formPrefix != null ? formPrefix.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (bankNumbering != null ? bankNumbering.hashCode() : 0);
        result = 31 * result + (adBankCode != null ? adBankCode.hashCode() : 0);
        result = 31 * result + (adBankBranch != null ? adBankBranch.hashCode() : 0);
        result = 31 * result + (infDate != null ? infDate.hashCode() : 0);
        result = 31 * result + (infUser != null ? infUser.hashCode() : 0);
        result = 31 * result + (infOpr != null ? infOpr.hashCode() : 0);
        result = 31 * result + (infMsg != null ? infMsg.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FinFormMMasterAudit{" +
                "id=" + id +
                ", formMNum='" + formMNum + '\'' +
                ", formMType='" + formMType + '\'' +
                ", bankID='" + bankID + '\'' +
                ", entityCrFlg='" + entityCrFlg + '\'' +
                ", delFlg='" + delFlg + '\'' +
                ", forexFlg='" + forexFlg + '\'' +
                ", expiryDate=" + expiryDate +
                ", applNum='" + applNum + '\'' +
                ", formPrefix='" + formPrefix + '\'' +
                ", year='" + year + '\'' +
                ", bankNumbering='" + bankNumbering + '\'' +
                ", adBankCode='" + adBankCode + '\'' +
                ", adBankBranch='" + adBankBranch + '\'' +
                ", infDate='" + infDate + '\'' +
                ", infUser='" + infUser + '\'' +
                ", infOpr='" + infOpr + '\'' +
                ", infMsg='" + infMsg + '\'' +
                ", tsCnt=" + tsCnt +
                '}';
    }




}
