package com.longbridge.formmfetcher.Entities.FinacleEntities;

import javax.persistence.Id;
import javax.persistence.UniqueConstraint;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@Entity
@Table(name = "ng_tf_form_m_goods_table", schema = "TBAADM",
    uniqueConstraints={@UniqueConstraint(columnNames={"FORM_TYPE", "FORM_NUM", "SR_NUM", "BANK_ID"})}
)
public class FinFormMItms {
    private String formType;
    private String formNum;
    private String srNum;
    private String bankId;
    private String entityCreFlg;
    private String delFlg;
    private String hsCode;
    private String secPurCode;
    private int noOfPackage;
    private Double fobValue;
    private String descGoods1;
    private String descGoods2;
    private Double freightCharges;
    private String fcyCrncyCode;
    private String rcreUserId;
    private Date rcreTime;
    private String lchgUserId;
    private Date lchgTime;
    private int tsCnt;

    @Basic
    @Column(name = "FORM_TYPE", nullable = true)
    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    @Basic
    @Column(name = "FORM_NUM", nullable = true)
    public String getFormNum() {
        return formNum;
    }

    public void setFormNum(String formNum) {
        this.formNum = formNum;
    }

    @Basic
    @Column(name = "SR_NUM", nullable = true)
    public String getSrNum() {
        return srNum;
    }

    public void setSrNum(String srNum) {
        this.srNum = srNum;
    }

    @Basic
    @Column(name = "BANK_ID", nullable = true)
    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @Basic
    @Column(name = "ENTITY_CRE_FLG", nullable = true)
    public String getEntityCreFlg() {
        return entityCreFlg;
    }

    public void setEntityCreFlg(String entityCreFlg) {
        this.entityCreFlg = entityCreFlg;
    }

    @Basic
    @Column(name = "DEL_FLG", nullable = true)
    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    @Basic
    @Column(name = "HS_CODE", nullable = true)
    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    @Basic
    @Column(name = "SEC_PUR_CODE", nullable = true)
    public String getSecPurCode() {
        return secPurCode;
    }

    public void setSecPurCode(String secPurCode) {
        this.secPurCode = secPurCode;
    }

    @Basic
    @Column(name = "NO_OF_PACKAGE", nullable = true)
    public int getNoOfPackage() {
        return noOfPackage;
    }

    public void setNoOfPackage(int noOfPackage) {
        this.noOfPackage = noOfPackage;
    }

    @Basic
    @Column(name = "FOB_VALUE", nullable = true)
    public Double getFobValue() {
        return fobValue;
    }

    public void setFobValue(Double fobValue) {
        this.fobValue = fobValue;
    }

    @Basic
    @Column(name = "DESC_GOODS1", nullable = true, length = 4000)
    public String getDescGoods1() {
        return descGoods1;
    }

    public void setDescGoods1(String descGoods1) {
        this.descGoods1 = descGoods1;
    }

    @Basic
    @Column(name = "DESC_GOODS2", nullable = true, length = 4000)
    public String getDescGoods2() {
        return descGoods2;
    }

    public void setDescGoods2(String descGoods2) {
        this.descGoods2 = descGoods2;
    }

    @Basic
    @Column(name = "FREIGHT_CHARGES", nullable = true)
    public Double getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCharges(Double freightCharges) {
        this.freightCharges = freightCharges;
    }

    @Basic
    @Column(name = "FCY_CRNCY_CODE", nullable = true)
    public String getFcyCrncyCode() {
        return fcyCrncyCode;
    }

    public void setFcyCrncyCode(String fcyCrncyCode) {
        this.fcyCrncyCode = fcyCrncyCode;
    }

    @Basic
    @Column(name = "RCRE_USER_ID", nullable = true)
    public String getRcreUserId() {
        return rcreUserId;
    }

    public void setRcreUserId(String rcreUserId) {
        this.rcreUserId = rcreUserId;
    }

    @Basic
    @Column(name = "RCRE_TIME", nullable = true)
    public Date getRcreTime() {
        return rcreTime;
    }

    public void setRcreTime(Date rcreTime) {
        this.rcreTime = rcreTime;
    }

    @Basic
    @Column(name = "LCHG_USER_ID", nullable = true)
    public String getLchgUserId() {
        return lchgUserId;
    }

    public void setLchgUserId(String lchgUserId) {
        this.lchgUserId = lchgUserId;
    }

    @Basic
    @Column(name = "LCHG_TIME", nullable = true)
    public Date getLchgTime() {
        return lchgTime;
    }

    public void setLchgTime(Date lchgTime) {
        this.lchgTime = lchgTime;
    }
    @Id
    @Basic
    @Column(name = "TS_CNT", nullable = true)
    public int getTsCnt() {
        return tsCnt;
    }

    @Override
    public String toString() {
        return "FinFormMItms{" +
                "formType='" + formType + '\'' +
                ", formNum='" + formNum + '\'' +
                ", srNum='" + srNum + '\'' +
                ", bankId='" + bankId + '\'' +
                ", entityCreFlg='" + entityCreFlg + '\'' +
                ", delFlg='" + delFlg + '\'' +
                ", hsCode='" + hsCode + '\'' +
                ", secPurCode='" + secPurCode + '\'' +
                ", noOfPackage=" + noOfPackage +
                ", fobValue=" + fobValue +
                ", descGoods1='" + descGoods1 + '\'' +
                ", descGoods2='" + descGoods2 + '\'' +
                ", freightCharges=" + freightCharges +
                ", fcyCrncyCode='" + fcyCrncyCode + '\'' +
                ", rcreUserId='" + rcreUserId + '\'' +
                ", rcreTime=" + rcreTime +
                ", lchgUserId='" + lchgUserId + '\'' +
                ", lchgTime=" + lchgTime +
                ", tsCnt=" + tsCnt +
                '}';
    }

    public void setTsCnt(int tsCnt) {
        this.tsCnt = tsCnt;
    }


}
