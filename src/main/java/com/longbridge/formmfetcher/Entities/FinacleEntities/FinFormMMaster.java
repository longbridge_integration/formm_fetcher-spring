package com.longbridge.formmfetcher.Entities.FinacleEntities;


import javax.persistence.Id;
import javax.persistence.UniqueConstraint;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.util.Date;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@Entity
@Table(name = "ng_tf_form_m_master_table", schema = "TBAADM",
        uniqueConstraints={@UniqueConstraint(columnNames={"FORM_NUM","FORM_TYPE", "BANK_ID"})})

public class FinFormMMaster {
    private String formType;
    private String formNum;
    private String bankId;
    private String entityCreFlg;
    private String delFlg;
    private String applNum;
    private String formPrefix;
    private String bankCode;
    private String year;
    private String branchCode;
    private String bankersName;
    private String bankersAddr1;
    private String bankersAddr2;
    private String bankersAddr3;
    private String bankersCity;
    private String bankersState;
    private String bankersCntry;
    private String bankersPin;
    private String purposeCode;
    private Date regisDate;
    private Date expiryDate;
    private Date approvalDate;
    private String statusCode;
    private String custId;
    private String acid;
    private String forexFlg;
    private String chrgAcid;
    private String appliFirstName;
    private String appliLastName;
    private String appliAddr1;
    private String appliAddr2;
    private String appliAddr3;
    private String appliCity;
    private String appliState;
    private String appliCntry;
    private String appliPin;
    private String appliPhoneNum;
    private String appliPassDet;
    private String chrgConFlg;
    private String chrgConAmt;
    private String chrgConCrncyCode;
    private String partyCode;
    private String beniFirstName;
    private String beniLastName;
    private String beniAddr1;
    private String beniAddr2;
    private String beniAddr3;
    private String beniCity;
    private String beniState;
    private String beniCntry;
    private String beniPin;
    private String beniPhoneNum;
    private String beniPassDet;
    private String paymentCode;
    private String transferCode;
    private String originCntryCode;
    private String supplyCntryCode;
    private String transportCode;
    private String portCode;
    private String designateBankCode;
    private String inspAgentCode;
    private String authFirstName;
    private String authLastName;
    private String authAppliFirstName;
    private String authAppliLastName;
    private String remarks;
    private String rarNumber;
    private String rarDate;
    private String generalDesc1;
    private Integer noOfItems;
    private String netWt;
    private String totalFobValue;
    private String totalFriCharges;
    private String totalAnciCharges;
    private String formmFcyAmt;
    private String fcyCrncyCode;
    private String invoiceNum;
    private Date invoiceDate;
    private String excgRateCode;
    private String formmRate;
    private Double formmNairaEqui;
    private String homeCrncyCode;
    private String firstTimeFlg;
    private String rcreUserId;
    private Date rcreTime;
    private String lchgUserId;
    private Date lchgTime;

    @Basic
    @Column(name = "FORM_TYPE", nullable = true)
    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }
    @Id
    @Basic
    @Column(name = "FORM_NUM", nullable = true)
    public String getFormNum() {
        return formNum;
    }

    public void setFormNum(String formNum) {
        this.formNum = formNum;
    }

    @Basic
    @Column(name = "BANK_ID", nullable = true)
    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @Basic
    @Column(name = "ENTITY_CRE_FLG", nullable = true)
    public String getEntityCreFlg() {
        return entityCreFlg;
    }

    public void setEntityCreFlg(String entityCreFlg) {
        this.entityCreFlg = entityCreFlg;
    }

    @Basic
    @Column(name = "DEL_FLG", nullable = true)
    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }
//

    @Basic
    @Column(name = "APPL_NUM", nullable = true)
    public String getApplNum() {
        return applNum;
    }

    public void setApplNum(String applNum) {
        this.applNum = applNum;
    }

    @Basic
    @Column(name = "FORM_PREFIX", nullable = true)
    public String getFormPrefix() {
        return formPrefix;
    }

    public void setFormPrefix(String formPrefix) {
        this.formPrefix = formPrefix;
    }

    @Basic
    @Column(name = "BANK_CODE", nullable = true)
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @Basic
    @Column(name = "YEAR", nullable = true)
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @Basic
    @Column(name = "BRANCH_CODE", nullable = true)
    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    @Basic
    @Column(name = "BANKERS_NAME", nullable = true)
    public String getBankersName() {
        return bankersName;
    }

    public void setBankersName(String bankersName) {
        this.bankersName = bankersName;
    }

    @Basic
    @Column(name = "BANKERS_ADDR1", nullable = true)
    public String getBankersAddr1() {
        return bankersAddr1;
    }

    public void setBankersAddr1(String bankersAddr1) {
        this.bankersAddr1 = bankersAddr1;
    }

    @Basic
    @Column(name = "BANKERS_ADDR2", nullable = true)
    public String getBankersAddr2() {
        return bankersAddr2;
    }

    public void setBankersAddr2(String bankersAddr2) {
        this.bankersAddr2 = bankersAddr2;
    }

    @Basic
    @Column(name = "BANKERS_ADDR3", nullable = true)
    public String getBankersAddr3() {
        return bankersAddr3;
    }

    public void setBankersAddr3(String bankersAddr3) {
        this.bankersAddr3 = bankersAddr3;
    }

    @Basic
    @Column(name = "BANKERS_CITY", nullable = true)
    public String getBankersCity() {
        return bankersCity;
    }

    public void setBankersCity(String bankersCity) {
        this.bankersCity = bankersCity;
    }

    @Basic
    @Column(name = "BANKERS_STATE", nullable = true)
    public String getBankersState() {
        return bankersState;
    }

    public void setBankersState(String bankersState) {
        this.bankersState = bankersState;
    }

    @Basic
    @Column(name = "BANKERS_CNTRY", nullable = true)
    public String getBankersCntry() {
        return bankersCntry;
    }

    public void setBankersCntry(String bankersCntry) {
        this.bankersCntry = bankersCntry;
    }

    @Basic
    @Column(name = "BANKERS_PIN", nullable = true)
    public String getBankersPin() {
        return bankersPin;
    }

    public void setBankersPin(String bankersPin) {
        this.bankersPin = bankersPin;
    }

    @Basic
    @Column(name = "PURPOSE_CODE", nullable = true)
    public String getPurposeCode() {
        return purposeCode;
    }

    public void setPurposeCode(String purposeCode) {
        this.purposeCode = purposeCode;
    }

    @Basic
    @Column(name = "REGIS_DATE", nullable = true)
    public Date getRegisDate() {
        return regisDate;
    }

    public void setRegisDate(Date regisDate) {
        this.regisDate = regisDate;
    }

    @Basic
    @Column(name = "EXPIRY_DATE", nullable = true)
    public Date getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(Date expiryDate) {
        this.expiryDate = expiryDate;
    }

    @Basic
    @Column(name = "APPROVAL_DATE", nullable = true)
    public Date getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Date approvalDate) {
        this.approvalDate = approvalDate;
    }

    @Basic
    @Column(name = "STATUS_CODE", nullable = true)
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @Basic
    @Column(name = "CUST_ID", nullable = true)
    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    @Basic
    @Column(name = "ACID", nullable = true)
    public String getAcid() {
        return acid;
    }

    public void setAcid(String acid) {
        this.acid = acid;
    }

    @Basic
    @Column(name = "FOREX_FLG", nullable = true)
    public String getForexFlg() {
        return forexFlg;
    }

    public void setForexFlg(String forexFlg) {
        this.forexFlg = forexFlg;
    }

    @Basic
    @Column(name = "CHRG_ACID", nullable = true)
    public String getChrgAcid() {
        return chrgAcid;
    }

    public void setChrgAcid(String chrgAcid) {
        this.chrgAcid = chrgAcid;
    }

    @Basic
    @Column(name = "APPLI_FIRST_NAME", nullable = true)
    public String getAppliFirstName() {
        return appliFirstName;
    }

    public void setAppliFirstName(String appliFirstName) {
        this.appliFirstName = appliFirstName;
    }

    @Basic
    @Column(name = "APPLI_LAST_NAME", nullable = true)
    public String getAppliLastName() {
        return appliLastName;
    }

    public void setAppliLastName(String appliLastName) {
        this.appliLastName = appliLastName;
    }

    @Basic
    @Column(name = "APPLI_ADDR1", nullable = true)
    public String getAppliAddr1() {
        return appliAddr1;
    }

    public void setAppliAddr1(String appliAddr1) {
        this.appliAddr1 = appliAddr1;
    }

    @Basic
    @Column(name = "APPLI_ADDR2", nullable = true)
    public String getAppliAddr2() {
        return appliAddr2;
    }

    public void setAppliAddr2(String appliAddr2) {
        this.appliAddr2 = appliAddr2;
    }

    @Basic
    @Column(name = "APPLI_ADDR3", nullable = true)
    public String getAppliAddr3() {
        return appliAddr3;
    }

    public void setAppliAddr3(String appliAddr3) {
        this.appliAddr3 = appliAddr3;
    }

    @Basic
    @Column(name = "APPLI_CITY", nullable = true)
    public String getAppliCity() {
        return appliCity;
    }

    public void setAppliCity(String appliCity) {
        this.appliCity = appliCity;
    }

    @Basic
    @Column(name = "APPLI_STATE", nullable = true)
    public String getAppliState() {
        return appliState;
    }

    public void setAppliState(String appliState) {
        this.appliState = appliState;
    }

    @Basic
    @Column(name = "APPLI_CNTRY", nullable = true)
    public String getAppliCntry() {
        return appliCntry;
    }

    public void setAppliCntry(String appliCntry) {
        this.appliCntry = appliCntry;
    }

    @Basic
    @Column(name = "APPLI_PIN", nullable = true)
    public String getAppliPin() {
        return appliPin;
    }

    public void setAppliPin(String appliPin) {
        this.appliPin = appliPin;
    }

    @Basic
    @Column(name = "APPLI_PHONE_NUM", nullable = true)
    public String getAppliPhoneNum() {
        return appliPhoneNum;
    }

    public void setAppliPhoneNum(String appliPhoneNum) {
        this.appliPhoneNum = appliPhoneNum;
    }

    @Basic
    @Column(name = "APPLI_PASS_DET", nullable = true)
    public String getAppliPassDet() {
        return appliPassDet;
    }

    public void setAppliPassDet(String appliPassDet) {
        this.appliPassDet = appliPassDet;
    }

    @Basic
    @Column(name = "CHRG_CON_FLG", nullable = true)
    public String getChrgConFlg() {
        return chrgConFlg;
    }

    public void setChrgConFlg(String chrgConFlg) {
        this.chrgConFlg = chrgConFlg;
    }

    @Basic
    @Column(name = "CHRG_CON_AMT", nullable = true)
    public String getChrgConAmt() {
        return chrgConAmt;
    }

    public void setChrgConAmt(String chrgConAmt) {
        this.chrgConAmt = chrgConAmt;
    }

    @Basic
    @Column(name = "CHRG_CON_CRNCY_CODE", nullable = true)
    public String getChrgConCrncyCode() {
        return chrgConCrncyCode;
    }

    public void setChrgConCrncyCode(String chrgConCrncyCode) {
        this.chrgConCrncyCode = chrgConCrncyCode;
    }

    @Basic
    @Column(name = "PARTY_CODE", nullable = true)
    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    @Basic
    @Column(name = "BENI_FIRST_NAME", nullable = true)
    public String getBeniFirstName() {
        return beniFirstName;
    }

    public void setBeniFirstName(String beniFirstName) {
        this.beniFirstName = beniFirstName;
    }

    @Basic
    @Column(name = "BENI_LAST_NAME", nullable = true)
    public String getBeniLastName() {
        return beniLastName;
    }

    public void setBeniLastName(String beniLastName) {
        this.beniLastName = beniLastName;
    }

    @Basic
    @Column(name = "BENI_ADDR1", nullable = true, length = 4000)
    public String getBeniAddr1() {
        return beniAddr1;
    }

    public void setBeniAddr1(String beniAddr1) {
        this.beniAddr1 = beniAddr1;
    }

    @Basic
    @Column(name = "BENI_ADDR2", nullable = true, length = 4000)
    public String getBeniAddr2() {
        return beniAddr2;
    }

    public void setBeniAddr2(String beniAddr2) {
        this.beniAddr2 = beniAddr2;
    }

    @Basic
    @Column(name = "BENI_ADDR3", nullable = true, length = 4000)
    public String getBeniAddr3() {
        return beniAddr3;
    }

    public void setBeniAddr3(String beniAddr3) {
        this.beniAddr3 = beniAddr3;
    }

    @Basic
    @Column(name = "BENI_CITY", nullable = true)
    public String getBeniCity() {
        return beniCity;
    }

    public void setBeniCity(String beniCity) {
        this.beniCity = beniCity;
    }

    @Basic
    @Column(name = "BENI_STATE", nullable = true)
    public String getBeniState() {
        return beniState;
    }

    public void setBeniState(String beniState) {
        this.beniState = beniState;
    }

    @Basic
    @Column(name = "BENI_CNTRY", nullable = true)
    public String getBeniCntry() {
        return beniCntry;
    }

    public void setBeniCntry(String beniCntry) {
        this.beniCntry = beniCntry;
    }

    @Basic
    @Column(name = "BENI_PIN", nullable = true)
    public String getBeniPin() {
        return beniPin;
    }

    public void setBeniPin(String beniPin) {
        this.beniPin = beniPin;
    }

    @Basic
    @Column(name = "BENI_PHONE_NUM", nullable = true)
    public String getBeniPhoneNum() {
        return beniPhoneNum;
    }

    public void setBeniPhoneNum(String beniPhoneNum) {
        this.beniPhoneNum = beniPhoneNum;
    }

    @Basic
    @Column(name = "BENI_PASS_DET", nullable = true)
    public String getBeniPassDet() {
        return beniPassDet;
    }

    public void setBeniPassDet(String beniPassDet) {
        this.beniPassDet = beniPassDet;
    }

    @Basic
    @Column(name = "PAYMENT_CODE", nullable = true)
    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    @Basic
    @Column(name = "TRANSFER_CODE", nullable = true)
    public String getTransferCode() {
        return transferCode;
    }

    public void setTransferCode(String transferCode) {
        this.transferCode = transferCode;
    }

    @Basic
    @Column(name = "ORIGIN_CNTRY_CODE", nullable = true)
    public String getOriginCntryCode() {
        return originCntryCode;
    }

    public void setOriginCntryCode(String originCntryCode) {
        this.originCntryCode = originCntryCode;
    }

    @Basic
    @Column(name = "SUPPLY_CNTRY_CODE", nullable = true)
    public String getSupplyCntryCode() {
        return supplyCntryCode;
    }

    public void setSupplyCntryCode(String supplyCntryCode) {
        this.supplyCntryCode = supplyCntryCode;
    }

    @Basic
    @Column(name = "TRANSPORT_CODE", nullable = true)
    public String getTransportCode() {
        return transportCode;
    }

    public void setTransportCode(String transportCode) {
        this.transportCode = transportCode;
    }

    @Basic
    @Column(name = "PORT_CODE", nullable = true)
    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    @Basic
    @Column(name = "DESIGNATE_BANK_CODE", nullable = true)
    public String getDesignateBankCode() {
        return designateBankCode;
    }

    public void setDesignateBankCode(String designateBankCode) {
        this.designateBankCode = designateBankCode;
    }

    @Basic
    @Column(name = "INSP_AGENT_CODE", nullable = true)
    public String getInspAgentCode() {
        return inspAgentCode;
    }

    public void setInspAgentCode(String inspAgentCode) {
        this.inspAgentCode = inspAgentCode;
    }

    @Basic
    @Column(name = "AUTH_FIRST_NAME", nullable = true)
    public String getAuthFirstName() {
        return authFirstName;
    }

    public void setAuthFirstName(String authFirstName) {
        this.authFirstName = authFirstName;
    }

    @Basic
    @Column(name = "AUTH_LAST_NAME", nullable = true)
    public String getAuthLastName() {
        return authLastName;
    }

    public void setAuthLastName(String authLastName) {
        this.authLastName = authLastName;
    }

    @Basic
    @Column(name = "AUTH_APPLI_FIRST_NAME", nullable = true)
    public String getAuthAppliFirstName() {
        return authAppliFirstName;
    }

    public void setAuthAppliFirstName(String authAppliFirstName) {
        this.authAppliFirstName = authAppliFirstName;
    }

    @Basic
    @Column(name = "AUTH_APPLI_LAST_NAME", nullable = true)
    public String getAuthAppliLastName() {
        return authAppliLastName;
    }

    public void setAuthAppliLastName(String authAppliLastName) {
        this.authAppliLastName = authAppliLastName;
    }

    @Basic
    @Column(name = "REMARKS", nullable = true)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @Basic
    @Column(name = "RAR_NUMBER", nullable = true)
    public String getRarNumber() {
        return rarNumber;
    }

    public void setRarNumber(String rarNumber) {
        this.rarNumber = rarNumber;
    }

    @Basic
    @Column(name = "RAR_DATE", nullable = true)
    public String getRarDate() {
        return rarDate;
    }

    public void setRarDate(String rarDate) {
        this.rarDate = rarDate;
    }

    @Basic
    @Column(name = "GENERAL_DESC1", nullable = true)
    public String getGeneralDesc1() {
        return generalDesc1;
    }

    public void setGeneralDesc1(String generalDesc1) {
        this.generalDesc1 = generalDesc1;
    }

    @Basic
    @Column(name = "NO_OF_ITEMS", nullable = true)
    public Integer getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(Integer noOfItems) {
        this.noOfItems = noOfItems;
    }

    @Basic
    @Column(name = "NET_WT", nullable = true)
    public String getNetWt() {
        return netWt;
    }

    public void setNetWt(String netWt) {
        this.netWt = netWt;
    }

    @Basic
    @Column(name = "TOTAL_FOB_VALUE", nullable = true)
    public String getTotalFobValue() {
        return totalFobValue;
    }

    public void setTotalFobValue(String totalFobValue) {
        this.totalFobValue = totalFobValue;
    }

    @Basic
    @Column(name = "TOTAL_FRI_CHARGES", nullable = true)
    public String getTotalFriCharges() {
        return totalFriCharges;
    }

    public void setTotalFriCharges(String totalFriCharges) {
        this.totalFriCharges = totalFriCharges;
    }

    @Basic
    @Column(name = "TOTAL_ANCI_CHARGES", nullable = true)
    public String getTotalAnciCharges() {
        return totalAnciCharges;
    }

    public void setTotalAnciCharges(String totalAnciCharges) {
        this.totalAnciCharges = totalAnciCharges;
    }

    @Basic
    @Column(name = "FORMM_FCY_AMT", nullable = true)
    public String getFormmFcyAmt() {
        return formmFcyAmt;
    }

    public void setFormmFcyAmt(String formmFcyAmt) {
        this.formmFcyAmt = formmFcyAmt;
    }

    @Basic
    @Column(name = "FCY_CRNCY_CODE", nullable = true)
    public String getFcyCrncyCode() {
        return fcyCrncyCode;
    }

    public void setFcyCrncyCode(String fcyCrncyCode) {
        this.fcyCrncyCode = fcyCrncyCode;
    }

    @Basic
    @Column(name = "INVOICE_NUM", nullable = true)
    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    @Basic
    @Column(name = "INVOICE_DATE", nullable = true)
    public Date getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Date invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @Basic
    @Column(name = "EXCG_RATE_CODE", nullable = true)
    public String getExcgRateCode() {
        return excgRateCode;
    }

    public void setExcgRateCode(String excgRateCode) {
        this.excgRateCode = excgRateCode;
    }

    @Basic
    @Column(name = "FORMM_RATE", nullable = true)
    public String getFormmRate() {
        return formmRate;
    }

    public void setFormmRate(String formmRate) {
        this.formmRate = formmRate;
    }

    @Basic
    @Column(name = "FORMM_NAIRA_EQUI", nullable = true, precision = 0)
    public Double getFormmNairaEqui() {
        return formmNairaEqui;
    }

    public void setFormmNairaEqui(Double formmNairaEqui) {
        this.formmNairaEqui = formmNairaEqui;
    }

    @Basic
    @Column(name = "HOME_CRNCY_CODE", nullable = true)
    public String getHomeCrncyCode() {
        return homeCrncyCode;
    }

    public void setHomeCrncyCode(String homeCrncyCode) {
        this.homeCrncyCode = homeCrncyCode;
    }

    @Basic
    @Column(name = "FIRST_TIME_FLG", nullable = true)
    public String getFirstTimeFlg() {
        return firstTimeFlg;
    }

    public void setFirstTimeFlg(String firstTimeFlg) {
        this.firstTimeFlg = firstTimeFlg;
    }

    @Basic
    @Column(name = "RCRE_USER_ID", nullable = true)
    public String getRcreUserId() {
        return rcreUserId;
    }

    public void setRcreUserId(String rcreUserId) {
        this.rcreUserId = rcreUserId;
    }

    @Basic
    @Column(name = "RCRE_TIME", nullable = true)
    public Date getRcreTime() {
        return rcreTime;
    }

    public void setRcreTime(Date rcreTime) {
        this.rcreTime = rcreTime;
    }

    @Basic
    @Column(name = "LCHG_USER_ID", nullable = true)
    public String getLchgUserId() {
        return lchgUserId;
    }

    public void setLchgUserId(String lchgUserId) {
        this.lchgUserId = lchgUserId;
    }

    @Basic
    @Column(name = "LCHG_TIME", nullable = true)
    public Date getLchgTime() {
        return lchgTime;
    }

    public void setLchgTime(Date lchgTime) {
        this.lchgTime = lchgTime;
    }

    @Override
    public String toString() {
        return "FinFormMMaster{" +
                "formType='" + formType + '\'' +
                ", formNum='" + formNum + '\'' +
                ", bankId='" + bankId + '\'' +
                ", entityCreFlg='" + entityCreFlg + '\'' +
                ", delFlg='" + delFlg + '\'' +
                ", applNum='" + applNum + '\'' +
                ", formPrefix='" + formPrefix + '\'' +
                ", bankCode='" + bankCode + '\'' +
                ", year='" + year + '\'' +
                ", branchCode='" + branchCode + '\'' +
                ", bankersName='" + bankersName + '\'' +
                ", bankersAddr1='" + bankersAddr1 + '\'' +
                ", bankersAddr2='" + bankersAddr2 + '\'' +
                ", bankersAddr3='" + bankersAddr3 + '\'' +
                ", bankersCity='" + bankersCity + '\'' +
                ", bankersState='" + bankersState + '\'' +
                ", bankersCntry='" + bankersCntry + '\'' +
                ", bankersPin='" + bankersPin + '\'' +
                ", purposeCode='" + purposeCode + '\'' +
                ", regisDate='" + regisDate + '\'' +
                ", expiryDate=" + expiryDate +
                ", approvalDate=" + approvalDate +
                ", statusCode='" + statusCode + '\'' +
                ", custId='" + custId + '\'' +
                ", acid='" + acid + '\'' +
                ", forexFlg='" + forexFlg + '\'' +
                ", chrgAcid='" + chrgAcid + '\'' +
                ", appliFirstName='" + appliFirstName + '\'' +
                ", appliLastName='" + appliLastName + '\'' +
                ", appliAddr1='" + appliAddr1 + '\'' +
                ", appliAddr2='" + appliAddr2 + '\'' +
                ", appliAddr3='" + appliAddr3 + '\'' +
                ", appliCity='" + appliCity + '\'' +
                ", appliState='" + appliState + '\'' +
                ", appliCntry='" + appliCntry + '\'' +
                ", appliPin='" + appliPin + '\'' +
                ", appliPhoneNum='" + appliPhoneNum + '\'' +
                ", appliPassDet='" + appliPassDet + '\'' +
                ", chrgConFlg='" + chrgConFlg + '\'' +
                ", chrgConAmt='" + chrgConAmt + '\'' +
                ", chrgConCrncyCode='" + chrgConCrncyCode + '\'' +
                ", partyCode='" + partyCode + '\'' +
                ", beniFirstName='" + beniFirstName + '\'' +
                ", beniLastName='" + beniLastName + '\'' +
                ", beniAddr1='" + beniAddr1 + '\'' +
                ", beniAddr2='" + beniAddr2 + '\'' +
                ", beniAddr3='" + beniAddr3 + '\'' +
                ", beniCity='" + beniCity + '\'' +
                ", beniState='" + beniState + '\'' +
                ", beniCntry='" + beniCntry + '\'' +
                ", beniPin='" + beniPin + '\'' +
                ", beniPhoneNum='" + beniPhoneNum + '\'' +
                ", beniPassDet='" + beniPassDet + '\'' +
                ", paymentCode='" + paymentCode + '\'' +
                ", transferCode='" + transferCode + '\'' +
                ", originCntryCode='" + originCntryCode + '\'' +
                ", supplyCntryCode='" + supplyCntryCode + '\'' +
                ", transportCode='" + transportCode + '\'' +
                ", portCode='" + portCode + '\'' +
                ", designateBankCode='" + designateBankCode + '\'' +
                ", inspAgentCode='" + inspAgentCode + '\'' +
                ", authFirstName='" + authFirstName + '\'' +
                ", authLastName='" + authLastName + '\'' +
                ", authAppliFirstName='" + authAppliFirstName + '\'' +
                ", authAppliLastName='" + authAppliLastName + '\'' +
                ", remarks='" + remarks + '\'' +
                ", rarNumber='" + rarNumber + '\'' +
                ", rarDate='" + rarDate + '\'' +
                ", generalDesc1='" + generalDesc1 + '\'' +
                ", noOfItems=" + noOfItems +
                ", netWt='" + netWt + '\'' +
                ", totalFobValue='" + totalFobValue + '\'' +
                ", totalFriCharges='" + totalFriCharges + '\'' +
                ", totalAnciCharges='" + totalAnciCharges + '\'' +
                ", formmFcyAmt='" + formmFcyAmt + '\'' +
                ", fcyCrncyCode='" + fcyCrncyCode + '\'' +
                ", invoiceNum='" + invoiceNum + '\'' +
                ", invoiceDate=" + invoiceDate +
                ", excgRateCode='" + excgRateCode + '\'' +
                ", formmRate='" + formmRate + '\'' +
                ", formmNairaEqui=" + formmNairaEqui +
                ", homeCrncyCode='" + homeCrncyCode + '\'' +
                ", firstTimeFlg='" + firstTimeFlg + '\'' +
                ", rcreUserId='" + rcreUserId + '\'' +
                ", rcreTime=" + rcreTime +
                ", lchgUserId='" + lchgUserId + '\'' +
                ", lchgTime=" + lchgTime +
                '}';
    }


}
