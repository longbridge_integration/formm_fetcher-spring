package com.longbridge.formmfetcher.Entities.FinacleEntities;

import java.util.Date;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "REFERENCE_CODE_TABLE", schema = "TBAADM", catalog = "")
public class RefCodeTable {

    private String refCode;
    private String refRecType ;
    private String delFlg;
    private String refDesc;
    private String lchgUserId;
    private Date lchgTime;
    private String rcreUserId;
    private Date rcreTime;
    private long tsCnt;
    private String longRefCode;
    private String bankId;



    @javax.persistence.Id
    @javax.persistence.Basic
    @javax.persistence.Column(name = "refCode", nullable = true, length = 255)
    public String getRefCode() {
        return refCode;
    }

    public void setRefCode(String refCode) {
        this.refCode = refCode;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "REF_REC_TYPE", nullable = true, length = 255)
    public String getRefRecType() {
        return refRecType;
    }

    public void setRefRecType(String refRecType) {
        this.refRecType = refRecType;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "delFlg", nullable = true, length = 255)
    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "refDesc", nullable = true, length = 255)
    public String getRefDesc() {
        return refDesc;
    }

    public void setRefDesc(String refDesc) {
        this.refDesc = refDesc;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "lchgUserId", nullable = true, length = 255)
    public String getLchgUserId() {
        return lchgUserId;
    }

    public void setLchgUserId(String lchgUserId) {
        this.lchgUserId = lchgUserId;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "lchgTime", nullable = true, length = 255)
    public Date getLchgTime() {
        return lchgTime;
    }

    public void setLchgTime(Date lchgTime) {
        this.lchgTime = lchgTime;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "rcreUserId", nullable = true, length = 255)
    public String getRcreUserId() {
        return rcreUserId;
    }

    public void setRcreUserId(String rcreUserId) {
        this.rcreUserId = rcreUserId;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "rcreTime", nullable = true, length = 255)
    public Date getRcreTime() {
        return rcreTime;
    }

    public void setRcreTime(Date rcreTime) {
        this.rcreTime = rcreTime;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "TS_CNT", nullable = true, length = 255)
    public long getTsCnt() {
        return tsCnt;
    }

    public void setTsCnt(long tsCnt) {
        this.tsCnt = tsCnt;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "longRefCode", nullable = true, length = 255)
    public String getLongRefCode() {
        return longRefCode;
    }

    public void setLongRefCode(String longRefCode) {
        this.longRefCode = longRefCode;
    }
    @javax.persistence.Basic
    @javax.persistence.Column(name = "bankId", nullable = true, length = 255)
    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }




    @Override
    public String toString() {
        return "RefCodeTable{" +
                "refCode='" + refCode + '\'' +
                ", refRecType='" + refRecType + '\'' +
                ", delFlg='" + delFlg + '\'' +
                ", refDesc='" + refDesc + '\'' +
                ", lchgUserId='" + lchgUserId + '\'' +
                ", lchgTime=" + lchgTime +
                ", rcreUserId='" + rcreUserId + '\'' +
                ", rcreTime=" + rcreTime +
                ", tsCnt=" + tsCnt +
                ", longRefCode='" + longRefCode + '\'' +
                ", bankId='" + bankId + '\'' +
                '}';
    }




}
