package com.longbridge.formmfetcher.Entities;

import javax.persistence.Id;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
import java.sql.Timestamp;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@Entity
@Table(name = "form_m", schema = "custom", catalog = "")
public class FormM {
    private long id;
    private String infTyp;
    private String infFrx;
    private String infVld;
    private String infSer;
    private String infPfx;
    private String infYea;
    private String infBnkNbr;
    private String frmNbr;
    private String infBnkCod;
    private String infBnkBra;
    private String appTin;
    private String appRcn;
    private String appNepc;
    private String appNam;
    private String appAdr;
    private String appTel;
    private String appSta;
    private String appCit;
    private String appFax;
    private String appMai;
    private String appPas;
    private String benNam;
    private String benAdr;
    private String benCtyCode;
    private String benTel;
    private String benFax;
    private String benMai;
    private String benByOrd;
    private String gdsMotCod;
    private String gdsCtySupCod;
    private String gdsDesCod;
    private String gdsCuoCod;
    private String gdsValCurCod;
    private String gdsValExc;
    private String gdsValFob;
    private String gdsValFrg;
    private String gdsValTac;
    private String gdsValInc;
    private String gdsValSrcCod;
    private String gdsValTcfVal;
    private String gdsProInv;
    private Timestamp gdsProDat;
    private String gdsMopCod;
    private String gdsPayDat;
    private String gdsTrfCod;
    private String gdsTodCod;
    private String gdsGenDsc;
    private String gdsGenWgt;
    private Integer gdsGenTit;
    private String tspShpDat;
    private String tspAirTck;
    private String tspAirLin;
    private String scaCod;
    private String bnkCod;
    private String endAppNam;
    private Timestamp endAppDat;
    private String endAutNam;
    private Timestamp endAutDat;
    private String sta;
    private String itmCtyOrgCod;
    private String atdTyp;
    private String atdRef;
    private String atdDat;
    private String atdBin;
    private String atdBinTyp;
    private String supInfDat;
    private String supInfUsr;
    private String supInfOpr;
    private String supInfMsg;
    private Timestamp timePersisted;
    private Byte isPersisted;


    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    @Basic
    @Column(name = "inf_typ", nullable = true, length = 255)
    public String getInfTyp() {
        return infTyp;
    }

    public void setInfTyp(String infTyp) {
        this.infTyp = infTyp;
    }

    @Basic
    @Column(name = "inf_frx", nullable = true, length = 255)
    public String getInfFrx() {
        return infFrx;
    }

    public void setInfFrx(String infFrx) {
        this.infFrx = infFrx;
    }

    @Basic
    @Column(name = "inf_vld", nullable = true)
    public String getInfVld() {
        return infVld;
    }

    public void setInfVld(String infVld) {
        this.infVld = infVld;
    }

    @Basic
    @Column(name = "inf_ser", nullable = true, length = 255)
    public String getInfSer() {
        return infSer;
    }

    public void setInfSer(String infSer) {
        this.infSer = infSer;
    }

    @Basic
    @Column(name = "inf_pfx", nullable = true, length = 255)
    public String getInfPfx() {
        return infPfx;
    }

    public void setInfPfx(String infPfx) {
        this.infPfx = infPfx;
    }

    @Basic
    @Column(name = "inf_yea", nullable = true, length = 255)
    public String getInfYea() {
        return infYea;
    }

    public void setInfYea(String infYea) {
        this.infYea = infYea;
    }

    @Basic
    @Column(name = "inf_bnk_nbr", nullable = true, length = 255)
    public String getInfBnkNbr() {
        return infBnkNbr;
    }

    public void setInfBnkNbr(String infBnkNbr) {
        this.infBnkNbr = infBnkNbr;
    }
    @Id
    @Basic
    @Column(name = "frm_nbr", nullable = true, length = 255)
    public String getFrmNbr() {
        return frmNbr;
    }

    public void setFrmNbr(String frmNbr) {
        this.frmNbr = frmNbr;
    }

    @Basic
    @Column(name = "inf_bnk_cod", nullable = true, length = 255)
    public String getInfBnkCod() {
        return infBnkCod;
    }

    public void setInfBnkCod(String infBnkCod) {
        this.infBnkCod = infBnkCod;
    }

    @Basic
    @Column(name = "inf_bnk_bra", nullable = true, length = 255)
    public String getInfBnkBra() {
        return infBnkBra;
    }

    public void setInfBnkBra(String infBnkBra) {
        this.infBnkBra = infBnkBra;
    }

    @Basic
    @Column(name = "app_tin", nullable = true, length = 255)
    public String getAppTin() {
        return appTin;
    }

    public void setAppTin(String appTin) {
        this.appTin = appTin;
    }

    @Basic
    @Column(name = "app_rcn", nullable = true, length = 255)
    public String getAppRcn() {
        return appRcn;
    }

    public void setAppRcn(String appRcn) {
        this.appRcn = appRcn;
    }

    @Basic
    @Column(name = "app_nepc", nullable = true, length = 255)
    public String getAppNepc() {
        return appNepc;
    }

    public void setAppNepc(String appNepc) {
        this.appNepc = appNepc;
    }

    @Basic
    @Column(name = "app_nam", nullable = true, length = 255)
    public String getAppNam() {
        return appNam;
    }

    public void setAppNam(String appNam) {
        this.appNam = appNam;
    }

    @Basic
    @Column(name = "app_adr", nullable = true, length = 255)
    public String getAppAdr() {
        return appAdr;
    }

    public void setAppAdr(String appAdr) {
        this.appAdr = appAdr;
    }

    @Basic
    @Column(name = "app_tel", nullable = true, length = 255)
    public String getAppTel() {
        return appTel;
    }

    public void setAppTel(String appTel) {
        this.appTel = appTel;
    }

    @Basic
    @Column(name = "app_sta", nullable = true, length = 255)
    public String getAppSta() {
        return appSta;
    }

    public void setAppSta(String appSta) {
        this.appSta = appSta;
    }

    @Basic
    @Column(name = "app_cit", nullable = true, length = 255)
    public String getAppCit() {
        return appCit;
    }

    public void setAppCit(String appCit) {
        this.appCit = appCit;
    }

    @Basic
    @Column(name = "app_fax", nullable = true, length = 255)
    public String getAppFax() {
        return appFax;
    }

    public void setAppFax(String appFax) {
        this.appFax = appFax;
    }

    @Basic
    @Column(name = "app_mai", nullable = true, length = 255)
    public String getAppMai() {
        return appMai;
    }

    public void setAppMai(String appMai) {
        this.appMai = appMai;
    }

    @Basic
    @Column(name = "app_pas", nullable = true, length = 255)
    public String getAppPas() {
        return appPas;
    }

    public void setAppPas(String appPas) {
        this.appPas = appPas;
    }

    @Basic
    @Column(name = "ben_nam", nullable = true, length = 255)
    public String getBenNam() {
        return benNam;
    }

    public void setBenNam(String benNam) {
        this.benNam = benNam;
    }

    @Basic
    @Column(name = "ben_adr", nullable = true, length = 255)
    public String getBenAdr() {
        return benAdr;
    }

    public void setBenAdr(String benAdr) {
        this.benAdr = benAdr;
    }

    @Basic
    @Column(name = "ben_cty_code", nullable = true, length = 255)
    public String getBenCtyCode() {
        return benCtyCode;
    }

    public void setBenCtyCode(String benCtyCode) {
        this.benCtyCode = benCtyCode;
    }

    @Basic
    @Column(name = "ben_tel", nullable = true, length = 255)
    public String getBenTel() {
        return benTel;
    }

    public void setBenTel(String benTel) {
        this.benTel = benTel;
    }

    @Basic
    @Column(name = "ben_fax", nullable = true, length = 255)
    public String getBenFax() {
        return benFax;
    }

    public void setBenFax(String benFax) {
        this.benFax = benFax;
    }

    @Basic
    @Column(name = "ben_mai", nullable = true, length = 255)
    public String getBenMai() {
        return benMai;
    }

    public void setBenMai(String benMai) {
        this.benMai = benMai;
    }

    @Basic
    @Column(name = "ben_by_ord", nullable = true, length = 255)
    public String getBenByOrd() {
        return benByOrd;
    }

    public void setBenByOrd(String benByOrd) {
        this.benByOrd = benByOrd;
    }

    @Basic
    @Column(name = "gds_mot_cod", nullable = true, length = 255)
    public String getGdsMotCod() {
        return gdsMotCod;
    }

    public void setGdsMotCod(String gdsMotCod) {
        this.gdsMotCod = gdsMotCod;
    }

    @Basic
    @Column(name = "gds_cty_sup_cod", nullable = true, length = 255)
    public String getGdsCtySupCod() {
        return gdsCtySupCod;
    }

    public void setGdsCtySupCod(String gdsCtySupCod) {
        this.gdsCtySupCod = gdsCtySupCod;
    }

    @Basic
    @Column(name = "gds_des_cod", nullable = true, length = 255)
    public String getGdsDesCod() {
        return gdsDesCod;
    }

    public void setGdsDesCod(String gdsDesCod) {
        this.gdsDesCod = gdsDesCod;
    }

    @Basic
    @Column(name = "gds_cuo_cod", nullable = true, length = 255)
    public String getGdsCuoCod() {
        return gdsCuoCod;
    }

    public void setGdsCuoCod(String gdsCuoCod) {
        this.gdsCuoCod = gdsCuoCod;
    }

    @Basic
    @Column(name = "gds_val_cur_cod", nullable = true, length = 255)
    public String getGdsValCurCod() {
        return gdsValCurCod;
    }

    public void setGdsValCurCod(String gdsValCurCod) {
        this.gdsValCurCod = gdsValCurCod;
    }

    @Basic
    @Column(name = "gds_val_exc", nullable = true, length = 255)
    public String getGdsValExc() {
        return gdsValExc;
    }

    public void setGdsValExc(String gdsValExc) {
        this.gdsValExc = gdsValExc;
    }

    @Basic
    @Column(name = "gds_val_fob", nullable = true, length = 255)
    public String getGdsValFob() {
        return gdsValFob;
    }

    public void setGdsValFob(String gdsValFob) {
        this.gdsValFob = gdsValFob;
    }

    @Basic
    @Column(name = "gds_val_frg", nullable = true, length = 255)
    public String getGdsValFrg() {
        return gdsValFrg;
    }

    public void setGdsValFrg(String gdsValFrg) {
        this.gdsValFrg = gdsValFrg;
    }

    @Basic
    @Column(name = "gds_val_tac", nullable = true, length = 255)
    public String getGdsValTac() {
        return gdsValTac;
    }

    public void setGdsValTac(String gdsValTac) {
        this.gdsValTac = gdsValTac;
    }

    @Basic
    @Column(name = "gds_val_inc", nullable = true, length = 255)
    public String getGdsValInc() {
        return gdsValInc;
    }

    public void setGdsValInc(String gdsValInc) {
        this.gdsValInc = gdsValInc;
    }

    @Basic
    @Column(name = "gds_val_src_cod", nullable = true, length = 255)
    public String getGdsValSrcCod() {
        return gdsValSrcCod;
    }

    public void setGdsValSrcCod(String gdsValSrcCod) {
        this.gdsValSrcCod = gdsValSrcCod;
    }

    @Basic
    @Column(name = "gds_val_tcf_val", nullable = true, length = 255)
    public String getGdsValTcfVal() {
        return gdsValTcfVal;
    }

    public void setGdsValTcfVal(String gdsValTcfVal) {
        this.gdsValTcfVal = gdsValTcfVal;
    }

    @Basic
    @Column(name = "gds_pro_inv", nullable = true, length = 255)
    public String getGdsProInv() {
        return gdsProInv;
    }

    public void setGdsProInv(String gdsProInv) {
        this.gdsProInv = gdsProInv;
    }

    @Basic
    @Column(name = "gds_pro_dat", nullable = true)
    public Timestamp getGdsProDat() {
        return gdsProDat;
    }

    public void setGdsProDat(Timestamp gdsProDat) {
        this.gdsProDat = gdsProDat;
    }

    @Basic
    @Column(name = "gds_mop_cod", nullable = true, length = 255)
    public String getGdsMopCod() {
        return gdsMopCod;
    }

    public void setGdsMopCod(String gdsMopCod) {
        this.gdsMopCod = gdsMopCod;
    }

    @Basic
    @Column(name = "gds_pay_dat", nullable = true, length = 255)
    public String getGdsPayDat() {
        return gdsPayDat;
    }

    public void setGdsPayDat(String gdsPayDat) {
        this.gdsPayDat = gdsPayDat;
    }

    @Basic
    @Column(name = "gds_trf_cod", nullable = true, length = 255)
    public String getGdsTrfCod() {
        return gdsTrfCod;
    }

    public void setGdsTrfCod(String gdsTrfCod) {
        this.gdsTrfCod = gdsTrfCod;
    }

    @Basic
    @Column(name = "gds_tod_cod", nullable = true, length = 255)
    public String getGdsTodCod() {
        return gdsTodCod;
    }

    public void setGdsTodCod(String gdsTodCod) {
        this.gdsTodCod = gdsTodCod;
    }

    @Basic
    @Column(name = "gds_gen_dsc", nullable = true, length = 255)
    public String getGdsGenDsc() {
        return gdsGenDsc;
    }

    public void setGdsGenDsc(String gdsGenDsc) {
        this.gdsGenDsc = gdsGenDsc;
    }

    @Basic
    @Column(name = "gds_gen_wgt", nullable = true, length = 255)
    public String getGdsGenWgt() {
        return gdsGenWgt;
    }

    public void setGdsGenWgt(String gdsGenWgt) {
        this.gdsGenWgt = gdsGenWgt;
    }

    @Basic
    @Column(name = "gds_gen_tit", nullable = true)
    public Integer getGdsGenTit() {
        return gdsGenTit;
    }

    public void setGdsGenTit(Integer gdsGenTit) {
        this.gdsGenTit = gdsGenTit;
    }

    @Basic
    @Column(name = "tsp_shp_dat", nullable = true, length = 255)
    public String getTspShpDat() {
        return tspShpDat;
    }

    public void setTspShpDat(String tspShpDat) {
        this.tspShpDat = tspShpDat;
    }

    @Basic
    @Column(name = "tsp_air_tck", nullable = true, length = 255)
    public String getTspAirTck() {
        return tspAirTck;
    }

    public void setTspAirTck(String tspAirTck) {
        this.tspAirTck = tspAirTck;
    }

    @Basic
    @Column(name = "tsp_air_lin", nullable = true, length = 255)
    public String getTspAirLin() {
        return tspAirLin;
    }

    public void setTspAirLin(String tspAirLin) {
        this.tspAirLin = tspAirLin;
    }

    @Basic
    @Column(name = "sca_cod", nullable = true, length = 255)
    public String getScaCod() {
        return scaCod;
    }

    public void setScaCod(String scaCod) {
        this.scaCod = scaCod;
    }

    @Basic
    @Column(name = "bnk_cod", nullable = true, length = 255)
    public String getBnkCod() {
        return bnkCod;
    }

    public void setBnkCod(String bnkCod) {
        this.bnkCod = bnkCod;
    }

    @Basic
    @Column(name = "end_app_nam", nullable = true, length = 255)
    public String getEndAppNam() {
        return endAppNam;
    }

    public void setEndAppNam(String endAppNam) {
        this.endAppNam = endAppNam;
    }

    @Basic
    @Column(name = "end_app_dat", nullable = true)
    public Timestamp getEndAppDat() {
        return endAppDat;
    }

    public void setEndAppDat(Timestamp endAppDat) {
        this.endAppDat = endAppDat;
    }

    @Basic
    @Column(name = "end_aut_nam", nullable = true, length = 255)
    public String getEndAutNam() {
        return endAutNam;
    }

    public void setEndAutNam(String endAutNam) {
        this.endAutNam = endAutNam;
    }

    @Basic
    @Column(name = "end_aut_dat", nullable = true)
    public Timestamp getEndAutDat() {
        return endAutDat;
    }

    public void setEndAutDat(Timestamp endAutDat) {
        this.endAutDat = endAutDat;
    }

    @Basic
    @Column(name = "sta", nullable = true, length = 255)
    public String getSta() {
        return sta;
    }

    public void setSta(String sta) {
        this.sta = sta;
    }

    @Basic
    @Column(name = "itm_cty_org_cod", nullable = true, length = 255)
    public String getItmCtyOrgCod() {
        return itmCtyOrgCod;
    }

    public void setItmCtyOrgCod(String itmCtyOrgCod) {
        this.itmCtyOrgCod = itmCtyOrgCod;
    }

    @Basic
    @Column(name = "atd_typ", nullable = true, length = 255)
    public String getAtdTyp() {
        return atdTyp;
    }

    public void setAtdTyp(String atdTyp) {
        this.atdTyp = atdTyp;
    }

    @Basic
    @Column(name = "atd_ref", nullable = true, length = 255)
    public String getAtdRef() {
        return atdRef;
    }

    public void setAtdRef(String atdRef) {
        this.atdRef = atdRef;
    }

    @Basic
    @Column(name = "atd_dat", nullable = true, length = 255)
    public String getAtdDat() {
        return atdDat;
    }

    public void setAtdDat(String atdDat) {
        this.atdDat = atdDat;
    }

    @Basic
    @Column(name = "atd_bin", nullable = true, length = 255)
    public String getAtdBin() {
        return atdBin;
    }

    public void setAtdBin(String atdBin) {
        this.atdBin = atdBin;
    }

    @Basic
    @Column(name = "atd_bin_typ", nullable = true, length = 255)
    public String getAtdBinTyp() {
        return atdBinTyp;
    }

    public void setAtdBinTyp(String atdBinTyp) {
        this.atdBinTyp = atdBinTyp;
    }

    @Basic
    @Column(name = "sup_inf_dat", nullable = true, length = 255)
    public String getSupInfDat() {
        return supInfDat;
    }

    public void setSupInfDat(String supInfDat) {
        this.supInfDat = supInfDat;
    }

    @Basic
    @Column(name = "sup_inf_usr", nullable = true, length = 255)
    public String getSupInfUsr() {
        return supInfUsr;
    }

    public void setSupInfUsr(String supInfUsr) {
        this.supInfUsr = supInfUsr;
    }

    @Basic
    @Column(name = "sup_inf_opr", nullable = true, length = 255)
    public String getSupInfOpr() {
        return supInfOpr;
    }

    public void setSupInfOpr(String supInfOpr) {
        this.supInfOpr = supInfOpr;
    }

    @Basic
    @Column(name = "sup_inf_msg", nullable = true, length = 255)
    public String getSupInfMsg() {
        return supInfMsg;
    }

    public void setSupInfMsg(String supInfMsg) {
        this.supInfMsg = supInfMsg;
    }

    @Basic
    @Column(name = "time_persisted", nullable = true)
    public Timestamp getTimePersisted() {
        return timePersisted;
    }

    public void setTimePersisted(Timestamp timePersisted) {
        this.timePersisted = timePersisted;
    }

    @Basic
    @Column(name = "is_persisted", nullable = true)
    public Byte getIsPersisted() {
        return isPersisted;
    }

    public void setIsPersisted(Byte isPersisted) {
        this.isPersisted = isPersisted;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FormM formM = (FormM) o;

        if (id != formM.id) return false;
        if (infTyp != null ? !infTyp.equals(formM.infTyp) : formM.infTyp != null) return false;
        if (infFrx != null ? !infFrx.equals(formM.infFrx) : formM.infFrx != null) return false;
        if (infVld != null ? !infVld.equals(formM.infVld) : formM.infVld != null) return false;
        if (infSer != null ? !infSer.equals(formM.infSer) : formM.infSer != null) return false;
        if (infPfx != null ? !infPfx.equals(formM.infPfx) : formM.infPfx != null) return false;
        if (infYea != null ? !infYea.equals(formM.infYea) : formM.infYea != null) return false;
        if (infBnkNbr != null ? !infBnkNbr.equals(formM.infBnkNbr) : formM.infBnkNbr != null) return false;
        if (frmNbr != null ? !frmNbr.equals(formM.frmNbr) : formM.frmNbr != null) return false;
        if (infBnkCod != null ? !infBnkCod.equals(formM.infBnkCod) : formM.infBnkCod != null) return false;
        if (infBnkBra != null ? !infBnkBra.equals(formM.infBnkBra) : formM.infBnkBra != null) return false;
        if (appTin != null ? !appTin.equals(formM.appTin) : formM.appTin != null) return false;
        if (appRcn != null ? !appRcn.equals(formM.appRcn) : formM.appRcn != null) return false;
        if (appNepc != null ? !appNepc.equals(formM.appNepc) : formM.appNepc != null) return false;
        if (appNam != null ? !appNam.equals(formM.appNam) : formM.appNam != null) return false;
        if (appAdr != null ? !appAdr.equals(formM.appAdr) : formM.appAdr != null) return false;
        if (appTel != null ? !appTel.equals(formM.appTel) : formM.appTel != null) return false;
        if (appSta != null ? !appSta.equals(formM.appSta) : formM.appSta != null) return false;
        if (appCit != null ? !appCit.equals(formM.appCit) : formM.appCit != null) return false;
        if (appFax != null ? !appFax.equals(formM.appFax) : formM.appFax != null) return false;
        if (appMai != null ? !appMai.equals(formM.appMai) : formM.appMai != null) return false;
        if (appPas != null ? !appPas.equals(formM.appPas) : formM.appPas != null) return false;
        if (benNam != null ? !benNam.equals(formM.benNam) : formM.benNam != null) return false;
        if (benAdr != null ? !benAdr.equals(formM.benAdr) : formM.benAdr != null) return false;
        if (benCtyCode != null ? !benCtyCode.equals(formM.benCtyCode) : formM.benCtyCode != null) return false;
        if (benTel != null ? !benTel.equals(formM.benTel) : formM.benTel != null) return false;
        if (benFax != null ? !benFax.equals(formM.benFax) : formM.benFax != null) return false;
        if (benMai != null ? !benMai.equals(formM.benMai) : formM.benMai != null) return false;
        if (benByOrd != null ? !benByOrd.equals(formM.benByOrd) : formM.benByOrd != null) return false;
        if (gdsMotCod != null ? !gdsMotCod.equals(formM.gdsMotCod) : formM.gdsMotCod != null) return false;
        if (gdsCtySupCod != null ? !gdsCtySupCod.equals(formM.gdsCtySupCod) : formM.gdsCtySupCod != null) return false;
        if (gdsDesCod != null ? !gdsDesCod.equals(formM.gdsDesCod) : formM.gdsDesCod != null) return false;
        if (gdsCuoCod != null ? !gdsCuoCod.equals(formM.gdsCuoCod) : formM.gdsCuoCod != null) return false;
        if (gdsValCurCod != null ? !gdsValCurCod.equals(formM.gdsValCurCod) : formM.gdsValCurCod != null) return false;
        if (gdsValExc != null ? !gdsValExc.equals(formM.gdsValExc) : formM.gdsValExc != null) return false;
        if (gdsValFob != null ? !gdsValFob.equals(formM.gdsValFob) : formM.gdsValFob != null) return false;
        if (gdsValFrg != null ? !gdsValFrg.equals(formM.gdsValFrg) : formM.gdsValFrg != null) return false;
        if (gdsValTac != null ? !gdsValTac.equals(formM.gdsValTac) : formM.gdsValTac != null) return false;
        if (gdsValInc != null ? !gdsValInc.equals(formM.gdsValInc) : formM.gdsValInc != null) return false;
        if (gdsValSrcCod != null ? !gdsValSrcCod.equals(formM.gdsValSrcCod) : formM.gdsValSrcCod != null) return false;
        if (gdsValTcfVal != null ? !gdsValTcfVal.equals(formM.gdsValTcfVal) : formM.gdsValTcfVal != null) return false;
        if (gdsProInv != null ? !gdsProInv.equals(formM.gdsProInv) : formM.gdsProInv != null) return false;
        if (gdsProDat != null ? !gdsProDat.equals(formM.gdsProDat) : formM.gdsProDat != null) return false;
        if (gdsMopCod != null ? !gdsMopCod.equals(formM.gdsMopCod) : formM.gdsMopCod != null) return false;
        if (gdsPayDat != null ? !gdsPayDat.equals(formM.gdsPayDat) : formM.gdsPayDat != null) return false;
        if (gdsTrfCod != null ? !gdsTrfCod.equals(formM.gdsTrfCod) : formM.gdsTrfCod != null) return false;
        if (gdsTodCod != null ? !gdsTodCod.equals(formM.gdsTodCod) : formM.gdsTodCod != null) return false;
        if (gdsGenDsc != null ? !gdsGenDsc.equals(formM.gdsGenDsc) : formM.gdsGenDsc != null) return false;
        if (gdsGenWgt != null ? !gdsGenWgt.equals(formM.gdsGenWgt) : formM.gdsGenWgt != null) return false;
        if (gdsGenTit != null ? !gdsGenTit.equals(formM.gdsGenTit) : formM.gdsGenTit != null) return false;
        if (tspShpDat != null ? !tspShpDat.equals(formM.tspShpDat) : formM.tspShpDat != null) return false;
        if (tspAirTck != null ? !tspAirTck.equals(formM.tspAirTck) : formM.tspAirTck != null) return false;
        if (tspAirLin != null ? !tspAirLin.equals(formM.tspAirLin) : formM.tspAirLin != null) return false;
        if (scaCod != null ? !scaCod.equals(formM.scaCod) : formM.scaCod != null) return false;
        if (bnkCod != null ? !bnkCod.equals(formM.bnkCod) : formM.bnkCod != null) return false;
        if (endAppNam != null ? !endAppNam.equals(formM.endAppNam) : formM.endAppNam != null) return false;
        if (endAppDat != null ? !endAppDat.equals(formM.endAppDat) : formM.endAppDat != null) return false;
        if (endAutNam != null ? !endAutNam.equals(formM.endAutNam) : formM.endAutNam != null) return false;
        if (endAutDat != null ? !endAutDat.equals(formM.endAutDat) : formM.endAutDat != null) return false;
        if (sta != null ? !sta.equals(formM.sta) : formM.sta != null) return false;
        if (itmCtyOrgCod != null ? !itmCtyOrgCod.equals(formM.itmCtyOrgCod) : formM.itmCtyOrgCod != null) return false;
        if (atdTyp != null ? !atdTyp.equals(formM.atdTyp) : formM.atdTyp != null) return false;
        if (atdRef != null ? !atdRef.equals(formM.atdRef) : formM.atdRef != null) return false;
        if (atdDat != null ? !atdDat.equals(formM.atdDat) : formM.atdDat != null) return false;
        if (atdBin != null ? !atdBin.equals(formM.atdBin) : formM.atdBin != null) return false;
        if (atdBinTyp != null ? !atdBinTyp.equals(formM.atdBinTyp) : formM.atdBinTyp != null) return false;
        if (supInfDat != null ? !supInfDat.equals(formM.supInfDat) : formM.supInfDat != null) return false;
        if (supInfUsr != null ? !supInfUsr.equals(formM.supInfUsr) : formM.supInfUsr != null) return false;
        if (supInfOpr != null ? !supInfOpr.equals(formM.supInfOpr) : formM.supInfOpr != null) return false;
        if (supInfMsg != null ? !supInfMsg.equals(formM.supInfMsg) : formM.supInfMsg != null) return false;
        if (timePersisted != null ? !timePersisted.equals(formM.timePersisted) : formM.timePersisted != null)
            return false;
        if (isPersisted != null ? !isPersisted.equals(formM.isPersisted) : formM.isPersisted != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (infTyp != null ? infTyp.hashCode() : 0);
        result = 31 * result + (infFrx != null ? infFrx.hashCode() : 0);
        result = 31 * result + (infVld != null ? infVld.hashCode() : 0);
        result = 31 * result + (infSer != null ? infSer.hashCode() : 0);
        result = 31 * result + (infPfx != null ? infPfx.hashCode() : 0);
        result = 31 * result + (infYea != null ? infYea.hashCode() : 0);
        result = 31 * result + (infBnkNbr != null ? infBnkNbr.hashCode() : 0);
        result = 31 * result + (frmNbr != null ? frmNbr.hashCode() : 0);
        result = 31 * result + (infBnkCod != null ? infBnkCod.hashCode() : 0);
        result = 31 * result + (infBnkBra != null ? infBnkBra.hashCode() : 0);
        result = 31 * result + (appTin != null ? appTin.hashCode() : 0);
        result = 31 * result + (appRcn != null ? appRcn.hashCode() : 0);
        result = 31 * result + (appNepc != null ? appNepc.hashCode() : 0);
        result = 31 * result + (appNam != null ? appNam.hashCode() : 0);
        result = 31 * result + (appAdr != null ? appAdr.hashCode() : 0);
        result = 31 * result + (appTel != null ? appTel.hashCode() : 0);
        result = 31 * result + (appSta != null ? appSta.hashCode() : 0);
        result = 31 * result + (appCit != null ? appCit.hashCode() : 0);
        result = 31 * result + (appFax != null ? appFax.hashCode() : 0);
        result = 31 * result + (appMai != null ? appMai.hashCode() : 0);
        result = 31 * result + (appPas != null ? appPas.hashCode() : 0);
        result = 31 * result + (benNam != null ? benNam.hashCode() : 0);
        result = 31 * result + (benAdr != null ? benAdr.hashCode() : 0);
        result = 31 * result + (benCtyCode != null ? benCtyCode.hashCode() : 0);
        result = 31 * result + (benTel != null ? benTel.hashCode() : 0);
        result = 31 * result + (benFax != null ? benFax.hashCode() : 0);
        result = 31 * result + (benMai != null ? benMai.hashCode() : 0);
        result = 31 * result + (benByOrd != null ? benByOrd.hashCode() : 0);
        result = 31 * result + (gdsMotCod != null ? gdsMotCod.hashCode() : 0);
        result = 31 * result + (gdsCtySupCod != null ? gdsCtySupCod.hashCode() : 0);
        result = 31 * result + (gdsDesCod != null ? gdsDesCod.hashCode() : 0);
        result = 31 * result + (gdsCuoCod != null ? gdsCuoCod.hashCode() : 0);
        result = 31 * result + (gdsValCurCod != null ? gdsValCurCod.hashCode() : 0);
        result = 31 * result + (gdsValExc != null ? gdsValExc.hashCode() : 0);
        result = 31 * result + (gdsValFob != null ? gdsValFob.hashCode() : 0);
        result = 31 * result + (gdsValFrg != null ? gdsValFrg.hashCode() : 0);
        result = 31 * result + (gdsValTac != null ? gdsValTac.hashCode() : 0);
        result = 31 * result + (gdsValInc != null ? gdsValInc.hashCode() : 0);
        result = 31 * result + (gdsValSrcCod != null ? gdsValSrcCod.hashCode() : 0);
        result = 31 * result + (gdsValTcfVal != null ? gdsValTcfVal.hashCode() : 0);
        result = 31 * result + (gdsProInv != null ? gdsProInv.hashCode() : 0);
        result = 31 * result + (gdsProDat != null ? gdsProDat.hashCode() : 0);
        result = 31 * result + (gdsMopCod != null ? gdsMopCod.hashCode() : 0);
        result = 31 * result + (gdsPayDat != null ? gdsPayDat.hashCode() : 0);
        result = 31 * result + (gdsTrfCod != null ? gdsTrfCod.hashCode() : 0);
        result = 31 * result + (gdsTodCod != null ? gdsTodCod.hashCode() : 0);
        result = 31 * result + (gdsGenDsc != null ? gdsGenDsc.hashCode() : 0);
        result = 31 * result + (gdsGenWgt != null ? gdsGenWgt.hashCode() : 0);
        result = 31 * result + (gdsGenTit != null ? gdsGenTit.hashCode() : 0);
        result = 31 * result + (tspShpDat != null ? tspShpDat.hashCode() : 0);
        result = 31 * result + (tspAirTck != null ? tspAirTck.hashCode() : 0);
        result = 31 * result + (tspAirLin != null ? tspAirLin.hashCode() : 0);
        result = 31 * result + (scaCod != null ? scaCod.hashCode() : 0);
        result = 31 * result + (bnkCod != null ? bnkCod.hashCode() : 0);
        result = 31 * result + (endAppNam != null ? endAppNam.hashCode() : 0);
        result = 31 * result + (endAppDat != null ? endAppDat.hashCode() : 0);
        result = 31 * result + (endAutNam != null ? endAutNam.hashCode() : 0);
        result = 31 * result + (endAutDat != null ? endAutDat.hashCode() : 0);
        result = 31 * result + (sta != null ? sta.hashCode() : 0);
        result = 31 * result + (itmCtyOrgCod != null ? itmCtyOrgCod.hashCode() : 0);
        result = 31 * result + (atdTyp != null ? atdTyp.hashCode() : 0);
        result = 31 * result + (atdRef != null ? atdRef.hashCode() : 0);
        result = 31 * result + (atdDat != null ? atdDat.hashCode() : 0);
        result = 31 * result + (atdBin != null ? atdBin.hashCode() : 0);
        result = 31 * result + (atdBinTyp != null ? atdBinTyp.hashCode() : 0);
        result = 31 * result + (supInfDat != null ? supInfDat.hashCode() : 0);
        result = 31 * result + (supInfUsr != null ? supInfUsr.hashCode() : 0);
        result = 31 * result + (supInfOpr != null ? supInfOpr.hashCode() : 0);
        result = 31 * result + (supInfMsg != null ? supInfMsg.hashCode() : 0);
        result = 31 * result + (timePersisted != null ? timePersisted.hashCode() : 0);
        result = 31 * result + (isPersisted != null ? isPersisted.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FormM{" +
                "id=" + id +
                ", infTyp='" + infTyp + '\'' +
                ", infFrx='" + infFrx + '\'' +
                ", infVld=" + infVld +
                ", infSer='" + infSer + '\'' +
                ", infPfx='" + infPfx + '\'' +
                ", infYea='" + infYea + '\'' +
                ", infBnkNbr='" + infBnkNbr + '\'' +
                ", frmNbr='" + frmNbr + '\'' +
                ", infBnkCod='" + infBnkCod + '\'' +
                ", infBnkBra='" + infBnkBra + '\'' +
                ", appTin='" + appTin + '\'' +
                ", appRcn='" + appRcn + '\'' +
                ", appNepc='" + appNepc + '\'' +
                ", appNam='" + appNam + '\'' +
                ", appAdr='" + appAdr + '\'' +
                ", appTel='" + appTel + '\'' +
                ", appSta='" + appSta + '\'' +
                ", appCit='" + appCit + '\'' +
                ", appFax='" + appFax + '\'' +
                ", appMai='" + appMai + '\'' +
                ", appPas='" + appPas + '\'' +
                ", benNam='" + benNam + '\'' +
                ", benAdr='" + benAdr + '\'' +
                ", benCtyCode='" + benCtyCode + '\'' +
                ", benTel='" + benTel + '\'' +
                ", benFax='" + benFax + '\'' +
                ", benMai='" + benMai + '\'' +
                ", benByOrd='" + benByOrd + '\'' +
                ", gdsMotCod='" + gdsMotCod + '\'' +
                ", gdsCtySupCod='" + gdsCtySupCod + '\'' +
                ", gdsDesCod='" + gdsDesCod + '\'' +
                ", gdsCuoCod='" + gdsCuoCod + '\'' +
                ", gdsValCurCod='" + gdsValCurCod + '\'' +
                ", gdsValExc='" + gdsValExc + '\'' +
                ", gdsValFob='" + gdsValFob + '\'' +
                ", gdsValFrg='" + gdsValFrg + '\'' +
                ", gdsValTac='" + gdsValTac + '\'' +
                ", gdsValInc='" + gdsValInc + '\'' +
                ", gdsValSrcCod='" + gdsValSrcCod + '\'' +
                ", gdsValTcfVal='" + gdsValTcfVal + '\'' +
                ", gdsProInv='" + gdsProInv + '\'' +
                ", gdsProDat=" + gdsProDat +
                ", gdsMopCod='" + gdsMopCod + '\'' +
                ", gdsPayDat='" + gdsPayDat + '\'' +
                ", gdsTrfCod='" + gdsTrfCod + '\'' +
                ", gdsTodCod='" + gdsTodCod + '\'' +
                ", gdsGenDsc='" + gdsGenDsc + '\'' +
                ", gdsGenWgt='" + gdsGenWgt + '\'' +
                ", gdsGenTit=" + gdsGenTit +
                ", tspShpDat='" + tspShpDat + '\'' +
                ", tspAirTck='" + tspAirTck + '\'' +
                ", tspAirLin='" + tspAirLin + '\'' +
                ", scaCod='" + scaCod + '\'' +
                ", bnkCod='" + bnkCod + '\'' +
                ", endAppNam='" + endAppNam + '\'' +
                ", endAppDat=" + endAppDat +
                ", endAutNam='" + endAutNam + '\'' +
                ", endAutDat=" + endAutDat +
                ", sta='" + sta + '\'' +
                ", itmCtyOrgCod='" + itmCtyOrgCod + '\'' +
                ", atdTyp='" + atdTyp + '\'' +
                ", atdRef='" + atdRef + '\'' +
                ", atdDat='" + atdDat + '\'' +
                ", atdBin='" + atdBin + '\'' +
                ", atdBinTyp='" + atdBinTyp + '\'' +
                ", supInfDat='" + supInfDat + '\'' +
                ", supInfUsr='" + supInfUsr + '\'' +
                ", supInfOpr='" + supInfOpr + '\'' +
                ", supInfMsg='" + supInfMsg + '\'' +
                ", timePersisted=" + timePersisted +
                ", isPersisted=" + isPersisted +
                '}';
    }
}
