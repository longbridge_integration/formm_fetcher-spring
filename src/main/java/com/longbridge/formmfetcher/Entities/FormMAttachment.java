package com.longbridge.formmfetcher.Entities;

import java.util.Arrays;
import javax.persistence.Id;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;
/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@Entity
@Table(name = "form_mattachment", schema = "custom", catalog = "")
public class FormMAttachment {
    private long id;
    private String formMNbr;
    private String atdType;
    private String atdRef;
    private String atdDat;
    private byte[] atdBin;
    private String atdBinTyp;
    private long tsCnt;
    private String isDocGenerated;

    @Id
    @Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @Basic
    @Column(name = "ts_cnt", nullable = true)
    public long getTsCnt() {
        return tsCnt;
    }

    public void setTsCnt(long tsCnt) {
        this.tsCnt = tsCnt;
    }

    @Basic
    @Column(name = "atd_type", nullable = true)
    public String getAtdType() {
        return atdType;
    }

    public void setAtdType(String atdType) {
        this.atdType = atdType;
    }

    @Basic
    @Column(name = "form_numb", nullable = true)
    public String getFormMNbr() {
        return formMNbr;
    }

    public void setFormMNbr(String formMNbr) {
        this.formMNbr = formMNbr;
    }

    @Basic
    @Column(name = "atd_ref", nullable = true)
    public String getAtdRef() {
        return atdRef;
    }

    public void setAtdRef(String atdRef) {
        this.atdRef = atdRef;
    }

    @Basic
    @Column(name = "atd_dat", nullable = true)
    public String getAtdDat() {
        return atdDat;
    }

    public void setAtdDat(String atdDat) {
        this.atdDat = atdDat;
    }

    @Basic
    @Column(name = "atd_bin_typ", nullable = true)
    public String getAtdBinTyp() {
        return atdBinTyp;
    }

    public void setAtdBinTyp(String atdBinTyp) {
        this.atdBinTyp = atdBinTyp;
    }

    @Basic
    @Column(name = "atd_bin", nullable = true)
    public byte[] getAtdBin() {
        return atdBin;
    }

    public void setAtdBin(byte[] atdBin) {
        this.atdBin = atdBin;
    }

    @Basic
    @Column(name = "doc_generated", nullable = true)
    public String getIsDocGenerated() {
        return isDocGenerated;
    }
    public void setIsDocGenerated(String isDocGenerated) {
        this.isDocGenerated = isDocGenerated;
    }


    @Override
    public String toString() {
        return "FormMAttachment{" +
                "id=" + id +
                ", formMNbr='" + formMNbr + '\'' +
                ", atdType='" + atdType + '\'' +
                ", atdRef='" + atdRef + '\'' +
                ", atdDat='" + atdDat + '\'' +
                ", atdBin=" + Arrays.toString(atdBin) +
                ", atdBinTyp='" + atdBinTyp + '\'' +
                ", tsCnt=" + tsCnt +
                ", isDocGenerated='" + isDocGenerated + '\'' +
                '}';
    }

}
