package com.longbridge.formmfetcher.Entities;

import java.sql.Timestamp;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */

public class FormMalreadyDownloadType {
    private long id;
    private String infFrx;
    private String infVld;
    private String infPfx;
    private String infYea;
    private String infBnkNbr;
    private String infBnkCod;
    private String infBnkBra;
    private String infSer;
    private Timestamp supInfDat;
    private String supnfMsg;
    private String supInfUsr;
    private String supInfOpr;


    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }


    public String getInfFrx() {
        return infFrx;
    }

    public void setInfFrx(String infFrx) {
        this.infFrx = infFrx;
    }


    public String getInfVld() {
        return infVld;
    }

    public void setInfVld(String infVld) {
        this.infVld = infVld;
    }


    public String getInfPfx() {
        return infPfx;
    }

    public void setInfPfx(String infPfx) {
        this.infPfx = infPfx;
    }


    public String getInfYea() {
        return infYea;
    }

    public void setInfYea(String infYea) {
        this.infYea = infYea;
    }


    public String getInfBnkNbr() {
        return infBnkNbr;
    }

    public void setInfBnkNbr(String infBnkNbr) {
        this.infBnkNbr = infBnkNbr;
    }


    public String getInfBnkCod() {
        return infBnkCod;
    }

    public void setInfBnkCod(String infBnkCod) {
        this.infBnkCod = infBnkCod;
    }


    public String getInfBnkBra() {
        return infBnkBra;
    }

    public void setInfBnkBra(String infBnkBra) {
        this.infBnkBra = infBnkBra;
    }


    public String getInfSer() {
        return infSer;
    }
    public void setInfSer(String infSer) {
        this.infSer = infSer;
    }


    public Timestamp getSupInfDat() {
        return supInfDat;
    }

    public void setSupInfDat(Timestamp supInfDat) {
        this.supInfDat = supInfDat;
    }

    public String getSupnfMsg() {
        return supnfMsg;
    }

    public void setSupnfMsg(String supnfMsg) {
        this.supnfMsg = supnfMsg;
    }

    public String getSupInfUsr() {
        return supInfUsr;
    }

    public void setSupInfUsr(String supInfUsr) {
        this.supInfUsr = supInfUsr;
    }

    public String getSupInfOpr() {
        return supInfOpr;
    }

    public void setSupInfOpr(String supInfOpr) {
        this.supInfOpr = supInfOpr;
    }


    @Override
    public String toString() {
        return "FormMalreadyDownloadType{" +
                "id=" + id +
                ", infFrx='" + infFrx + '\'' +
                ", infVld='" + infVld + '\'' +
                ", infPfx='" + infPfx + '\'' +
                ", infYea='" + infYea + '\'' +
                ", infBnkNbr='" + infBnkNbr + '\'' +
                ", infBnkCod='" + infBnkCod + '\'' +
                ", infBnkBra='" + infBnkBra + '\'' +
                ", infSer='" + infSer + '\'' +
                ", supInfDat=" + supInfDat +
                ", supnfMsg='" + supnfMsg + '\'' +
                ", supInfUsr='" + supInfUsr + '\'' +
                ", supInfOpr='" + supInfOpr + '\'' +
                '}';
    }


}
