package com.longbridge.formmfetcher.FinacleEntities;

import java.util.Date;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "ng_tf_form_m_goods_table", schema = "TBAADM", catalog = "")
public class FinFormMItms {
    private String formType;
    private String formNum;
    private String srNum;
    private String bankId;
    private String entityCreFlg;
    private String delFlg;
    private String hsCode;
    private String secPurCode;
    private int noOfPackage;
    private Double fobValue;
    private String descGoods1;
    private String descGoods2;
    private Double freightCharges;
    private String fcyCrncyCode;
    private String rcreUserId;
    private Date rcreTime;
    private String lchgUserId;
    private Date lchgTime;
    private int tsCnt;

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORM_TYPE", nullable = true, length = 255)
    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORM_NUM", nullable = true, length = 255)
    public String getFormNum() {
        return formNum;
    }

    public void setFormNum(String formNum) {
        this.formNum = formNum;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "SR_NUM", nullable = true, length = 255)
    public String getSrNum() {
        return srNum;
    }

    public void setSrNum(String srNum) {
        this.srNum = srNum;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANK_ID", nullable = true, length = 255)
    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "ENTITY_CRE_FLG", nullable = true, length = 255)
    public String getEntityCreFlg() {
        return entityCreFlg;
    }

    public void setEntityCreFlg(String entityCreFlg) {
        this.entityCreFlg = entityCreFlg;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "DEL_FLG", nullable = true, length = 255)
    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "HS_CODE", nullable = true, length = 255)
    public String getHsCode() {
        return hsCode;
    }

    public void setHsCode(String hsCode) {
        this.hsCode = hsCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "SEC_PUR_CODE", nullable = true, length = 255)
    public String getSecPurCode() {
        return secPurCode;
    }

    public void setSecPurCode(String secPurCode) {
        this.secPurCode = secPurCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "NO_OF_PACKAGE", nullable = true, length = 255)
    public int getNoOfPackage() {
        return noOfPackage;
    }

    public void setNoOfPackage(int noOfPackage) {
        this.noOfPackage = noOfPackage;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FOB_VALUE", nullable = true, length = 255)
    public Double getFobValue() {
        return fobValue;
    }

    public void setFobValue(Double fobValue) {
        this.fobValue = fobValue;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "DESC_GOODS1", nullable = true, length = 255)
    public String getDescGoods1() {
        return descGoods1;
    }

    public void setDescGoods1(String descGoods1) {
        this.descGoods1 = descGoods1;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "DESC_GOODS2", nullable = true, length = 255)
    public String getDescGoods2() {
        return descGoods2;
    }

    public void setDescGoods2(String descGoods2) {
        this.descGoods2 = descGoods2;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FREIGHT_CHARGES", nullable = true, length = 255)
    public Double getFreightCharges() {
        return freightCharges;
    }

    public void setFreightCharges(Double freightCharges) {
        this.freightCharges = freightCharges;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FCY_CRNCY_CODE", nullable = true, length = 255)
    public String getFcyCrncyCode() {
        return fcyCrncyCode;
    }

    public void setFcyCrncyCode(String fcyCrncyCode) {
        this.fcyCrncyCode = fcyCrncyCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "RCRE_USER_ID", nullable = true, length = 255)
    public String getRcreUserId() {
        return rcreUserId;
    }

    public void setRcreUserId(String rcreUserId) {
        this.rcreUserId = rcreUserId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "RCRE_TIME", nullable = true, length = 255)
    public Date getRcreTime() {
        return rcreTime;
    }

    public void setRcreTime(Date rcreTime) {
        this.rcreTime = rcreTime;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "LCHG_USER_ID", nullable = true, length = 255)
    public String getLchgUserId() {
        return lchgUserId;
    }

    public void setLchgUserId(String lchgUserId) {
        this.lchgUserId = lchgUserId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "LCHG_TIME", nullable = true, length = 255)
    public Date getLchgTime() {
        return lchgTime;
    }

    public void setLchgTime(Date lchgTime) {
        this.lchgTime = lchgTime;
    }
    @javax.persistence.Id
    @javax.persistence.Basic
    @javax.persistence.Column(name = "TS_CNT", nullable = true, length = 255)
    public int getTsCnt() {
        return tsCnt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinFormMItms that = (FinFormMItms) o;

        if (noOfPackage != that.noOfPackage) return false;
        if (tsCnt != that.tsCnt) return false;
        if (formType != null ? !formType.equals(that.formType) : that.formType != null) return false;
        if (formNum != null ? !formNum.equals(that.formNum) : that.formNum != null) return false;
        if (srNum != null ? !srNum.equals(that.srNum) : that.srNum != null) return false;
        if (bankId != null ? !bankId.equals(that.bankId) : that.bankId != null) return false;
        if (entityCreFlg != null ? !entityCreFlg.equals(that.entityCreFlg) : that.entityCreFlg != null) return false;
        if (delFlg != null ? !delFlg.equals(that.delFlg) : that.delFlg != null) return false;
        if (hsCode != null ? !hsCode.equals(that.hsCode) : that.hsCode != null) return false;
        if (secPurCode != null ? !secPurCode.equals(that.secPurCode) : that.secPurCode != null) return false;
        if (fobValue != null ? !fobValue.equals(that.fobValue) : that.fobValue != null) return false;
        if (descGoods1 != null ? !descGoods1.equals(that.descGoods1) : that.descGoods1 != null) return false;
        if (descGoods2 != null ? !descGoods2.equals(that.descGoods2) : that.descGoods2 != null) return false;
        if (freightCharges != null ? !freightCharges.equals(that.freightCharges) : that.freightCharges != null)
            return false;
        if (fcyCrncyCode != null ? !fcyCrncyCode.equals(that.fcyCrncyCode) : that.fcyCrncyCode != null) return false;
        if (rcreUserId != null ? !rcreUserId.equals(that.rcreUserId) : that.rcreUserId != null) return false;
        if (rcreTime != null ? !rcreTime.equals(that.rcreTime) : that.rcreTime != null) return false;
        if (lchgUserId != null ? !lchgUserId.equals(that.lchgUserId) : that.lchgUserId != null) return false;
        return lchgTime != null ? lchgTime.equals(that.lchgTime) : that.lchgTime == null;
    }

    @Override
    public int hashCode() {
        int result = formType != null ? formType.hashCode() : 0;
        result = 31 * result + (formNum != null ? formNum.hashCode() : 0);
        result = 31 * result + (srNum != null ? srNum.hashCode() : 0);
        result = 31 * result + (bankId != null ? bankId.hashCode() : 0);
        result = 31 * result + (entityCreFlg != null ? entityCreFlg.hashCode() : 0);
        result = 31 * result + (delFlg != null ? delFlg.hashCode() : 0);
        result = 31 * result + (hsCode != null ? hsCode.hashCode() : 0);
        result = 31 * result + (secPurCode != null ? secPurCode.hashCode() : 0);
        result = 31 * result + noOfPackage;
        result = 31 * result + (fobValue != null ? fobValue.hashCode() : 0);
        result = 31 * result + (descGoods1 != null ? descGoods1.hashCode() : 0);
        result = 31 * result + (descGoods2 != null ? descGoods2.hashCode() : 0);
        result = 31 * result + (freightCharges != null ? freightCharges.hashCode() : 0);
        result = 31 * result + (fcyCrncyCode != null ? fcyCrncyCode.hashCode() : 0);
        result = 31 * result + (rcreUserId != null ? rcreUserId.hashCode() : 0);
        result = 31 * result + (rcreTime != null ? rcreTime.hashCode() : 0);
        result = 31 * result + (lchgUserId != null ? lchgUserId.hashCode() : 0);
        result = 31 * result + (lchgTime != null ? lchgTime.hashCode() : 0);
        result = 31 * result + tsCnt;
        return result;
    }

    @Override
    public String toString() {
        return "FinFormMItms{" +
                "formType='" + formType + '\'' +
                ", formNum='" + formNum + '\'' +
                ", srNum='" + srNum + '\'' +
                ", bankId='" + bankId + '\'' +
                ", entityCreFlg='" + entityCreFlg + '\'' +
                ", delFlg='" + delFlg + '\'' +
                ", hsCode='" + hsCode + '\'' +
                ", secPurCode='" + secPurCode + '\'' +
                ", noOfPackage=" + noOfPackage +
                ", fobValue=" + fobValue +
                ", descGoods1='" + descGoods1 + '\'' +
                ", descGoods2='" + descGoods2 + '\'' +
                ", freightCharges=" + freightCharges +
                ", fcyCrncyCode='" + fcyCrncyCode + '\'' +
                ", rcreUserId='" + rcreUserId + '\'' +
                ", rcreTime=" + rcreTime +
                ", lchgUserId='" + lchgUserId + '\'' +
                ", lchgTime=" + lchgTime +
                ", tsCnt=" + tsCnt +
                '}';
    }

    public void setTsCnt(int tsCnt) {
        this.tsCnt = tsCnt;
    }


}
