package com.longbridge.formmfetcher.FinacleEntities;

import java.sql.Timestamp;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "ng_tf_form_m_val_master_table", schema = "TBAADM", catalog = "")
public class FinFormMValMaster {
    private String formType;
    private String formNum;
    private String bankId;
    private String entityCreFlg;
    private String delFlg;
    private String applNum;
    private String formPrefix;
    private String bankCode;
    private String year;
    private String branchCode;
    private String bankersName;
    private String bankersAddr1;
    private String bankersAddr2;
    private String bankersAddr3;
    private String bankersCity;
    private String bankersState;
    private String bankersCntry;
    private String bankersPin;
    private String purposeCode;
    private String regisDate;
    private String expiryDate;
    private Timestamp approvalDate;
    private String statusCode;
    private String custId;
    private String acid;
    private String forexFlg;
    private String chrgAcid;
    private String appliFirstName;
    private String appliLastName;
    private String appliAddr1;
    private String appliAddr2;
    private String appliAddr3;
    private String appliCity;
    private String appliState;
    private String appliCntry;
    private String appliPin;
    private String appliPhoneNum;
    private String appliPassDet;
    private String chrgConFlg;
    private String chrgConAmt;
    private String chrgConCrncyCode;
    private String partyCode;
    private String beniFirstName;
    private String beniLastName;
    private String beniAddr1;
    private String beniAddr2;
    private String beniAddr3;
    private String beniCity;
    private String beniState;
    private String beniCntry;
    private String beniPin;
    private String beniPhoneNum;
    private String beniPassDet;
    private String paymentCode;
    private String transferCode;
    private String originCntryCode;
    private String supplyCntryCode;
    private String transportCode;
    private String portCode;
    private String designateBankCode;
    private String inspAgentCode;
    private String authFirstName;
    private String authLastName;
    private String authAppliFirstName;
    private String authAppliLastName;
    private String remarks;
    private String rarNumber;
    private String rarDate;
    private String generalDesc1;
    private Integer noOfItems;
    private String netWt;
    private String totalFobValue;
    private String totalFriCharges;
    private String totalAnciCharges;
    private String formmFcyAmt;
    private String fcyCrncyCode;
    private String invoiceNum;
    private Timestamp invoiceDate;
    private String excgRateCode;
    private String formmRate;
    private Double formmNairaEqui;
    private String homeCrncyCode;
    private String firstTimeFlg;
    private String rcreUserId;
    private Timestamp rcreTime;
    private String lchgUserId;
    private Timestamp lchgTime;

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORM_TYPE", nullable = true, length = 255)
    public String getFormType() {
        return formType;
    }

    public void setFormType(String formType) {
        this.formType = formType;
    }

    @javax.persistence.Id
    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORM_NUM", nullable = true, length = 255)
    public String getFormNum() {
        return formNum;
    }

    public void setFormNum(String formNum) {
        this.formNum = formNum;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANK_ID", nullable = true, length = 255)
    public String getBankId() {
        return bankId;
    }

    public void setBankId(String bankId) {
        this.bankId = bankId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "ENTITY_CRE_FLG", nullable = true, length = 255)
    public String getEntityCreFlg() {
        return entityCreFlg;
    }

    public void setEntityCreFlg(String entityCreFlg) {
        this.entityCreFlg = entityCreFlg;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "DEL_FLG", nullable = true, length = 255)
    public String getDelFlg() {
        return delFlg;
    }

    public void setDelFlg(String delFlg) {
        this.delFlg = delFlg;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPL_NUM", nullable = true, length = 255)
    public String getApplNum() {
        return applNum;
    }

    public void setApplNum(String applNum) {
        this.applNum = applNum;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORM_PREFIX", nullable = true, length = 255)
    public String getFormPrefix() {
        return formPrefix;
    }

    public void setFormPrefix(String formPrefix) {
        this.formPrefix = formPrefix;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANK_CODE", nullable = true, length = 255)
    public String getBankCode() {
        return bankCode;
    }

    public void setBankCode(String bankCode) {
        this.bankCode = bankCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "YEAR", nullable = true, length = 255)
    public String getYear() {
        return year;
    }

    public void setYear(String year) {
        this.year = year;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BRANCH_CODE", nullable = true, length = 255)
    public String getBranchCode() {
        return branchCode;
    }

    public void setBranchCode(String branchCode) {
        this.branchCode = branchCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANKERS_NAME", nullable = true, length = 255)
    public String getBankersName() {
        return bankersName;
    }

    public void setBankersName(String bankersName) {
        this.bankersName = bankersName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANKERS_ADDR1", nullable = true, length = 255)
    public String getBankersAddr1() {
        return bankersAddr1;
    }

    public void setBankersAddr1(String bankersAddr1) {
        this.bankersAddr1 = bankersAddr1;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANKERS_ADDR2", nullable = true, length = 255)
    public String getBankersAddr2() {
        return bankersAddr2;
    }

    public void setBankersAddr2(String bankersAddr2) {
        this.bankersAddr2 = bankersAddr2;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANKERS_ADDR3", nullable = true, length = 255)
    public String getBankersAddr3() {
        return bankersAddr3;
    }

    public void setBankersAddr3(String bankersAddr3) {
        this.bankersAddr3 = bankersAddr3;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANKERS_CITY", nullable = true, length = 255)
    public String getBankersCity() {
        return bankersCity;
    }

    public void setBankersCity(String bankersCity) {
        this.bankersCity = bankersCity;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANKERS_STATE", nullable = true, length = 255)
    public String getBankersState() {
        return bankersState;
    }

    public void setBankersState(String bankersState) {
        this.bankersState = bankersState;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANKERS_CNTRY", nullable = true, length = 255)
    public String getBankersCntry() {
        return bankersCntry;
    }

    public void setBankersCntry(String bankersCntry) {
        this.bankersCntry = bankersCntry;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BANKERS_PIN", nullable = true, length = 255)
    public String getBankersPin() {
        return bankersPin;
    }

    public void setBankersPin(String bankersPin) {
        this.bankersPin = bankersPin;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "PURPOSE_CODE", nullable = true, length = 255)
    public String getPurposeCode() {
        return purposeCode;
    }

    public void setPurposeCode(String purposeCode) {
        this.purposeCode = purposeCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "REGIS_DATE", nullable = true, length = 255)
    public String getRegisDate() {
        return regisDate;
    }

    public void setRegisDate(String regisDate) {
        this.regisDate = regisDate;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "EXPIRY_DATE", nullable = true, length = 255)
    public String getExpiryDate() {
        return expiryDate;
    }

    public void setExpiryDate(String expiryDate) {
        this.expiryDate = expiryDate;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPROVAL_DATE", nullable = true)
    public Timestamp getApprovalDate() {
        return approvalDate;
    }

    public void setApprovalDate(Timestamp approvalDate) {
        this.approvalDate = approvalDate;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "STATUS_CODE", nullable = true, length = 255)
    public String getStatusCode() {
        return statusCode;
    }

    public void setStatusCode(String statusCode) {
        this.statusCode = statusCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "CUST_ID", nullable = true, length = 255)
    public String getCustId() {
        return custId;
    }

    public void setCustId(String custId) {
        this.custId = custId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "ACID", nullable = true, length = 255)
    public String getAcid() {
        return acid;
    }

    public void setAcid(String acid) {
        this.acid = acid;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FOREX_FLG", nullable = true, length = 255)
    public String getForexFlg() {
        return forexFlg;
    }

    public void setForexFlg(String forexFlg) {
        this.forexFlg = forexFlg;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "CHRG_ACID", nullable = true, length = 255)
    public String getChrgAcid() {
        return chrgAcid;
    }

    public void setChrgAcid(String chrgAcid) {
        this.chrgAcid = chrgAcid;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_FIRST_NAME", nullable = true, length = 255)
    public String getAppliFirstName() {
        return appliFirstName;
    }

    public void setAppliFirstName(String appliFirstName) {
        this.appliFirstName = appliFirstName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_LAST_NAME", nullable = true, length = 255)
    public String getAppliLastName() {
        return appliLastName;
    }

    public void setAppliLastName(String appliLastName) {
        this.appliLastName = appliLastName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_ADDR1", nullable = true, length = 255)
    public String getAppliAddr1() {
        return appliAddr1;
    }

    public void setAppliAddr1(String appliAddr1) {
        this.appliAddr1 = appliAddr1;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_ADDR2", nullable = true, length = 255)
    public String getAppliAddr2() {
        return appliAddr2;
    }

    public void setAppliAddr2(String appliAddr2) {
        this.appliAddr2 = appliAddr2;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_ADDR3", nullable = true, length = 255)
    public String getAppliAddr3() {
        return appliAddr3;
    }

    public void setAppliAddr3(String appliAddr3) {
        this.appliAddr3 = appliAddr3;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_CITY", nullable = true, length = 255)
    public String getAppliCity() {
        return appliCity;
    }

    public void setAppliCity(String appliCity) {
        this.appliCity = appliCity;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_STATE", nullable = true, length = 255)
    public String getAppliState() {
        return appliState;
    }

    public void setAppliState(String appliState) {
        this.appliState = appliState;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_CNTRY", nullable = true, length = 255)
    public String getAppliCntry() {
        return appliCntry;
    }

    public void setAppliCntry(String appliCntry) {
        this.appliCntry = appliCntry;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_PIN", nullable = true, length = 255)
    public String getAppliPin() {
        return appliPin;
    }

    public void setAppliPin(String appliPin) {
        this.appliPin = appliPin;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_PHONE_NUM", nullable = true, length = 255)
    public String getAppliPhoneNum() {
        return appliPhoneNum;
    }

    public void setAppliPhoneNum(String appliPhoneNum) {
        this.appliPhoneNum = appliPhoneNum;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "APPLI_PASS_DET", nullable = true, length = 255)
    public String getAppliPassDet() {
        return appliPassDet;
    }

    public void setAppliPassDet(String appliPassDet) {
        this.appliPassDet = appliPassDet;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "CHRG_CON_FLG", nullable = true, length = 255)
    public String getChrgConFlg() {
        return chrgConFlg;
    }

    public void setChrgConFlg(String chrgConFlg) {
        this.chrgConFlg = chrgConFlg;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "CHRG_CON_AMT", nullable = true, length = 255)
    public String getChrgConAmt() {
        return chrgConAmt;
    }

    public void setChrgConAmt(String chrgConAmt) {
        this.chrgConAmt = chrgConAmt;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "CHRG_CON_CRNCY_CODE", nullable = true, length = 255)
    public String getChrgConCrncyCode() {
        return chrgConCrncyCode;
    }

    public void setChrgConCrncyCode(String chrgConCrncyCode) {
        this.chrgConCrncyCode = chrgConCrncyCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "PARTY_CODE", nullable = true, length = 255)
    public String getPartyCode() {
        return partyCode;
    }

    public void setPartyCode(String partyCode) {
        this.partyCode = partyCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_FIRST_NAME", nullable = true, length = 255)
    public String getBeniFirstName() {
        return beniFirstName;
    }

    public void setBeniFirstName(String beniFirstName) {
        this.beniFirstName = beniFirstName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_LAST_NAME", nullable = true, length = 255)
    public String getBeniLastName() {
        return beniLastName;
    }

    public void setBeniLastName(String beniLastName) {
        this.beniLastName = beniLastName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_ADDR1", nullable = true, length = 255)
    public String getBeniAddr1() {
        return beniAddr1;
    }

    public void setBeniAddr1(String beniAddr1) {
        this.beniAddr1 = beniAddr1;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_ADDR2", nullable = true, length = 255)
    public String getBeniAddr2() {
        return beniAddr2;
    }

    public void setBeniAddr2(String beniAddr2) {
        this.beniAddr2 = beniAddr2;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_ADDR3", nullable = true, length = 255)
    public String getBeniAddr3() {
        return beniAddr3;
    }

    public void setBeniAddr3(String beniAddr3) {
        this.beniAddr3 = beniAddr3;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_CITY", nullable = true, length = 255)
    public String getBeniCity() {
        return beniCity;
    }

    public void setBeniCity(String beniCity) {
        this.beniCity = beniCity;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_STATE", nullable = true, length = 255)
    public String getBeniState() {
        return beniState;
    }

    public void setBeniState(String beniState) {
        this.beniState = beniState;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_CNTRY", nullable = true, length = 255)
    public String getBeniCntry() {
        return beniCntry;
    }

    public void setBeniCntry(String beniCntry) {
        this.beniCntry = beniCntry;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_PIN", nullable = true, length = 255)
    public String getBeniPin() {
        return beniPin;
    }

    public void setBeniPin(String beniPin) {
        this.beniPin = beniPin;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_PHONE_NUM", nullable = true, length = 255)
    public String getBeniPhoneNum() {
        return beniPhoneNum;
    }

    public void setBeniPhoneNum(String beniPhoneNum) {
        this.beniPhoneNum = beniPhoneNum;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "BENI_PASS_DET", nullable = true, length = 255)
    public String getBeniPassDet() {
        return beniPassDet;
    }

    public void setBeniPassDet(String beniPassDet) {
        this.beniPassDet = beniPassDet;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "PAYMENT_CODE", nullable = true, length = 255)
    public String getPaymentCode() {
        return paymentCode;
    }

    public void setPaymentCode(String paymentCode) {
        this.paymentCode = paymentCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "TRANSFER_CODE", nullable = true, length = 255)
    public String getTransferCode() {
        return transferCode;
    }

    public void setTransferCode(String transferCode) {
        this.transferCode = transferCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "ORIGIN_CNTRY_CODE", nullable = true, length = 255)
    public String getOriginCntryCode() {
        return originCntryCode;
    }

    public void setOriginCntryCode(String originCntryCode) {
        this.originCntryCode = originCntryCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "SUPPLY_CNTRY_CODE", nullable = true, length = 255)
    public String getSupplyCntryCode() {
        return supplyCntryCode;
    }

    public void setSupplyCntryCode(String supplyCntryCode) {
        this.supplyCntryCode = supplyCntryCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "TRANSPORT_CODE", nullable = true, length = 255)
    public String getTransportCode() {
        return transportCode;
    }

    public void setTransportCode(String transportCode) {
        this.transportCode = transportCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "PORT_CODE", nullable = true, length = 255)
    public String getPortCode() {
        return portCode;
    }

    public void setPortCode(String portCode) {
        this.portCode = portCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "DESIGNATE_BANK_CODE", nullable = true, length = 255)
    public String getDesignateBankCode() {
        return designateBankCode;
    }

    public void setDesignateBankCode(String designateBankCode) {
        this.designateBankCode = designateBankCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "INSP_AGENT_CODE", nullable = true, length = 255)
    public String getInspAgentCode() {
        return inspAgentCode;
    }

    public void setInspAgentCode(String inspAgentCode) {
        this.inspAgentCode = inspAgentCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "AUTH_FIRST_NAME", nullable = true, length = 255)
    public String getAuthFirstName() {
        return authFirstName;
    }

    public void setAuthFirstName(String authFirstName) {
        this.authFirstName = authFirstName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "AUTH_LAST_NAME", nullable = true, length = 255)
    public String getAuthLastName() {
        return authLastName;
    }

    public void setAuthLastName(String authLastName) {
        this.authLastName = authLastName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "AUTH_APPLI_FIRST_NAME", nullable = true, length = 255)
    public String getAuthAppliFirstName() {
        return authAppliFirstName;
    }

    public void setAuthAppliFirstName(String authAppliFirstName) {
        this.authAppliFirstName = authAppliFirstName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "AUTH_APPLI_LAST_NAME", nullable = true, length = 255)
    public String getAuthAppliLastName() {
        return authAppliLastName;
    }

    public void setAuthAppliLastName(String authAppliLastName) {
        this.authAppliLastName = authAppliLastName;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "REMARKS", nullable = true, length = 255)
    public String getRemarks() {
        return remarks;
    }

    public void setRemarks(String remarks) {
        this.remarks = remarks;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "RAR_NUMBER", nullable = true, length = 255)
    public String getRarNumber() {
        return rarNumber;
    }

    public void setRarNumber(String rarNumber) {
        this.rarNumber = rarNumber;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "RAR_DATE", nullable = true, length = 255)
    public String getRarDate() {
        return rarDate;
    }

    public void setRarDate(String rarDate) {
        this.rarDate = rarDate;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "GENERAL_DESC1", nullable = true, length = 255)
    public String getGeneralDesc1() {
        return generalDesc1;
    }

    public void setGeneralDesc1(String generalDesc1) {
        this.generalDesc1 = generalDesc1;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "NO_OF_ITEMS", nullable = true)
    public Integer getNoOfItems() {
        return noOfItems;
    }

    public void setNoOfItems(Integer noOfItems) {
        this.noOfItems = noOfItems;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "NET_WT", nullable = true, length = 255)
    public String getNetWt() {
        return netWt;
    }

    public void setNetWt(String netWt) {
        this.netWt = netWt;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "TOTAL_FOB_VALUE", nullable = true, length = 255)
    public String getTotalFobValue() {
        return totalFobValue;
    }

    public void setTotalFobValue(String totalFobValue) {
        this.totalFobValue = totalFobValue;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "TOTAL_FRI_CHARGES", nullable = true, length = 255)
    public String getTotalFriCharges() {
        return totalFriCharges;
    }

    public void setTotalFriCharges(String totalFriCharges) {
        this.totalFriCharges = totalFriCharges;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "TOTAL_ANCI_CHARGES", nullable = true, length = 255)
    public String getTotalAnciCharges() {
        return totalAnciCharges;
    }

    public void setTotalAnciCharges(String totalAnciCharges) {
        this.totalAnciCharges = totalAnciCharges;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORMM_FCY_AMT", nullable = true, length = 255)
    public String getFormmFcyAmt() {
        return formmFcyAmt;
    }

    public void setFormmFcyAmt(String formmFcyAmt) {
        this.formmFcyAmt = formmFcyAmt;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FCY_CRNCY_CODE", nullable = true, length = 255)
    public String getFcyCrncyCode() {
        return fcyCrncyCode;
    }

    public void setFcyCrncyCode(String fcyCrncyCode) {
        this.fcyCrncyCode = fcyCrncyCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "INVOICE_NUM", nullable = true, length = 255)
    public String getInvoiceNum() {
        return invoiceNum;
    }

    public void setInvoiceNum(String invoiceNum) {
        this.invoiceNum = invoiceNum;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "INVOICE_DATE", nullable = true)
    public Timestamp getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(Timestamp invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "EXCG_RATE_CODE", nullable = true, length = 255)
    public String getExcgRateCode() {
        return excgRateCode;
    }

    public void setExcgRateCode(String excgRateCode) {
        this.excgRateCode = excgRateCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORMM_RATE", nullable = true, length = 255)
    public String getFormmRate() {
        return formmRate;
    }

    public void setFormmRate(String formmRate) {
        this.formmRate = formmRate;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FORMM_NAIRA_EQUI", nullable = true, precision = 0)
    public Double getFormmNairaEqui() {
        return formmNairaEqui;
    }

    public void setFormmNairaEqui(Double formmNairaEqui) {
        this.formmNairaEqui = formmNairaEqui;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "HOME_CRNCY_CODE", nullable = true, length = 255)
    public String getHomeCrncyCode() {
        return homeCrncyCode;
    }

    public void setHomeCrncyCode(String homeCrncyCode) {
        this.homeCrncyCode = homeCrncyCode;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "FIRST_TIME_FLG", nullable = true, length = 255)
    public String getFirstTimeFlg() {
        return firstTimeFlg;
    }

    public void setFirstTimeFlg(String firstTimeFlg) {
        this.firstTimeFlg = firstTimeFlg;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "RCRE_USER_ID", nullable = true, length = 255)
    public String getRcreUserId() {
        return rcreUserId;
    }

    public void setRcreUserId(String rcreUserId) {
        this.rcreUserId = rcreUserId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "RCRE_TIME", nullable = true)
    public Timestamp getRcreTime() {
        return rcreTime;
    }

    public void setRcreTime(Timestamp rcreTime) {
        this.rcreTime = rcreTime;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "LCHG_USER_ID", nullable = true, length = 255)
    public String getLchgUserId() {
        return lchgUserId;
    }

    public void setLchgUserId(String lchgUserId) {
        this.lchgUserId = lchgUserId;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "LCHG_TIME", nullable = true)
    public Timestamp getLchgTime() {
        return lchgTime;
    }

    public void setLchgTime(Timestamp lchgTime) {
        this.lchgTime = lchgTime;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FinFormMValMaster that = (FinFormMValMaster) o;

        if (formType != null ? !formType.equals(that.formType) : that.formType != null) return false;
        if (formNum != null ? !formNum.equals(that.formNum) : that.formNum != null) return false;
        if (bankId != null ? !bankId.equals(that.bankId) : that.bankId != null) return false;
        if (entityCreFlg != null ? !entityCreFlg.equals(that.entityCreFlg) : that.entityCreFlg != null) return false;
        if (delFlg != null ? !delFlg.equals(that.delFlg) : that.delFlg != null) return false;
        if (applNum != null ? !applNum.equals(that.applNum) : that.applNum != null) return false;
        if (formPrefix != null ? !formPrefix.equals(that.formPrefix) : that.formPrefix != null) return false;
        if (bankCode != null ? !bankCode.equals(that.bankCode) : that.bankCode != null) return false;
        if (year != null ? !year.equals(that.year) : that.year != null) return false;
        if (branchCode != null ? !branchCode.equals(that.branchCode) : that.branchCode != null) return false;
        if (bankersName != null ? !bankersName.equals(that.bankersName) : that.bankersName != null) return false;
        if (bankersAddr1 != null ? !bankersAddr1.equals(that.bankersAddr1) : that.bankersAddr1 != null) return false;
        if (bankersAddr2 != null ? !bankersAddr2.equals(that.bankersAddr2) : that.bankersAddr2 != null) return false;
        if (bankersAddr3 != null ? !bankersAddr3.equals(that.bankersAddr3) : that.bankersAddr3 != null) return false;
        if (bankersCity != null ? !bankersCity.equals(that.bankersCity) : that.bankersCity != null) return false;
        if (bankersState != null ? !bankersState.equals(that.bankersState) : that.bankersState != null) return false;
        if (bankersCntry != null ? !bankersCntry.equals(that.bankersCntry) : that.bankersCntry != null) return false;
        if (bankersPin != null ? !bankersPin.equals(that.bankersPin) : that.bankersPin != null) return false;
        if (purposeCode != null ? !purposeCode.equals(that.purposeCode) : that.purposeCode != null) return false;
        if (regisDate != null ? !regisDate.equals(that.regisDate) : that.regisDate != null) return false;
        if (expiryDate != null ? !expiryDate.equals(that.expiryDate) : that.expiryDate != null) return false;
        if (approvalDate != null ? !approvalDate.equals(that.approvalDate) : that.approvalDate != null) return false;
        if (statusCode != null ? !statusCode.equals(that.statusCode) : that.statusCode != null) return false;
        if (custId != null ? !custId.equals(that.custId) : that.custId != null) return false;
        if (acid != null ? !acid.equals(that.acid) : that.acid != null) return false;
        if (forexFlg != null ? !forexFlg.equals(that.forexFlg) : that.forexFlg != null) return false;
        if (chrgAcid != null ? !chrgAcid.equals(that.chrgAcid) : that.chrgAcid != null) return false;
        if (appliFirstName != null ? !appliFirstName.equals(that.appliFirstName) : that.appliFirstName != null)
            return false;
        if (appliLastName != null ? !appliLastName.equals(that.appliLastName) : that.appliLastName != null)
            return false;
        if (appliAddr1 != null ? !appliAddr1.equals(that.appliAddr1) : that.appliAddr1 != null) return false;
        if (appliAddr2 != null ? !appliAddr2.equals(that.appliAddr2) : that.appliAddr2 != null) return false;
        if (appliAddr3 != null ? !appliAddr3.equals(that.appliAddr3) : that.appliAddr3 != null) return false;
        if (appliCity != null ? !appliCity.equals(that.appliCity) : that.appliCity != null) return false;
        if (appliState != null ? !appliState.equals(that.appliState) : that.appliState != null) return false;
        if (appliCntry != null ? !appliCntry.equals(that.appliCntry) : that.appliCntry != null) return false;
        if (appliPin != null ? !appliPin.equals(that.appliPin) : that.appliPin != null) return false;
        if (appliPhoneNum != null ? !appliPhoneNum.equals(that.appliPhoneNum) : that.appliPhoneNum != null)
            return false;
        if (appliPassDet != null ? !appliPassDet.equals(that.appliPassDet) : that.appliPassDet != null) return false;
        if (chrgConFlg != null ? !chrgConFlg.equals(that.chrgConFlg) : that.chrgConFlg != null) return false;
        if (chrgConAmt != null ? !chrgConAmt.equals(that.chrgConAmt) : that.chrgConAmt != null) return false;
        if (chrgConCrncyCode != null ? !chrgConCrncyCode.equals(that.chrgConCrncyCode) : that.chrgConCrncyCode != null)
            return false;
        if (partyCode != null ? !partyCode.equals(that.partyCode) : that.partyCode != null) return false;
        if (beniFirstName != null ? !beniFirstName.equals(that.beniFirstName) : that.beniFirstName != null)
            return false;
        if (beniLastName != null ? !beniLastName.equals(that.beniLastName) : that.beniLastName != null) return false;
        if (beniAddr1 != null ? !beniAddr1.equals(that.beniAddr1) : that.beniAddr1 != null) return false;
        if (beniAddr2 != null ? !beniAddr2.equals(that.beniAddr2) : that.beniAddr2 != null) return false;
        if (beniAddr3 != null ? !beniAddr3.equals(that.beniAddr3) : that.beniAddr3 != null) return false;
        if (beniCity != null ? !beniCity.equals(that.beniCity) : that.beniCity != null) return false;
        if (beniState != null ? !beniState.equals(that.beniState) : that.beniState != null) return false;
        if (beniCntry != null ? !beniCntry.equals(that.beniCntry) : that.beniCntry != null) return false;
        if (beniPin != null ? !beniPin.equals(that.beniPin) : that.beniPin != null) return false;
        if (beniPhoneNum != null ? !beniPhoneNum.equals(that.beniPhoneNum) : that.beniPhoneNum != null) return false;
        if (beniPassDet != null ? !beniPassDet.equals(that.beniPassDet) : that.beniPassDet != null) return false;
        if (paymentCode != null ? !paymentCode.equals(that.paymentCode) : that.paymentCode != null) return false;
        if (transferCode != null ? !transferCode.equals(that.transferCode) : that.transferCode != null) return false;
        if (originCntryCode != null ? !originCntryCode.equals(that.originCntryCode) : that.originCntryCode != null)
            return false;
        if (supplyCntryCode != null ? !supplyCntryCode.equals(that.supplyCntryCode) : that.supplyCntryCode != null)
            return false;
        if (transportCode != null ? !transportCode.equals(that.transportCode) : that.transportCode != null)
            return false;
        if (portCode != null ? !portCode.equals(that.portCode) : that.portCode != null) return false;
        if (designateBankCode != null ? !designateBankCode.equals(that.designateBankCode) : that.designateBankCode != null)
            return false;
        if (inspAgentCode != null ? !inspAgentCode.equals(that.inspAgentCode) : that.inspAgentCode != null)
            return false;
        if (authFirstName != null ? !authFirstName.equals(that.authFirstName) : that.authFirstName != null)
            return false;
        if (authLastName != null ? !authLastName.equals(that.authLastName) : that.authLastName != null) return false;
        if (authAppliFirstName != null ? !authAppliFirstName.equals(that.authAppliFirstName) : that.authAppliFirstName != null)
            return false;
        if (authAppliLastName != null ? !authAppliLastName.equals(that.authAppliLastName) : that.authAppliLastName != null)
            return false;
        if (remarks != null ? !remarks.equals(that.remarks) : that.remarks != null) return false;
        if (rarNumber != null ? !rarNumber.equals(that.rarNumber) : that.rarNumber != null) return false;
        if (rarDate != null ? !rarDate.equals(that.rarDate) : that.rarDate != null) return false;
        if (generalDesc1 != null ? !generalDesc1.equals(that.generalDesc1) : that.generalDesc1 != null) return false;
        if (noOfItems != null ? !noOfItems.equals(that.noOfItems) : that.noOfItems != null) return false;
        if (netWt != null ? !netWt.equals(that.netWt) : that.netWt != null) return false;
        if (totalFobValue != null ? !totalFobValue.equals(that.totalFobValue) : that.totalFobValue != null)
            return false;
        if (totalFriCharges != null ? !totalFriCharges.equals(that.totalFriCharges) : that.totalFriCharges != null)
            return false;
        if (totalAnciCharges != null ? !totalAnciCharges.equals(that.totalAnciCharges) : that.totalAnciCharges != null)
            return false;
        if (formmFcyAmt != null ? !formmFcyAmt.equals(that.formmFcyAmt) : that.formmFcyAmt != null) return false;
        if (fcyCrncyCode != null ? !fcyCrncyCode.equals(that.fcyCrncyCode) : that.fcyCrncyCode != null) return false;
        if (invoiceNum != null ? !invoiceNum.equals(that.invoiceNum) : that.invoiceNum != null) return false;
        if (invoiceDate != null ? !invoiceDate.equals(that.invoiceDate) : that.invoiceDate != null) return false;
        if (excgRateCode != null ? !excgRateCode.equals(that.excgRateCode) : that.excgRateCode != null) return false;
        if (formmRate != null ? !formmRate.equals(that.formmRate) : that.formmRate != null) return false;
        if (formmNairaEqui != null ? !formmNairaEqui.equals(that.formmNairaEqui) : that.formmNairaEqui != null)
            return false;
        if (homeCrncyCode != null ? !homeCrncyCode.equals(that.homeCrncyCode) : that.homeCrncyCode != null)
            return false;
        if (firstTimeFlg != null ? !firstTimeFlg.equals(that.firstTimeFlg) : that.firstTimeFlg != null) return false;
        if (rcreUserId != null ? !rcreUserId.equals(that.rcreUserId) : that.rcreUserId != null) return false;
        if (rcreTime != null ? !rcreTime.equals(that.rcreTime) : that.rcreTime != null) return false;
        if (lchgUserId != null ? !lchgUserId.equals(that.lchgUserId) : that.lchgUserId != null) return false;
        return lchgTime != null ? lchgTime.equals(that.lchgTime) : that.lchgTime == null;
    }

    @Override
    public int hashCode() {
        int result = formType != null ? formType.hashCode() : 0;
        result = 31 * result + (formNum != null ? formNum.hashCode() : 0);
        result = 31 * result + (bankId != null ? bankId.hashCode() : 0);
        result = 31 * result + (entityCreFlg != null ? entityCreFlg.hashCode() : 0);
        result = 31 * result + (delFlg != null ? delFlg.hashCode() : 0);
        result = 31 * result + (applNum != null ? applNum.hashCode() : 0);
        result = 31 * result + (formPrefix != null ? formPrefix.hashCode() : 0);
        result = 31 * result + (bankCode != null ? bankCode.hashCode() : 0);
        result = 31 * result + (year != null ? year.hashCode() : 0);
        result = 31 * result + (branchCode != null ? branchCode.hashCode() : 0);
        result = 31 * result + (bankersName != null ? bankersName.hashCode() : 0);
        result = 31 * result + (bankersAddr1 != null ? bankersAddr1.hashCode() : 0);
        result = 31 * result + (bankersAddr2 != null ? bankersAddr2.hashCode() : 0);
        result = 31 * result + (bankersAddr3 != null ? bankersAddr3.hashCode() : 0);
        result = 31 * result + (bankersCity != null ? bankersCity.hashCode() : 0);
        result = 31 * result + (bankersState != null ? bankersState.hashCode() : 0);
        result = 31 * result + (bankersCntry != null ? bankersCntry.hashCode() : 0);
        result = 31 * result + (bankersPin != null ? bankersPin.hashCode() : 0);
        result = 31 * result + (purposeCode != null ? purposeCode.hashCode() : 0);
        result = 31 * result + (regisDate != null ? regisDate.hashCode() : 0);
        result = 31 * result + (expiryDate != null ? expiryDate.hashCode() : 0);
        result = 31 * result + (approvalDate != null ? approvalDate.hashCode() : 0);
        result = 31 * result + (statusCode != null ? statusCode.hashCode() : 0);
        result = 31 * result + (custId != null ? custId.hashCode() : 0);
        result = 31 * result + (acid != null ? acid.hashCode() : 0);
        result = 31 * result + (forexFlg != null ? forexFlg.hashCode() : 0);
        result = 31 * result + (chrgAcid != null ? chrgAcid.hashCode() : 0);
        result = 31 * result + (appliFirstName != null ? appliFirstName.hashCode() : 0);
        result = 31 * result + (appliLastName != null ? appliLastName.hashCode() : 0);
        result = 31 * result + (appliAddr1 != null ? appliAddr1.hashCode() : 0);
        result = 31 * result + (appliAddr2 != null ? appliAddr2.hashCode() : 0);
        result = 31 * result + (appliAddr3 != null ? appliAddr3.hashCode() : 0);
        result = 31 * result + (appliCity != null ? appliCity.hashCode() : 0);
        result = 31 * result + (appliState != null ? appliState.hashCode() : 0);
        result = 31 * result + (appliCntry != null ? appliCntry.hashCode() : 0);
        result = 31 * result + (appliPin != null ? appliPin.hashCode() : 0);
        result = 31 * result + (appliPhoneNum != null ? appliPhoneNum.hashCode() : 0);
        result = 31 * result + (appliPassDet != null ? appliPassDet.hashCode() : 0);
        result = 31 * result + (chrgConFlg != null ? chrgConFlg.hashCode() : 0);
        result = 31 * result + (chrgConAmt != null ? chrgConAmt.hashCode() : 0);
        result = 31 * result + (chrgConCrncyCode != null ? chrgConCrncyCode.hashCode() : 0);
        result = 31 * result + (partyCode != null ? partyCode.hashCode() : 0);
        result = 31 * result + (beniFirstName != null ? beniFirstName.hashCode() : 0);
        result = 31 * result + (beniLastName != null ? beniLastName.hashCode() : 0);
        result = 31 * result + (beniAddr1 != null ? beniAddr1.hashCode() : 0);
        result = 31 * result + (beniAddr2 != null ? beniAddr2.hashCode() : 0);
        result = 31 * result + (beniAddr3 != null ? beniAddr3.hashCode() : 0);
        result = 31 * result + (beniCity != null ? beniCity.hashCode() : 0);
        result = 31 * result + (beniState != null ? beniState.hashCode() : 0);
        result = 31 * result + (beniCntry != null ? beniCntry.hashCode() : 0);
        result = 31 * result + (beniPin != null ? beniPin.hashCode() : 0);
        result = 31 * result + (beniPhoneNum != null ? beniPhoneNum.hashCode() : 0);
        result = 31 * result + (beniPassDet != null ? beniPassDet.hashCode() : 0);
        result = 31 * result + (paymentCode != null ? paymentCode.hashCode() : 0);
        result = 31 * result + (transferCode != null ? transferCode.hashCode() : 0);
        result = 31 * result + (originCntryCode != null ? originCntryCode.hashCode() : 0);
        result = 31 * result + (supplyCntryCode != null ? supplyCntryCode.hashCode() : 0);
        result = 31 * result + (transportCode != null ? transportCode.hashCode() : 0);
        result = 31 * result + (portCode != null ? portCode.hashCode() : 0);
        result = 31 * result + (designateBankCode != null ? designateBankCode.hashCode() : 0);
        result = 31 * result + (inspAgentCode != null ? inspAgentCode.hashCode() : 0);
        result = 31 * result + (authFirstName != null ? authFirstName.hashCode() : 0);
        result = 31 * result + (authLastName != null ? authLastName.hashCode() : 0);
        result = 31 * result + (authAppliFirstName != null ? authAppliFirstName.hashCode() : 0);
        result = 31 * result + (authAppliLastName != null ? authAppliLastName.hashCode() : 0);
        result = 31 * result + (remarks != null ? remarks.hashCode() : 0);
        result = 31 * result + (rarNumber != null ? rarNumber.hashCode() : 0);
        result = 31 * result + (rarDate != null ? rarDate.hashCode() : 0);
        result = 31 * result + (generalDesc1 != null ? generalDesc1.hashCode() : 0);
        result = 31 * result + (noOfItems != null ? noOfItems.hashCode() : 0);
        result = 31 * result + (netWt != null ? netWt.hashCode() : 0);
        result = 31 * result + (totalFobValue != null ? totalFobValue.hashCode() : 0);
        result = 31 * result + (totalFriCharges != null ? totalFriCharges.hashCode() : 0);
        result = 31 * result + (totalAnciCharges != null ? totalAnciCharges.hashCode() : 0);
        result = 31 * result + (formmFcyAmt != null ? formmFcyAmt.hashCode() : 0);
        result = 31 * result + (fcyCrncyCode != null ? fcyCrncyCode.hashCode() : 0);
        result = 31 * result + (invoiceNum != null ? invoiceNum.hashCode() : 0);
        result = 31 * result + (invoiceDate != null ? invoiceDate.hashCode() : 0);
        result = 31 * result + (excgRateCode != null ? excgRateCode.hashCode() : 0);
        result = 31 * result + (formmRate != null ? formmRate.hashCode() : 0);
        result = 31 * result + (formmNairaEqui != null ? formmNairaEqui.hashCode() : 0);
        result = 31 * result + (homeCrncyCode != null ? homeCrncyCode.hashCode() : 0);
        result = 31 * result + (firstTimeFlg != null ? firstTimeFlg.hashCode() : 0);
        result = 31 * result + (rcreUserId != null ? rcreUserId.hashCode() : 0);
        result = 31 * result + (rcreTime != null ? rcreTime.hashCode() : 0);
        result = 31 * result + (lchgUserId != null ? lchgUserId.hashCode() : 0);
        result = 31 * result + (lchgTime != null ? lchgTime.hashCode() : 0);
        return result;
    }

}
