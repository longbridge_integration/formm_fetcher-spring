package com.longbridge.formmfetcher.FinacleEntities;

import java.util.Arrays;

/**
 * Created by LB-PRJ-020 on 5/17/2017.
 */
@javax.persistence.Entity
@javax.persistence.Table(name = "form_mattachment", schema = "TBAADM", catalog = "")
public class FormMAttachment {
    private long id;
    private String formMNbr;
    private String atdType;
    private String atdRef;
    private String atdDat;
    private byte[] atdBin;
    private String atdBinTyp;
    private long tsCnt;
    private String isDocGenerated;

    @javax.persistence.Id
    @javax.persistence.Column(name = "id", nullable = false)
    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "ts_cnt", nullable = true, length = 255)
    public long getTsCnt() {
        return tsCnt;
    }

    public void setTsCnt(long tsCnt) {
        this.tsCnt = tsCnt;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "atd_type", nullable = true, length = 255)
    public String getAtdType() {
        return atdType;
    }

    public void setAtdType(String atdType) {
        this.atdType = atdType;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "form_numb", nullable = true, length = 255)
    public String getFormMNbr() {
        return formMNbr;
    }

    public void setFormMNbr(String formMNbr) {
        this.formMNbr = formMNbr;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "atd_ref", nullable = true, length = 255)
    public String getAtdRef() {
        return atdRef;
    }

    public void setAtdRef(String atdRef) {
        this.atdRef = atdRef;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "atd_dat", nullable = true, length = 255)
    public String getAtdDat() {
        return atdDat;
    }

    public void setAtdDat(String atdDat) {
        this.atdDat = atdDat;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "atd_bin_typ", nullable = true, length = 255)
    public String getAtdBinTyp() {
        return atdBinTyp;
    }

    public void setAtdBinTyp(String atdBinTyp) {
        this.atdBinTyp = atdBinTyp;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "atd_bin", nullable = true)
    public byte[] getAtdBin() {
        return atdBin;
    }

    public void setAtdBin(byte[] atdBin) {
        this.atdBin = atdBin;
    }

    @javax.persistence.Basic
    @javax.persistence.Column(name = "doc_generated", nullable = true)
    public String getIsDocGenerated() {
        return isDocGenerated;
    }
    public void setIsDocGenerated(String isDocGenerated) {
        this.isDocGenerated = isDocGenerated;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        FormMAttachment that = (FormMAttachment) o;

        if (id != that.id) return false;
        if (tsCnt != that.tsCnt) return false;
        if (formMNbr != null ? !formMNbr.equals(that.formMNbr) : that.formMNbr != null) return false;
        if (atdType != null ? !atdType.equals(that.atdType) : that.atdType != null) return false;
        if (atdRef != null ? !atdRef.equals(that.atdRef) : that.atdRef != null) return false;
        if (atdDat != null ? !atdDat.equals(that.atdDat) : that.atdDat != null) return false;
        if (!Arrays.equals(atdBin, that.atdBin)) return false;
        if (atdBinTyp != null ? !atdBinTyp.equals(that.atdBinTyp) : that.atdBinTyp != null) return false;
        return isDocGenerated != null ? isDocGenerated.equals(that.isDocGenerated) : that.isDocGenerated == null;
    }

    @Override
    public int hashCode() {
        int result = (int) (id ^ (id >>> 32));
        result = 31 * result + (formMNbr != null ? formMNbr.hashCode() : 0);
        result = 31 * result + (atdType != null ? atdType.hashCode() : 0);
        result = 31 * result + (atdRef != null ? atdRef.hashCode() : 0);
        result = 31 * result + (atdDat != null ? atdDat.hashCode() : 0);
        result = 31 * result + Arrays.hashCode(atdBin);
        result = 31 * result + (atdBinTyp != null ? atdBinTyp.hashCode() : 0);
        result = 31 * result + (int) (tsCnt ^ (tsCnt >>> 32));
        result = 31 * result + (isDocGenerated != null ? isDocGenerated.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "FormMAttachment{" +
                "id=" + id +
                ", formMNbr='" + formMNbr + '\'' +
                ", atdType='" + atdType + '\'' +
                ", atdRef='" + atdRef + '\'' +
                ", atdDat='" + atdDat + '\'' +
                ", atdBin=" + Arrays.toString(atdBin) +
                ", atdBinTyp='" + atdBinTyp + '\'' +
                ", tsCnt=" + tsCnt +
                ", isDocGenerated='" + isDocGenerated + '\'' +
                '}';
    }

}
