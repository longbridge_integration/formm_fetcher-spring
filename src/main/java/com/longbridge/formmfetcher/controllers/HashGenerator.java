package com.longbridge.formmfetcher.controllers;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.util.Scanner;

/**
 * Created by LB-PRJ-020 on 5/25/2017.
 */
@Component
public class HashGenerator {
    public String getHash(String $0s) throws NoSuchAlgorithmException {
        File file = new File("../privKey.txt");
        String $1 = null;
        Scanner scanner= null;
        try {
            scanner = new Scanner(file);
            $1 = scanner.nextLine();
            $0s = scanner.nextLine();
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        try {
            $1 = new StrongAES().getPassDecr($1,$0s);
        } catch (NoSuchPaddingException e) {
            e.printStackTrace();
        } catch (BadPaddingException e) {
            e.printStackTrace();
        } catch (IllegalBlockSizeException e) {
            e.printStackTrace();
        } catch (InvalidKeyException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return $1;
    }
}
