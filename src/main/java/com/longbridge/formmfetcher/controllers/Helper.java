package com.longbridge.formmfetcher.controllers;

import com.longbridge.formmfetcher.Entities.*;
import com.longbridge.formmfetcher.Entities.FinacleEntities.FinFormMItms;
import com.longbridge.formmfetcher.Entities.FinacleEntities.FinFormMMaster;
import com.longbridge.formmfetcher.Entities.FinacleEntities.RefCodeTable;
import com.wsdlClient;
import com.wsdl.*;

import java.sql.Timestamp;
import java.text.ParseException;
import java.util.Date;
import java.util.List;

/**
 * Created by DELL on 8/12/2016.
 */
public class Helper extends wsdlClient {

    static boolean  flgCntrl ;
    private static FormM formM;
    private static FormMalreadyDownloadType formMalreadyDownloadType;
    private static FinFormMValMaster finFormMValMaster;
    private static List<FinFormMItms> finFormMItms;
    private static FinFormMMaster finFormMMaster;
    private static FinFormMItms formMItms;
    private static FormMAttachment formMAttachment;
    private static FinFormMMasterAudit finFormMMasterAudit;

    public static void generateEntities(FormMType holder,String formStatus){
        flgCntrl = true;
        formM = generateFormM(holder);
        if(formM==null){
            logger.error("FormM is null Skipping Form");
            return;
        }
        if(formStatus.equals("VALIDATED")){
            generateFinFormMValMaster(formM);
        }
        generateFinFormMMaster(formM);
        generateFormMItms(formM,holder.getITMS());
        generateAttchMnt(formM,holder.getATDS());
        generateFinFormMMasterAudit(formM,holder.getSUPINF());

    }

    public static FormMalreadyDownloadType generateEntities(FormMAlreadyDownloadedType holder,String formStatus){

        INFType inf = holder.getINF();
        formMalreadyDownloadType = new FormMalreadyDownloadType();
        formMalreadyDownloadType.setInfFrx(safeString(inf.getFRX()));
        formMalreadyDownloadType.setInfYea(safeString(inf.getYEA()));
        formMalreadyDownloadType.setInfVld(safeString(inf.getVLD()));

        try {formMalreadyDownloadType.setInfPfx(safeString(inf.getPFX().name()));}
        catch (NullPointerException exc) {formMalreadyDownloadType.setInfPfx("");}

        try {formMalreadyDownloadType.setInfBnkNbr (safeString(inf.getBNK().getCOD()));}
        catch (NullPointerException exc) {formMalreadyDownloadType.setInfBnkCod("");}

        try {formMalreadyDownloadType.setInfBnkCod(safeString(inf.getBNK().getCODE()));}
        catch (NullPointerException exc) {formMalreadyDownloadType.setInfBnkCod("");}

        try {formMalreadyDownloadType.setInfBnkBra(safeString(inf.getBNK().getBRA()));}
        catch (NullPointerException exc) {formMalreadyDownloadType.setInfBnkBra("");}

        try {formMalreadyDownloadType.setInfSer(safeString(inf.getSER()));}
        catch (NullPointerException exc) { formMalreadyDownloadType.setInfSer("");}

        generateFinFormMMasterAudit(formMalreadyDownloadType,holder.getSUPINF());

        return formMalreadyDownloadType;
    }

    public static void generateFormMItms(FormM temp, List<ITMType> itms){

        logger.info("Query: findByFormNum via formM using formNumber "+temp.getFrmNbr());
        finFormMItms = finFormMItmsRepository.findByFormNum(temp.getFrmNbr());
        if(finFormMItms.size() == itms.size()){
            logger.info("FrmMItems Already exists");
            return;
        }
        int TsCnt = finFormMItmsRepository.findFirstByOrderByTsCntDesc() == null?0:finFormMItmsRepository.findFirstByOrderByTsCntDesc().getTsCnt();
        boolean first = true;
        for (ITMType itm : itms) {
            if(finFormMItmsRepository.findByFormNumAndSrNumAndBankIdAndFormType(safeString(temp.getFrmNbr()),safeString("" + itm.getRNK()),"01","M")!=null)
            {
                first = false;
                logger.error("FormM Item(s) Already exists skipping this row for form "+safeString(temp.getFrmNbr()));
                continue;
            }
            if (first && finFormMItms.size()==0) {
                try {temp.setItmCtyOrgCod(safeString(itm.getCTY().getORG().getCOD()));}
                catch (NullPointerException exc) {temp.setItmCtyOrgCod("");}
                first = false;
            }
            formMItms = new FinFormMItms();
            formMItms.setFormType("M");
            formMItms.setFormNum(safeString(temp.getFrmNbr()));
            formMItms.setSrNum(safeString("" + itm.getRNK()));
            formMItms.setBankId("01");
            formMItms.setEntityCreFlg("Y");
            formMItms.setDelFlg("N");
            formMItms.setHsCode(safeString(itm.getHSC()));
            formMItms.setSecPurCode(safeString(itm.getSEP().getCOD()));
            formMItms.setNoOfPackage(itm.getPKG().getNBR());
            try {
                formMItms.setFobValue(Double.valueOf(itm.getFOB().getVAL()));
            } catch (NullPointerException exc) {
                formMItms.setFobValue(0.0);
            }
            formMItms.setDescGoods1(safeString(itm.getDSC()));
            formMItms.setDescGoods2(safeString(itm.getDSC()));
            try {
                formMItms.setFreightCharges(Double.valueOf(safeString(itm.getFRG())));
            } catch (NullPointerException exc) {
                formMItms.setFreightCharges(0.0);
            }
            formMItms.setFcyCrncyCode(safeString(temp.getGdsValCurCod()));
            formMItms.setRcreUserId("CUSTOM");
            formMItms.setRcreTime(new Date());
            formMItms.setLchgUserId("CUSTOM");
            formMItms.setLchgTime(new Date());
            formMItms.setTsCnt(TsCnt+1);
            finFormMItmsRepository.save(formMItms);
            logger.info("******FinFormMItems Saved  {}"+formMItms.toString());
            TsCnt++;
        }
    }

    public static FormM generateFormM(FormMType holder){
        INFType inf = holder.getINF();
        ENDType end = holder.getEND();
        APPType app = holder.getAPP();
        BENType ben = holder.getBEN();
        GDSType gds = holder.getGDS();
        TSPType tsp = holder.getTSP();

        String applNumber="";
        String formMNumber="";

        try{applNumber = (safeString(inf.getPFX().name()));}
        catch(NullPointerException  e){logger.error("Form Prefix is Null");}

        try{formMNumber = applNumber + safeString(inf.getBNK().getCOD());}
        catch(NullPointerException e) {logger.error("Form Number is Null");}

        if(formMNumber.equals("BA")||formMNumber.length()<3){
            logger.error("Form does not have a formNumber skipping insert into FormM Table");
            return null;
        }


        long lstIndex = formMRepository.findFirstByOrderByIdDesc() ==null?0:formMRepository.findFirstByOrderByIdDesc().getId();
        formM = new FormM();
        formM.setId(lstIndex+1);
        formM.setFrmNbr(formMNumber);
        formM.setAppSta(safeString(app.getSTA()));
        formM.setInfFrx(safeString(inf.getFRX()));
        formM.setInfYea(safeString(inf.getYEA()));
        formM.setInfSer(safeString(inf.getSER()));
        formM.setInfVld(safeString(inf.getVLD())) ;
        formM.setInfTyp("M");
        formM.setAppTin(safeString(app.getTIN()));
        formM.setAppRcn(safeString(app.getRCN()));
        formM.setAppNepc(safeString(app.getNEPC()));
        formM.setAppFax(safeString(app.getFAX()));
        formM.setAppMai(safeString(app.getMAI()));
        formM.setBenFax(safeString(ben.getFAX()));
        formM.setBenMai(safeString(ben.getMAI()));
        formM.setBenByOrd(safeString(ben.getORD()));
        formM.setInfFrx(safeString(inf.getFRX()));
        formM.setAppNam(safeString(app.getNAM()));
        formM.setAppAdr(safeString(app.getADR()));
        formM.setAppCit(safeString(app.getCIT()));
        formM.setAppTel(safeString(app.getTEL()));
        formM.setAppPas(safeString(app.getPAS()));
        formM.setBenNam(safeString(ben.getNAM()));
        formM.setBenAdr(safeString(ben.getADR()));
        formM.setBenTel(safeString(ben.getTEL()));
        formM.setInfPfx(applNumber);


        try{formM.setInfBnkNbr(safeString(inf.getBNK().getCOD()));}
        catch(NullPointerException e) {formM.setInfBnkNbr("");}

        try {formM.setInfBnkCod(safeString(inf.getBNK().getCODE()));}
        catch(NullPointerException e) {formM.setInfBnkCod("");}

        try{formM.setInfBnkBra(safeString(inf.getBNK().getBRA()));}
        catch(NullPointerException e) {formM.setInfBnkBra("");}

        try{formM.setGdsDesCod(safeString(gds.getDIS().getCOD()));}
        catch(NullPointerException e) {formM.setGdsDesCod("");}

        try{formM.setGdsCuoCod(safeString(gds.getCUO().getCOD()));}
        catch(NullPointerException e) {formM.setGdsCuoCod("");}

        try{formM.setGdsValInc(safeString(gds.getVAL().getINC()));}
        catch(NullPointerException  e){formM.setGdsValInc("");}

        try{formM.setGdsValSrcCod(safeString(gds.getVAL().getSRC().getCOD()));}
        catch(NullPointerException e){formM.setGdsValSrcCod("");}

        try{formM.setGdsPayDat(safeString(gds.getPAY().getDAT()));}
        catch(NullPointerException e){formM.setGdsPayDat("");}

        try{formM.setGdsTodCod(safeString(gds.getTOD().getCOD()));}
        catch(NullPointerException e){formM.setGdsTodCod("");}

        try{formM.setTspShpDat(safeString(tsp.getSHP().getDAT()));}
        catch(NullPointerException e){formM.setTspShpDat("");}

        try{formM.setTspAirLin(safeString(tsp.getAIR().getLIN()));}
        catch(NullPointerException e){formM.setTspAirLin("");}

        try{formM.setTspAirTck(safeString(tsp.getAIR().getTCK()));}
        catch(NullPointerException e){formM.setTspAirTck("");}

        try{formM.setSta(safeString(holder.getSTA().name()));}
        catch(NullPointerException e){formM.setSta("");}

        try{formM.setBenCtyCode(safeString(ben.getCTY().getCOD()));}
        catch(NullPointerException e){formM.setBenCtyCode("");}

        try{formM.setGdsMopCod(safeString(gds.getMOP().getCOD()));}
        catch(NullPointerException e){formM.setGdsMopCod("");}

        try{formM.setGdsTrfCod(safeString(gds.getTRF().getCOD()));}
        catch(NullPointerException e){formM.setGdsTrfCod("");}

        try{formM.setGdsCtySupCod(safeString(gds.getCTY().getSUP().getCOD()));}
        catch(NullPointerException e){formM.setGdsCtySupCod("");}

        try{formM.setGdsMotCod(safeString(gds.getMOT().getCOD()));}
        catch(NullPointerException e){formM.setGdsMotCod("");}

        try{formM.setBnkCod(safeString(holder.getBNK().getCOD()));}
        catch(NullPointerException e){formM.setBnkCod("");}

        try{formM.setScaCod(safeString(holder.getSCA().getCOD()));}
        catch(NullPointerException e){formM.setScaCod("");}

        try{formM.setEndAppNam(safeString(end.getAPP().getNAM()));}
        catch(NullPointerException e){formM.setEndAppNam("");}

        try{formM.setEndAutNam(safeString(end.getAUT().getNAM()));}
        catch(NullPointerException e){formM.setEndAutNam("");}

        try{formM.setGdsGenDsc(safeString(gds.getGEN().getDSC()));}
        catch(NullPointerException e){formM.setGdsGenDsc("");}

        try{formM.setGdsGenWgt(safeString(gds.getGEN().getWGT()));}
        catch(NullPointerException e){formM.setGdsGenWgt("");}

        try{formM.setGdsGenTit(gds.getGEN().getTIT());}
        catch(NullPointerException e){formM.setGdsGenTit(0);}

        try{formM.setGdsValFob(safeString(gds.getVAL().getFOB()));}
        catch(NullPointerException e){formM.setGdsValFob("");}

        try{formM.setGdsValFrg(safeString(gds.getVAL().getFRG()));}
        catch(NullPointerException e){formM.setGdsValFrg("");}

        try{formM.setGdsValTac(safeString(gds.getVAL().getTAC()));}
        catch(NullPointerException e){formM.setGdsValTac("");}

        try{formM.setGdsValTcfVal(safeString(gds.getVAL().getTCF().getVAL()));}
        catch(NullPointerException e){formM.setGdsValTcfVal("");}

        try{formM.setGdsValCurCod(safeString(gds.getVAL().getCUR().getCOD()));}
        catch(NullPointerException e){formM.setGdsValCurCod("");}

        try{formM.setGdsProInv(safeString(gds.getPRO().getINV()));}
        catch(NullPointerException e){formM.setGdsProInv("");}

        try{formM.setGdsValExc(safeString(gds.getVAL().getEXC()));}
        catch(NullPointerException e){formM.setGdsValExc("");}
        
        try{formM.setEndAppDat(new Timestamp(stringToDate(end.getAPP().getDAT()).getTime()));}
        catch(NullPointerException e){formM.setEndAppDat(null);}

        try{formM.setEndAutDat(new Timestamp(stringToDate(end.getAUT().getDAT()).getTime()));}
        catch(NullPointerException e){formM.setEndAutDat(null);}

        try{formM.setGdsProDat(new Timestamp(stringToDate(gds.getPRO().getDAT()).getTime()));}
        catch(NullPointerException e){formM.setGdsProDat(null);}
        logger.info("formM content is: "+formM.toString());

        logger.info("Query: findByFormNbr via generateFormM method using FormNumber "+formMNumber);
        if(formMRepository.findByFrmNbr(formMNumber)!=null){
            logger.info("FormM Already Exists for: "+formMNumber);
            return formM;
        }
        formMRepository.save(formM);
        logger.info("FormM has been Saved: "+formMNumber);
        return formM;
    }

    public static String generateAttchMnt(FormM formM,List<AttachmentType> atds) {
        long tsCnt = 1;
        if (atds != null) {
            for (AttachmentType atd : atds) {
                formMAttachment = new FormMAttachment();
                formMAttachment.setFormMNbr(formM.getFrmNbr());
                formMAttachment.setAtdBinTyp(safeString(atd.getBINTYP()));
                formMAttachment.setAtdDat(safeString(atd.getDAT().toString()));
                formMAttachment.setAtdRef(safeString(atd.getREF()));
                formMAttachment.setAtdType(safeString(atd.getTYP()));
                byte[] docinBytes = atd.getBIN();
                formMAttachment.setIsDocGenerated("N");
                formMAttachment.setAtdBin(docinBytes);
                    //Below code is to create documents
//                    String folderName = formMAttachment.getFormMNbr();
//                    String fileName = formMAttachment.getAtdType().replace(" ","");
//                    String fileIndex = formMAttachment.getAtdRef().replace("/","_");
//                    String fileType = formMAttachment.getAtdBinTyp();
//                    String fileDirectory = "C://User//Public//Documents//"+folderName+"//"+fileName+"//";
//                    //BASE64Decoder decoder = new BASE64Decoder();
//                    //String fileval = new String(docinBytes);
//                    byte[]decodedBytes;boolean isgood = false;
//                    try {
//                        //decodedBytes = decoder.decodeBuffer(fileval);
//                        File fileDirToSave = new File(fileDirectory);
//                        if(!fileDirToSave.exists()){
//                            if(fileDirToSave.mkdirs()) {
//                                isgood = true;
//                            }
//                        }
//                        if(fileDirToSave.exists()||isgood){
//                            String fileName2 = fileDirectory+fileIndex+"."+fileType;
//                            File fileToSave = new File(fileName2);
//                            FileOutputStream fop = new FileOutputStream(fileToSave);//
//                            fop.write(docinBytes);
//                            fop.flush();
//                            fop.close();
//                        }
//                    } catch (IOException e) {
//                        e.printStackTrace();
//                    }
                    //End of code to create documents

                long cnt = formMAttchRepository.findFirstByOrderByIdDesc() ==null?0:formMAttchRepository.findFirstByOrderByIdDesc().getId();
                ++cnt;
                logger.info("Query: findByAttchProp using props for formNumber  "+formMAttachment.getFormMNbr());
                FormMAttachment checker = formMAttchRepository.findByAtdBinTypAndAtdRefAndAtdTypeAndAtdDatAndFormMNbr(formMAttachment.getAtdBinTyp(),
                        formMAttachment.getAtdRef(),formMAttachment.getAtdType(),formMAttachment.getAtdDat(),formMAttachment.getFormMNbr());
                if(checker==null){
                    formMAttachment.setId(cnt);
                    formMAttachment.setTsCnt(tsCnt);
                    formMAttchRepository.save(formMAttachment);
                    logger.info("FormM Attachment Inserted ");
                    tsCnt = tsCnt+1;
                }else if(checker!=null && checker.getAtdBin()==null){
                    checker.setAtdBin(formMAttachment.getAtdBin());
                    formMAttchRepository.save(checker);
                    logger.info("FormM Attachment Updated ");
                }else{logger.info("Attachment Already Exists");}
            }
        }
        return null;
    }

    public static String generateFinFormMMaster(FormM formM){
        String returnedStr="";

        logger.info("Query: findByFormNumAndFormTypeAndBankId via generateFinFormMaster using form Number "+formM.getFrmNbr());
        finFormMMaster = finFormMMasterRepository.
                findByFormNumAndFormTypeAndBankId(formM.getFrmNbr(),"M","01");
        if(finFormMMaster!= null ){
            returnedStr =  "FormM Master Record Already Exists";
            logger.info(returnedStr);
            Date prevDate = finFormMMaster.getExpiryDate();
            logger.info("Checking for Expiry Date");
            if(prevDate!=null){
                Date newExpDate;
                try {
                    newExpDate = formatter1.parse(formM.getInfVld());
                    if(newExpDate.getTime()>prevDate.getTime()){
                        logger.info("Updating Master Table for Extended Validity ");
                        finFormMMaster.setExpiryDate(newExpDate);
                        finFormMMasterRepository.save(finFormMMaster);
                        logger.info("Successfully Updated for Extended Validity ");
                    }
                } catch (ParseException e) {e.printStackTrace();}
            }else{
                logger.info("Previous Expiry date is Null");
                try {
                    finFormMMaster.setExpiryDate(formatter1.parse(formM.getInfVld()));
                    finFormMMasterRepository.save(finFormMMaster);
                } catch (ParseException e) {e.printStackTrace();}

            }
        }else{
            finFormMMaster = new FinFormMMaster();
            try{
                finFormMMaster.setAppliAddr1(formM.getAppAdr().replaceAll("(\\r|\\n)", "").substring(0,39));
            }catch(Exception e){
                finFormMMaster.setAppliAddr1(formM.getAppAdr().replaceAll("(\\r|\\n)", ""));
            }
            try{
                finFormMMaster.setAppliAddr2(formM.getAppAdr().replaceAll("(\\r|\\n)", "").substring(40,79));
            }catch(Exception e){
                finFormMMaster.setAppliAddr2(" ");
            }
            try{
                finFormMMaster.setAppliAddr3(formM.getAppAdr().replaceAll("(\\r|\\n)", "").substring(80));
            }catch(Exception e){
                finFormMMaster.setAppliAddr3(" ");
            }
            logger.info("Query: findByRefDescandRefrecType via FinformMaster(formM) using AppCity "+formM.getAppCit());
            List<RefCodeTable> refCodeTables = refCodeRepository.findByRefDescAndRefRecType(formM.getAppCit(),"01");
            if(refCodeTables != null && refCodeTables.size()==1){
                logger.info("RefCode found for formNumber "+formM.getFrmNbr());
                finFormMMaster.setAppliCity(refCodeTables.get(0).getRefCode());
            }else{
                logger.info("RefCode found for formNumber "+formM.getFrmNbr());
                if(formM.getAppCit().length()>=5){
                    finFormMMaster.setAppliCity(formM.getAppCit().substring(0,5));
                }else{
                    finFormMMaster.setAppliCity(formM.getAppCit().substring(0,formM.getAppCit().length()));
                }
            }
            logger.info("Now Setting Ref_Code as "+finFormMMaster.getAppliCity());

            finFormMMaster.setFormNum(formM.getFrmNbr());
            finFormMMaster.setFormType("M");
            finFormMMaster.setDelFlg("N");
            finFormMMaster.setBankId("01");
            finFormMMaster.setEntityCreFlg("Y");
            finFormMMaster.setApplNum(formM.getInfSer());
            finFormMMaster.setFormPrefix(formM.getInfPfx());
            finFormMMaster.setBankCode(formM.getInfBnkCod());
            finFormMMaster.setYear(safeString(formM.getInfYea()));
            finFormMMaster.setBranchCode(formM.getInfBnkBra().substring(3));
            finFormMMaster.setBankersName("");
            finFormMMaster.setBankersAddr1("");
            finFormMMaster.setBankersAddr2("");
            finFormMMaster.setBankersAddr3("");
            finFormMMaster.setBankersCity("");
            finFormMMaster.setBankersState("");
            finFormMMaster.setBankersCntry("");
            finFormMMaster.setBankersPin("");
            finFormMMaster.setPurposeCode("");
            finFormMMaster.setRegisDate(safeDate(formM.getEndAppDat()));
            finFormMMaster.setExpiryDate(stringToDate(formM.getInfVld()));
            finFormMMaster.setApprovalDate( safeDate(formM.getEndAutDat()));
            finFormMMaster.setStatusCode(formM.getSta());
            finFormMMaster.setCustId("");
            finFormMMaster.setAcid("");
            finFormMMaster.setForexFlg(formM.getInfFrx());
            finFormMMaster.setChrgAcid("");
            finFormMMaster.setAppliFirstName(formM.getAppNam());
            finFormMMaster.setAppliLastName(formM.getAppNam());
            finFormMMaster.setAppliState(formM.getAppSta());
            finFormMMaster.setAppliCntry(" ");
            finFormMMaster.setAppliPin(formM.getAppTin());
            finFormMMaster.setAppliPhoneNum(formM.getAppTel());
            finFormMMaster.setAppliPassDet(formM.getAppPas());
            finFormMMaster.setChrgConFlg("");
            finFormMMaster.setChrgConAmt("");
            finFormMMaster.setChrgConCrncyCode("");
            finFormMMaster.setPartyCode("");
            finFormMMaster.setBeniFirstName(formM.getBenNam());
            finFormMMaster.setBeniLastName(formM.getBenNam());
            finFormMMaster.setBeniAddr1(formM.getBenAdr().replaceAll("(\\r|\\n)", ""));
            finFormMMaster.setBeniAddr2(formM.getBenAdr().replaceAll("(\\r|\\n)", ""));
            finFormMMaster.setBeniAddr3(formM.getBenAdr().replaceAll("(\\r|\\n)", ""));
            finFormMMaster.setBeniCity("");
            finFormMMaster.setBeniState("");
            finFormMMaster.setBeniPin("");
            finFormMMaster.setBeniCntry(formM.getBenCtyCode());
            finFormMMaster.setBeniPhoneNum(formM.getBenTel());
            finFormMMaster.setBeniPassDet("");
            finFormMMaster.setPaymentCode(formM.getGdsMopCod());
            finFormMMaster.setTransferCode(formM.getGdsTrfCod());
            finFormMMaster.setOriginCntryCode(formM.getItmCtyOrgCod());
            finFormMMaster.setSupplyCntryCode(formM.getGdsCtySupCod());
            finFormMMaster.setTransportCode(formM.getGdsMotCod());
            finFormMMaster.setPortCode(formM.getGdsDesCod());
            finFormMMaster.setDesignateBankCode(formM.getBnkCod());
            finFormMMaster.setInspAgentCode(formM.getScaCod());
            finFormMMaster.setAuthFirstName(formM.getEndAutNam());
            finFormMMaster.setAuthLastName(formM.getEndAutNam());
            finFormMMaster.setAuthAppliFirstName(formM.getEndAppNam());
            finFormMMaster.setAuthAppliLastName(formM.getEndAppNam());
            finFormMMaster.setRemarks("");
            finFormMMaster.setRarNumber("");
            finFormMMaster.setRarDate("");
            finFormMMaster.setGeneralDesc1(safeString(formM.getGdsGenDsc()));
            finFormMMaster.setNoOfItems(formM.getGdsGenTit());
            finFormMMaster.setNetWt(formM.getGdsGenWgt());
            finFormMMaster.setTotalFobValue(formM.getGdsValFob());
            finFormMMaster.setTotalFriCharges(formM.getGdsValFrg());
            finFormMMaster.setTotalAnciCharges(formM.getGdsValTac());
            finFormMMaster.setFormmFcyAmt(formM.getGdsValTcfVal());
            finFormMMaster.setFcyCrncyCode(formM.getGdsValCurCod());
            finFormMMaster.setInvoiceNum(formM.getGdsProInv());
            finFormMMaster.setInvoiceDate( new Date(formM.getGdsProDat().getTime()));
            finFormMMaster.setExcgRateCode("");
            finFormMMaster.setFormmRate(formM.getGdsValExc());
            finFormMMaster.setFormmNairaEqui((Double.parseDouble(formM.getGdsValTcfVal()) * Double.parseDouble(formM.getGdsValExc())));
            finFormMMaster.setHomeCrncyCode("NGN");
            finFormMMaster.setFirstTimeFlg("");
            finFormMMaster.setRcreUserId("");
            finFormMMaster.setRcreTime(null);
            finFormMMaster.setLchgUserId("");
            finFormMMaster.setLchgTime(null);
            logger.info("issue is: "+finFormMMaster.toString());
            finFormMMasterRepository.save(finFormMMaster);
            logger.info("FormM Master Record Has been saved for: "+finFormMMaster.getFormNum());
            returnedStr =  "FormM Master saved";
        }
        return returnedStr;
    }

    public static String generateFinFormMValMaster(FormM formM){

        logger.info("Query: findByFormNum via generateFinFormValMaster using FormNum "+formM.getFrmNbr());
        finFormMValMaster = finFormMMasterValRepository.findByFormNum(formM.getFrmNbr());
        if(finFormMValMaster == null){
            finFormMValMaster = new FinFormMValMaster();
            finFormMValMaster.setFormNum(formM.getFrmNbr());
            finFormMValMaster.setFormType("M");
            finFormMValMaster.setEntityCreFlg("Y");
            finFormMValMaster.setDelFlg("N");
            finFormMValMaster.setBankId("01");
            finFormMValMaster.setApplNum(formM.getInfSer());
            finFormMValMaster.setFormPrefix(formM.getInfPfx());
            finFormMValMaster.setBankCode(formM.getInfBnkCod());
            finFormMValMaster.setYear(formM.getInfYea());
            finFormMValMaster.setBranchCode(formM.getInfBnkBra().substring(3));
            finFormMValMaster.setBankersName("");
            finFormMValMaster.setBankersAddr1("");
            finFormMValMaster.setBankersAddr2("");
            finFormMValMaster.setBankersAddr3("");
            finFormMValMaster.setBankersCity("");
            finFormMValMaster.setBankersState("");
            finFormMValMaster.setBankersCntry("");
            finFormMValMaster.setBankersPin("");
            finFormMValMaster.setPurposeCode("");
            finFormMValMaster.setRegisDate(dateToString(formM.getEndAppDat()));
            finFormMValMaster.setExpiryDate(formM.getInfVld());
            finFormMValMaster.setApprovalDate(new Timestamp(safeDate(formM.getEndAutDat()).getTime()));
            finFormMValMaster.setStatusCode(formM.getSta());
            finFormMValMaster.setCustId("");
            finFormMValMaster.setAcid("");
            finFormMValMaster.setForexFlg(formM.getInfFrx());
            finFormMValMaster.setChrgAcid("");
            finFormMValMaster.setAppliFirstName(formM.getAppNam());
            finFormMValMaster.setAppliLastName(formM.getAppNam());
            try{
                finFormMValMaster.setAppliAddr1(formM.getAppAdr().replaceAll("(\\r|\\n)", "").substring(0,39));
            }catch(Exception e){
                finFormMValMaster.setAppliAddr1(formM.getAppAdr().replaceAll("(\\r|\\n)", ""));
            }
            try{
                finFormMValMaster.setAppliAddr2(formM.getAppAdr().replaceAll("(\\r|\\n)", "").substring(40,79));
            }catch(Exception e){
                finFormMValMaster.setAppliAddr2(" ");
            }
            try{
                finFormMValMaster.setAppliAddr3(formM.getAppAdr().replaceAll("(\\r|\\n)", "").substring(80));
            }catch(Exception e){
                finFormMValMaster.setAppliAddr3(" ");
            }
            logger.info("Query: findByRefDescandRefrecType via FinformMaster(FormMmasterVal) using AppCity "+formM.getAppCit());
            List<RefCodeTable> refCodeTables = refCodeRepository.findByRefDescAndRefRecType(formM.getAppCit(),"01");
            if(refCodeTables != null && refCodeTables.size()==1){
                logger.info("RefCode found for formNumber "+finFormMValMaster.getFormNum());
                finFormMValMaster.setAppliCity(refCodeRepository.findByRefDescAndRefRecType(formM.getAppCit(),"01").get(0).getRefCode());
            }else{
                logger.error("RefCode not found for formNumber "+finFormMValMaster.getFormNum());
                if(formM.getAppCit().length()>=5){
                    finFormMValMaster.setAppliCity(formM.getAppCit().substring(0,5));
                }else{
                    finFormMValMaster.setAppliCity(formM.getAppCit().substring(0,formM.getAppCit().length()));
                }

            }
            logger.info("Now Setting Ref_Code as "+finFormMValMaster.getAppliCity());

            finFormMValMaster.setAppliState(formM.getAppSta());
            finFormMValMaster.setAppliCntry(" ");
            finFormMValMaster.setAppliPin(formM.getAppTin());
            finFormMValMaster.setAppliPhoneNum(formM.getAppTel());
            finFormMValMaster.setAppliPassDet(formM.getAppPas());
            finFormMValMaster.setChrgConFlg("");
            finFormMValMaster.setChrgConAmt("");
            finFormMValMaster.setChrgConCrncyCode("");
            finFormMValMaster.setPartyCode("");
            finFormMValMaster.setBeniFirstName(formM.getBenNam());
            finFormMValMaster.setBeniLastName(formM.getBenNam());
            finFormMValMaster.setBeniAddr1(formM.getBenAdr().replaceAll("(\\r|\\n)", ""));
            finFormMValMaster.setBeniAddr2(formM.getBenAdr().replaceAll("(\\r|\\n)", ""));
            finFormMValMaster.setBeniAddr3(formM.getBenAdr().replaceAll("(\\r|\\n)", ""));
            finFormMValMaster.setBeniCity("");
            finFormMValMaster.setBeniState("");
            finFormMValMaster.setBeniPin("");
            finFormMValMaster.setBeniCntry(formM.getBenCtyCode());
            finFormMValMaster.setBeniPhoneNum(formM.getBenTel());
            finFormMValMaster.setBeniPassDet("");
            finFormMValMaster.setPaymentCode(formM.getGdsMopCod());
            finFormMValMaster.setTransferCode(formM.getGdsTrfCod());
            finFormMValMaster.setOriginCntryCode(formM.getItmCtyOrgCod());
            finFormMValMaster.setSupplyCntryCode(formM.getGdsCtySupCod());
            finFormMValMaster.setTransportCode(formM.getGdsMotCod());
            finFormMValMaster.setPortCode(formM.getGdsDesCod());
            finFormMValMaster.setDesignateBankCode(formM.getBnkCod());
            finFormMValMaster.setInspAgentCode(formM.getScaCod());
            finFormMValMaster.setAuthFirstName(formM.getEndAutNam());
            finFormMValMaster.setAuthLastName(formM.getEndAutNam());
            finFormMValMaster.setAuthAppliFirstName(formM.getEndAppNam());
            finFormMValMaster.setAuthAppliLastName(formM.getEndAppNam());
            finFormMValMaster.setRemarks("");
            finFormMValMaster.setRarNumber("");
            finFormMValMaster.setRarDate("");
            finFormMValMaster.setGeneralDesc1(formM.getGdsGenDsc());
            finFormMValMaster.setNoOfItems(formM.getGdsGenTit());
            finFormMValMaster.setNetWt(formM.getGdsGenWgt());
            finFormMValMaster.setTotalFobValue(formM.getGdsValFob());
            finFormMValMaster.setTotalFriCharges(formM.getGdsValFrg());
            finFormMValMaster.setTotalAnciCharges(formM.getGdsValTac());
            finFormMValMaster.setFormmFcyAmt(formM.getGdsValTcfVal());
            finFormMValMaster.setFcyCrncyCode(formM.getGdsValCurCod());
            finFormMValMaster.setInvoiceNum(formM.getGdsProInv());
            finFormMValMaster.setInvoiceDate(formM.getGdsProDat());
            finFormMValMaster.setExcgRateCode("");
            finFormMValMaster.setFormmRate(formM.getGdsValExc());
            finFormMValMaster.setFormmNairaEqui((Double.parseDouble(formM.getGdsValTcfVal()) * Double.parseDouble(formM.getGdsValExc())));
            finFormMValMaster.setHomeCrncyCode("NGN");
            finFormMValMaster.setFirstTimeFlg("");
            finFormMValMaster.setRcreUserId("");
            finFormMValMaster.setRcreTime(null);
            finFormMValMaster.setLchgUserId("");
            finFormMValMaster.setLchgTime(null);

            finFormMMasterValRepository.save(finFormMValMaster);
            logger.info("Validated Form Saved");
            finFormMValMaster = null;
            return "saved";
        }
        else{
            logger.error("Validated Form Record Already Exists");
            return "Record Already Exists";
        }

    }

    public static String generateFinFormMMasterAudit(FormM formM,List<SupportInformationType> suppInf){

        String prevlastOprtnDate="";
        String newlastOprtnDate="";
        String prevOprtnStatus="";
        String lastOprtnStatus = "";
        if(suppInf!=null){
            long tsCnt=0;
            long idCnt =0;
            logger.info("Query: findByApplNumOrdered via masterAudit(FormM) using ApplNum "+formM.getInfSer());
            try{
                FinFormMMasterAudit finFormMMasterAudit =  finFormMMasterAuditRepository.findFirstByFormMNumOrderByTsCntDesc(formM.getFrmNbr());
                prevlastOprtnDate = finFormMMasterAudit.getInfDate();
                tsCnt = finFormMMasterAudit.getTsCnt();
            }catch(Exception e) {
                logger.error("Query: finFormMMasterAudit.getInfDate() came as null in audit for "+formM.getFrmNbr());
            }
            try{
                idCnt = finFormMMasterAuditRepository.findFirstByOrderByIdDesc().getId();
            } catch(Exception e){
                logger.error("Query: finFormMMasterAuditRepository.findFirstByOrderByIdDesc() came as null in audit for "+formM.getFrmNbr());
            }

            for(SupportInformationType suppINF:suppInf){
                String xmlDateString = suppINF.getDAT().toString().replace("T"," ").substring(0,19);
                logger.info("Query: findByAuditProps via masterAudit(FormM) using formNumber "+formM.getFrmNbr());
                finFormMMasterAudit = finFormMMasterAuditRepository.findByFormMNumAndApplNumAndAdBankCodeAndInfDateAndInfOpr(
                        formM.getFrmNbr(),
                        formM.getInfSer(),
                        formM.getInfBnkCod(),
                        xmlDateString,
                        suppINF.getOPR());
                if(finFormMMasterAudit==null){
                    finFormMMasterAudit = new FinFormMMasterAudit();
                    if(formM.getInfVld()!=null && formM.getInfVld()!=""){
                        finFormMMasterAudit.setExpiryDate(new Timestamp(stringToDate(formM.getInfVld()).getTime()));
                    }
                    finFormMMasterAudit.setFormMNum(formM.getFrmNbr());
                    finFormMMasterAudit.setFormMType("M");
                    finFormMMasterAudit.setBankID("01");
                    finFormMMasterAudit.setEntityCrFlg("Y");
                    finFormMMasterAudit.setDelFlg("N");
                    finFormMMasterAudit.setApplNum(formM.getInfSer());
                    finFormMMasterAudit.setForexFlg(formM.getInfFrx());
                    finFormMMasterAudit.setAdBankCode(formM.getInfBnkCod());
                    finFormMMasterAudit.setBankNumbering(formM.getInfBnkNbr());
                    finFormMMasterAudit.setYear(formM.getInfYea());
                    finFormMMasterAudit.setAdBankBranch(formM.getInfBnkBra().substring(3));
                    finFormMMasterAudit.setInfUser(suppINF.getUSR());
                    finFormMMasterAudit.setInfMsg(suppINF.getMSG());
                    finFormMMasterAudit.setInfOpr(suppINF.getOPR());
                    finFormMMasterAudit.setInfDate(xmlDateString);
                    ++tsCnt;++idCnt;
                    finFormMMasterAudit.setTsCnt(tsCnt);
                    finFormMMasterAudit.setId(idCnt);
                    finFormMMasterAuditRepository.save(finFormMMasterAudit);

                    logger.info("FormMMasterAudit has been Saved");
                }else{
                    logger.info("Record Already Exists in Audit");
                    flgCntrl = false;
                }
                lastOprtnStatus = suppINF.getOPR();
                newlastOprtnDate = xmlDateString;
            }
        }
        logger.info("Query: findByFormNumAndFormTypeAndBankId via masterAudit(FormM) using formNum"+formM.getFrmNbr());
        finFormMMaster = finFormMMasterRepository.findByFormNumAndFormTypeAndBankId(formM.getFrmNbr(),"M","01");
        boolean doCheck = false;
        if(finFormMMaster!=null){
            prevOprtnStatus = finFormMMaster.getStatusCode();
            logger.info(prevlastOprtnDate+" nowf "+newlastOprtnDate);
            if(!prevlastOprtnDate.equals("")&&!newlastOprtnDate.equals("")){
                try {
                    if(formatter3.parse(newlastOprtnDate).getTime() >= formatter3.parse(prevlastOprtnDate).getTime()){
                        doCheck = true;
                    }else{
                        logger.info("Not a new Audit Information Skipping Update of Master Table for Expiry Date");
                    }
                } catch (ParseException e) {e.printStackTrace();}

                if(doCheck){
                    if(!prevOprtnStatus.toUpperCase().equals(lastOprtnStatus.toUpperCase())&&
                            !lastOprtnStatus.toUpperCase().equals("EXTEND VALIDITY")){
                        logger.info("Master Table status Updated from "+prevOprtnStatus+" to  "+lastOprtnStatus);
                        finFormMMaster.setStatusCode(lastOprtnStatus);
                    }
                    logger.info(formM.getInfVld()+" date compare 1 "+finFormMMaster.getExpiryDate());
                    try {
                        if(formatter1.parse(formM.getInfVld()).getTime()>finFormMMaster.getExpiryDate().getTime()){
                            finFormMMaster.setExpiryDate(formatter1.parse(formM.getInfVld()));
                            logger.info("Expiry Date Updated for "+formM.getFrmNbr());
                        }

                    } catch (ParseException e) {e.printStackTrace();}
                    finFormMMasterRepository.save(finFormMMaster);

                }
            }else{
                logger.info("Master Table Expiry Date Update skipped  1");
            }

        }else{
            logger.info("formMaster not found for masterAudit Update using form: "+formM.getFrmNbr());
        }
        finFormMMasterAudit = null;
        finFormMMaster = null;
        return "saved";
    }

    public static String generateFinFormMMasterAudit(FormMalreadyDownloadType formMalreadyDownloadType,List<SupportInformationType> suppInf){

        String prevlastOprtnDate="";
        String newlastOprtnDate="";
        String prevOprtnStatus;
        String lastOprtatnStatus = "";
        String frmNumb = formMalreadyDownloadType.getInfPfx()+formMalreadyDownloadType.getInfBnkNbr();

        boolean doCheck = false;
        if(suppInf!=null){
            long tsCnt=0;
            long idCnt =0;
            logger.info("Query: findByFormNumb via masterAudit(FormMAlrdyDownloaded) using formNum "+formMalreadyDownloadType.getInfSer());
            try{
                FinFormMMasterAudit finFormMMasterAudit = finFormMMasterAuditRepository.findFirstByFormMNumOrderByTsCntDesc(frmNumb);
                tsCnt = finFormMMasterAudit.getTsCnt();
                prevlastOprtnDate = finFormMMasterAudit.getInfDate();
            }catch(NullPointerException e){e.printStackTrace();}

            try{
                idCnt = finFormMMasterAuditRepository.findFirstByOrderByIdDesc().getId();
            } catch (NullPointerException e){e.printStackTrace();}

            for(SupportInformationType suppINF:suppInf){
                String infDAT = suppINF.getDAT().toString().replace("T"," ").substring(0,19);
                logger.info("Query: findByAuditProps via masterAudit(FormMAlrdyDownloaded) using formNum "+frmNumb);
                finFormMMasterAudit = finFormMMasterAuditRepository.findByFormMNumAndApplNumAndAdBankCodeAndInfDateAndInfOpr(
                        frmNumb,
                        formMalreadyDownloadType.getInfSer(),
                        formMalreadyDownloadType.getInfBnkCod(),
                        infDAT,
                        suppINF.getOPR());

                if(finFormMMasterAudit==null){
                    finFormMMasterAudit = new FinFormMMasterAudit();
                    finFormMMasterAudit.setExpiryDate(null);
                    try {
                        if(formMalreadyDownloadType.getInfVld()!=null&&!formMalreadyDownloadType.getInfVld().equals("")){
                            finFormMMasterAudit.setExpiryDate(new Timestamp(formatter1.parse(formMalreadyDownloadType.getInfVld()).getTime()));
                        }
                    } catch (ParseException e) {e.printStackTrace();}
                    finFormMMasterAudit.setFormMNum(frmNumb);
                    finFormMMasterAudit.setFormMType("M");
                    finFormMMasterAudit.setBankID("01");
                    finFormMMasterAudit.setEntityCrFlg("Y");
                    finFormMMasterAudit.setDelFlg("N");
                    finFormMMasterAudit.setApplNum(formMalreadyDownloadType.getInfSer());
                    finFormMMasterAudit.setFormPrefix(formMalreadyDownloadType.getInfPfx());
                    finFormMMasterAudit.setAdBankCode(formMalreadyDownloadType.getInfBnkCod());
                    finFormMMasterAudit.setBankNumbering(formMalreadyDownloadType.getInfBnkNbr());
                    finFormMMasterAudit.setYear(formMalreadyDownloadType.getInfYea());
                    finFormMMasterAudit.setForexFlg(formMalreadyDownloadType.getInfFrx());
                    finFormMMasterAudit.setAdBankBranch(formMalreadyDownloadType.getInfBnkBra().substring(3));
                    finFormMMasterAudit.setInfUser(suppINF.getUSR());
                    finFormMMasterAudit.setInfMsg(suppINF.getMSG());
                    finFormMMasterAudit.setInfOpr(suppINF.getOPR());
                    finFormMMasterAudit.setInfDate(infDAT);
                    ++tsCnt;++idCnt;
                    finFormMMasterAudit.setTsCnt(tsCnt);
                    finFormMMasterAudit.setId(idCnt);
                    finFormMMasterAuditRepository.save(finFormMMasterAudit);
                    logger.info("FormMMasterAudit has been Saved");
                }else{
                    logger.info("Record Already Exists in Audit");
                    flgCntrl = false;
                }
                lastOprtatnStatus = suppINF.getOPR();
                newlastOprtnDate  = infDAT;
            }
        }

        logger.info("Query: findByFormNumAndFormTypeAndBankId  via masterAudit(FormMAlrdyDownloaded) using formNum and Applnum "+frmNumb+" "+formMalreadyDownloadType.getInfSer());
        finFormMMaster = finFormMMasterRepository.findByFormNumAndFormTypeAndBankId(frmNumb,"M","01");

        if(finFormMMaster!=null){
            prevOprtnStatus = finFormMMaster.getStatusCode();
            logger.info(newlastOprtnDate+" and "+prevlastOprtnDate);
            if(!newlastOprtnDate.equals("")&& !prevlastOprtnDate.equals("")){
                try {
                    if(formatter3.parse(newlastOprtnDate).getTime() >= formatter3.parse(prevlastOprtnDate).getTime()){
                        doCheck = true;
                    }else{
                        logger.info("Not a new Audit Information Skipping Update of Master Table for Expiry date");
                    }
                }catch (ParseException e) {e.printStackTrace();}
                if(doCheck){
                    if(!lastOprtatnStatus.toUpperCase().equals("EXTEND VALIDITY")&&
                            !lastOprtatnStatus.toUpperCase().equals(prevOprtnStatus.toUpperCase())){
                        logger.info("Master Table status updated via FormMAlready Downloaded from  "+prevOprtnStatus+" to "+lastOprtatnStatus);
                        finFormMMaster.setStatusCode(lastOprtatnStatus);
                    }
                    logger.info(formMalreadyDownloadType.getInfVld()+" date compare 2 "+finFormMMaster.getExpiryDate());
                    try{
                        if(formatter1.parse(formMalreadyDownloadType.getInfVld()).getTime()>
                                finFormMMaster.getExpiryDate().getTime()){
                            finFormMMaster.setExpiryDate(formatter1.parse(formMalreadyDownloadType.getInfVld()));
                            logger.info("Expiry Date Updated for "+frmNumb);

                        }
                    }
                    catch(ParseException e){e.printStackTrace();}
                    finFormMMasterRepository.save(finFormMMaster);

                }
            }
            else{
                logger.info("Master Table Update skipped has no record found 2");
            }
        }
        finFormMMasterAudit = null;
        finFormMMaster = null;
        return "saved";
    }

    public static String safeString(String string) {
        if (string == null||string.equalsIgnoreCase("null")) {
            return "";
        }else {
            String returned = string.replaceAll("(\\r|\\n)", "");
            if (returned.length() > 99) {
                return returned.subSequence(0, 99).toString();
            }else{
                return returned;
            }
        }
    }

    public static Date safeDate(Date date) {
        Date safeDate;
        if (date == null) {
            return null;
        }  else {
            safeDate = date;
            return safeDate;
        }
    }

    public static String dateToString(Date date) {
        String safeDate;
        if (date == null) {
            return "";
        }else {
            safeDate = formatter1.format(date);
            return safeDate;
        }
    }

    public static Date stringToDate(String dateString) {
        Date safeDate = null;
        if (dateString == null||dateString.equalsIgnoreCase("null")) {
            return null;
        } else {
            try {
                safeDate = formatter1.parse(dateString);
            } catch (ParseException e) {
                e.printStackTrace();
                safeDate = null;
            }
            return safeDate;
        }
    }

}
