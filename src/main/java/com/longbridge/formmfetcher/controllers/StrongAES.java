package com.longbridge.formmfetcher.controllers;
import sun.misc.BASE64Decoder;
import sun.misc.BASE64Encoder;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.spec.SecretKeySpec;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.Key;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;

/**
 * Created by LB-PRJ-020 on 7/13/2017.
 */
public class StrongAES {

    private Key aesKey;
    private Cipher cipher;
    private String encryString;
    private String decrypted;
//    String text = "CUSTOM";
//    String key = "Bar12345Bar12345"; // 128 bit key

    public static void main(String[] args) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, IOException {

        String res = new StrongAES().getPassEncr("o7L0ngbr1dg32oi7","custom");
        System.out.println("Encryoted Pass is "+res);
//        String res2 = new StrongAES().getPassDecr("o7L0ngbr1dg32oi7",res);
//        System.out.println("Decrypted Pass "+res2);

    }

    public String getPassDecr(String $k,String $e) throws NoSuchPaddingException, NoSuchAlgorithmException, BadPaddingException, IllegalBlockSizeException, InvalidKeyException, IOException {
        aesKey = new SecretKeySpec($k.getBytes(), "AES");
        cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.DECRYPT_MODE, aesKey);
        BASE64Decoder base64Decoder = new BASE64Decoder();
        byte [] enc = base64Decoder.decodeBuffer($e);
        String $d = new String(cipher.doFinal(enc));
        return $d;
    }

    public String getPassEncr(String key,String password) throws NoSuchPaddingException, NoSuchAlgorithmException, InvalidKeyException, BadPaddingException, IllegalBlockSizeException, IOException {
        aesKey = new SecretKeySpec(key.getBytes(), "AES");
        cipher = Cipher.getInstance("AES");
        cipher.init(Cipher.ENCRYPT_MODE, aesKey);
        byte[] encryString = cipher.doFinal(password.getBytes());
        BASE64Encoder encoder = new BASE64Encoder();
        String encScript  = encoder.encode(encryString);
        return encScript;
    }
}