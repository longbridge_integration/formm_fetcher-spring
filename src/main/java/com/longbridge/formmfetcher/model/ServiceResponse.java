package com.longbridge.formmfetcher.model;

/**
 * Created by LB-PRJ-020 on 10/20/2017.
 */
public class ServiceResponse {
    private String respMessage;
    private boolean successFull;

    public String getRespMessage() {
        return respMessage;
    }

    public void setRespMessage(String respMessage) {
        this.respMessage = respMessage;
    }

    public boolean isSuccessFull() {
        return successFull;
    }

    public void setSuccessFull(boolean successFull) {
        this.successFull = successFull;
    }

    @Override
    public String toString() {
        return "ServiceResponse{" +
                "respMessage='" + respMessage + '\'' +
                ", successFull=" + successFull +
                '}';
    }
}
