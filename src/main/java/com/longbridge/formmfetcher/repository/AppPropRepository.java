package com.longbridge.formmfetcher.repository;

import com.longbridge.formmfetcher.Entities.AppProperty;
import org.springframework.data.jpa.repository.JpaRepository;

public interface AppPropRepository extends JpaRepository<AppProperty, Long> {

    AppProperty save(AppProperty finFormMItms);

    AppProperty findByBankCode(String bankCode);

    AppProperty findFirstByOrderByIdDesc();
}
