package com.longbridge.formmfetcher.repository;

import com.longbridge.formmfetcher.Entities.FinacleEntities.FinFormMItms;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FinFormMItmsRepository extends JpaRepository<FinFormMItms, Long> {

    FinFormMItms save(FinFormMItms finFormMItms);

    List<FinFormMItms> findByFormNum(String formNum);

    FinFormMItms findFirstByOrderByTsCntDesc();

    FinFormMItms findByFormNumAndSrNumAndBankIdAndFormType(String frmNum,String srNum,String bankID,String formTpe);
}
