package com.longbridge.formmfetcher.repository;

import com.longbridge.formmfetcher.Entities.FinFormMMasterAudit;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FinFormMMasterAuditRepository extends JpaRepository<FinFormMMasterAudit, String> {

    FinFormMMasterAudit save(FinFormMMasterAudit finFormMMasterAudit);

    FinFormMMasterAudit findByFormMNumAndApplNumAndAdBankCodeAndInfDateAndInfOpr(String formNum,
                       String appNum, String adBnkCode, String infDate,String infOpr);

    FinFormMMasterAudit findFirstByOrderByIdDesc();

    FinFormMMasterAudit findFirstByFormMNumOrderByTsCntDesc(String formNum);



}
