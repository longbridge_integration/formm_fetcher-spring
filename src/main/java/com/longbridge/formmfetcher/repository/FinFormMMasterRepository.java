package com.longbridge.formmfetcher.repository;

import com.longbridge.formmfetcher.Entities.FinacleEntities.FinFormMMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FinFormMMasterRepository extends JpaRepository<FinFormMMaster, String> {

    FinFormMMaster save(FinFormMMaster finFormMMaster);

    FinFormMMaster findByFormNumAndFormTypeAndBankId(String formNumber,String formType,String bankID);


}
