package com.longbridge.formmfetcher.repository;

import com.longbridge.formmfetcher.Entities.FinFormMValMaster;
import org.springframework.data.jpa.repository.JpaRepository;

public interface FinFormMMasterValRepository extends JpaRepository<FinFormMValMaster, String> {

    FinFormMValMaster save(FinFormMValMaster finFormMValMaster);

    FinFormMValMaster findByFormNum(String formNumber);

}
