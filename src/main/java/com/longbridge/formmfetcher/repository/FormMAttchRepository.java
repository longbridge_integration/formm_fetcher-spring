package com.longbridge.formmfetcher.repository;

import com.longbridge.formmfetcher.Entities.FormMAttachment;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface FormMAttchRepository extends JpaRepository<FormMAttachment, Long> {

    FormMAttachment save(FormMAttachment formMAttachment);

    FormMAttachment findByAtdBinTypAndAtdRefAndAtdTypeAndAtdDatAndFormMNbr(String atdBinType, String atdRef,
                                                         String atdType,String atdDat,String frmMNumb);
    List<FormMAttachment> findAll();

    FormMAttachment findFirstByOrderByIdDesc();

    List<FormMAttachment> findByIsDocGenerated(String docGenFlg);
}
