package com.longbridge.formmfetcher.repository;

import com.longbridge.formmfetcher.Entities.FormM;
import org.springframework.data.jpa.repository.JpaRepository;

import java.sql.Timestamp;

public interface FormMRepository extends JpaRepository<FormM, String> {

    FormM save(FormM formM);

    FormM findFirstByOrderByIdDesc();

    FormM findByFrmNbr(String frmNumber);
}
