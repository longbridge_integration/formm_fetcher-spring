package com.longbridge.formmfetcher.repository;

import com.longbridge.formmfetcher.Entities.FinacleEntities.RefCodeTable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface RefCodeRepository extends JpaRepository<RefCodeTable, String> {

    List<RefCodeTable> findByRefDescAndRefRecType(String refDesc,String refRecType);

}
