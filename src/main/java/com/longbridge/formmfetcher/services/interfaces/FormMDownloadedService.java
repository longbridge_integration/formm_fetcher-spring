package com.longbridge.formmfetcher.services.interfaces;



import com.longbridge.formmfetcher.Entities.FormMalreadyDownloadType;

public interface FormMDownloadedService {

   String save(FormMalreadyDownloadType formMalreadyDownloadType);

   Long count();

}