package com.longbridge.formmfetcher.services.interfaces;



import com.longbridge.formmfetcher.Entities.FormM;
import java.util.List;

public interface FormMService {

   String save (FormM formM);

}