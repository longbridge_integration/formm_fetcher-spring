
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for APPType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="APPType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://twm.ncs.gov.ng/formm/xsd}information">
 *       &lt;sequence>
 *         &lt;element name="STA" type="{http://twm.ncs.gov.ng/formm/xsd}X2" minOccurs="0"/>
 *         &lt;element name="RCN" type="{http://twm.ncs.gov.ng/formm/xsd}X17" minOccurs="0"/>
 *         &lt;element name="TIN" type="{http://twm.ncs.gov.ng/formm/xsd}X17"/>
 *         &lt;element name="NEPC" type="{http://twm.ncs.gov.ng/formm/xsd}X17" minOccurs="0"/>
 *         &lt;element name="PAS" type="{http://twm.ncs.gov.ng/formm/xsd}X17" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "APPType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "sta",
    "rcn",
    "tin",
    "nepc",
    "pas"
})
public class APPType
    extends Information
{

    @XmlElement(name = "STA", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String sta;
    @XmlElement(name = "RCN", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String rcn;
    @XmlElement(name = "TIN", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String tin;
    @XmlElement(name = "NEPC", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String nepc;
    @XmlElement(name = "PAS", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String pas;

    /**
     * Gets the value of the sta property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSTA() {
        return sta;
    }

    /**
     * Sets the value of the sta property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSTA(String value) {
        this.sta = value;
    }

    /**
     * Gets the value of the rcn property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRCN() {
        return rcn;
    }

    /**
     * Sets the value of the rcn property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRCN(String value) {
        this.rcn = value;
    }

    /**
     * Gets the value of the tin property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTIN() {
        return tin;
    }

    /**
     * Sets the value of the tin property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTIN(String value) {
        this.tin = value;
    }

    /**
     * Gets the value of the nepc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNEPC() {
        return nepc;
    }

    /**
     * Sets the value of the nepc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNEPC(String value) {
        this.nepc = value;
    }

    /**
     * Gets the value of the pas property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPAS() {
        return pas;
    }

    /**
     * Sets the value of the pas property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPAS(String value) {
        this.pas = value;
    }

}
