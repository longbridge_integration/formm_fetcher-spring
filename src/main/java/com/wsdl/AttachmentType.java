
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for AttachmentType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="AttachmentType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TYP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="REF" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DAT" type="{http://www.w3.org/2001/XMLSchema}date"/>
 *         &lt;element name="BIN" type="{http://www.w3.org/2001/XMLSchema}base64Binary"/>
 *         &lt;element name="BIN_TYP" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "AttachmentType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "typ",
    "ref",
    "dat",
    "bin",
    "bintyp"
})
public class AttachmentType {

    @XmlElement(name = "TYP", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String typ;
    @XmlElement(name = "REF", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String ref;
    @XmlElement(name = "DAT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    @XmlSchemaType(name = "date")
    protected XMLGregorianCalendar dat;
    @XmlElement(name = "BIN", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected byte[] bin;
    @XmlElement(name = "BIN_TYP", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String bintyp;

    /**
     * Gets the value of the typ property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTYP() {
        return typ;
    }

    /**
     * Sets the value of the typ property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTYP(String value) {
        this.typ = value;
    }

    /**
     * Gets the value of the ref property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getREF() {
        return ref;
    }

    /**
     * Sets the value of the ref property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setREF(String value) {
        this.ref = value;
    }

    /**
     * Gets the value of the dat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDAT() {
        return dat;
    }

    /**
     * Sets the value of the dat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDAT(XMLGregorianCalendar value) {
        this.dat = value;
    }

    /**
     * Gets the value of the bin property.
     * 
     * @return
     *     possible object is
     *     byte[]
     */
    public byte[] getBIN() {
        return bin;
    }

    /**
     * Sets the value of the bin property.
     * 
     * @param value
     *     allowed object is
     *     byte[]
     */
    public void setBIN(byte[] value) {
        this.bin = value;
    }

    /**
     * Gets the value of the bintyp property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBINTYP() {
        return bintyp;
    }

    /**
     * Sets the value of the bintyp property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBINTYP(String value) {
        this.bintyp = value;
    }

}
