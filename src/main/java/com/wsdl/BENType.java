
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for BENType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="BENType">
 *   &lt;complexContent>
 *     &lt;extension base="{http://twm.ncs.gov.ng/formm/xsd}information">
 *       &lt;sequence>
 *         &lt;element name="ZIP" type="{http://twm.ncs.gov.ng/formm/xsd}X7" minOccurs="0"/>
 *         &lt;element name="CTY" type="{http://twm.ncs.gov.ng/formm/xsd}cod_2"/>
 *         &lt;element name="ORD" type="{http://twm.ncs.gov.ng/formm/xsd}X35" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "BENType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "zip",
    "cty",
    "ord"
})
public class BENType
    extends Information
{

    @XmlElement(name = "ZIP", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String zip;
    @XmlElement(name = "CTY", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected Cod2 cty;
    @XmlElement(name = "ORD", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String ord;

    /**
     * Gets the value of the zip property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getZIP() {
        return zip;
    }

    /**
     * Sets the value of the zip property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setZIP(String value) {
        this.zip = value;
    }

    /**
     * Gets the value of the cty property.
     * 
     * @return
     *     possible object is
     *     {@link Cod2 }
     *     
     */
    public Cod2 getCTY() {
        return cty;
    }

    /**
     * Sets the value of the cty property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod2 }
     *     
     */
    public void setCTY(Cod2 value) {
        this.cty = value;
    }

    /**
     * Gets the value of the ord property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getORD() {
        return ord;
    }

    /**
     * Sets the value of the ord property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setORD(String value) {
        this.ord = value;
    }

}
