
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Code X1015
 * 
 * <p>Java class for cod_1015 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="cod_1015">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="CODE" type="{http://twm.ncs.gov.ng/formm/xsd}bankCode"/>
 *         &lt;element name="COD" type="{http://twm.ncs.gov.ng/formm/xsd}X10-15" minOccurs="0"/>
 *         &lt;element name="BRA" type="{http://twm.ncs.gov.ng/formm/xsd}X17"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "cod_1015", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "code",
    "cod",
    "bra"
})
public class Cod1015 {

    @XmlElement(name = "CODE", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String code;
    @XmlElement(name = "COD", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String cod;
    @XmlElement(name = "BRA", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String bra;

    /**
     * Gets the value of the code property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCODE() {
        return code;
    }

    /**
     * Sets the value of the code property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCODE(String value) {
        this.code = value;
    }

    /**
     * Gets the value of the cod property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCOD() {
        return cod;
    }

    /**
     * Sets the value of the cod property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCOD(String value) {
        this.cod = value;
    }

    /**
     * Gets the value of the bra property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getBRA() {
        return bra;
    }

    /**
     * Sets the value of the bra property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setBRA(String value) {
        this.bra = value;
    }

}
