
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Code X3 and Name
 * 
 * <p>Java class for codnam_3 complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="codnam_3">
 *   &lt;complexContent>
 *     &lt;extension base="{http://twm.ncs.gov.ng/formm/xsd}cod_3">
 *       &lt;sequence>
 *         &lt;element name="NAM" type="{http://twm.ncs.gov.ng/formm/xsd}X35" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/extension>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "codnam_3", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "nam"
})
public class Codnam3
    extends Cod3
{

    @XmlElement(name = "NAM", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String nam;

    /**
     * Gets the value of the nam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAM() {
        return nam;
    }

    /**
     * Sets the value of the nam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAM(String value) {
        this.nam = value;
    }

}
