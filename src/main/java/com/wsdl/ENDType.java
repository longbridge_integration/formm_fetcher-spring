
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ENDType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ENDType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="APP">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="NAM" type="{http://twm.ncs.gov.ng/formm/xsd}X128" minOccurs="0"/>
 *                   &lt;element name="DAT" type="{http://twm.ncs.gov.ng/formm/xsd}date" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="AUT">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="NAM" type="{http://twm.ncs.gov.ng/formm/xsd}X128" minOccurs="0"/>
 *                   &lt;element name="DAT" type="{http://twm.ncs.gov.ng/formm/xsd}date" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ENDType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "app",
    "aut"
})
public class ENDType {

    @XmlElement(name = "APP", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected ENDType.APP app;
    @XmlElement(name = "AUT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected ENDType.AUT aut;

    /**
     * Gets the value of the app property.
     * 
     * @return
     *     possible object is
     *     {@link ENDType.APP }
     *     
     */
    public ENDType.APP getAPP() {
        return app;
    }

    /**
     * Sets the value of the app property.
     * 
     * @param value
     *     allowed object is
     *     {@link ENDType.APP }
     *     
     */
    public void setAPP(ENDType.APP value) {
        this.app = value;
    }

    /**
     * Gets the value of the aut property.
     * 
     * @return
     *     possible object is
     *     {@link ENDType.AUT }
     *     
     */
    public ENDType.AUT getAUT() {
        return aut;
    }

    /**
     * Sets the value of the aut property.
     * 
     * @param value
     *     allowed object is
     *     {@link ENDType.AUT }
     *     
     */
    public void setAUT(ENDType.AUT value) {
        this.aut = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="NAM" type="{http://twm.ncs.gov.ng/formm/xsd}X128" minOccurs="0"/>
     *         &lt;element name="DAT" type="{http://twm.ncs.gov.ng/formm/xsd}date" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nam",
        "dat"
    })
    public static class APP {

        @XmlElement(name = "NAM", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String nam;
        @XmlElement(name = "DAT", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String dat;

        /**
         * Gets the value of the nam property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNAM() {
            return nam;
        }

        /**
         * Sets the value of the nam property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNAM(String value) {
            this.nam = value;
        }

        /**
         * Gets the value of the dat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDAT() {
            return dat;
        }

        /**
         * Sets the value of the dat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDAT(String value) {
            this.dat = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="NAM" type="{http://twm.ncs.gov.ng/formm/xsd}X128" minOccurs="0"/>
     *         &lt;element name="DAT" type="{http://twm.ncs.gov.ng/formm/xsd}date" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nam",
        "dat"
    })
    public static class AUT {

        @XmlElement(name = "NAM", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String nam;
        @XmlElement(name = "DAT", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String dat;

        /**
         * Gets the value of the nam property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNAM() {
            return nam;
        }

        /**
         * Sets the value of the nam property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNAM(String value) {
            this.nam = value;
        }

        /**
         * Gets the value of the dat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDAT() {
            return dat;
        }

        /**
         * Sets the value of the dat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDAT(String value) {
            this.dat = value;
        }

    }

}
