
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * Flag
 * 
 * <p>Java class for flg complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="flg">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FLG" type="{http://twm.ncs.gov.ng/formm/xsd}boolean"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "flg", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "flg"
})
public class Flg {

    @XmlElement(name = "FLG", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String flg;

    /**
     * Gets the value of the flg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFLG() {
        return flg;
    }

    /**
     * Sets the value of the flg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFLG(String value) {
        this.flg = value;
    }

}
