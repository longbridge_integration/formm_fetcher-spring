
package com.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formMAlreadyDownloadedType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formMAlreadyDownloadedType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}INF"/>
 *         &lt;element name="SUP_INF" type="{http://twm.ncs.gov.ng/formm/xsd}SupportInformationType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formMAlreadyDownloadedType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "inf",
    "supinf"
})
public class FormMAlreadyDownloadedType {

    @XmlElement(name = "INF", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected INFType inf;
    @XmlElement(name = "SUP_INF", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected List<SupportInformationType> supinf;

    /**
     * Gets the value of the inf property.
     * 
     * @return
     *     possible object is
     *     {@link INFType }
     *     
     */
    public INFType getINF() {
        return inf;
    }

    /**
     * Sets the value of the inf property.
     * 
     * @param value
     *     allowed object is
     *     {@link INFType }
     *     
     */
    public void setINF(INFType value) {
        this.inf = value;
    }

    /**
     * Gets the value of the supinf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supinf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSUPINF().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupportInformationType }
     * 
     * 
     */
    public List<SupportInformationType> getSUPINF() {
        if (supinf == null) {
            supinf = new ArrayList<SupportInformationType>();
        }
        return this.supinf;
    }

}
