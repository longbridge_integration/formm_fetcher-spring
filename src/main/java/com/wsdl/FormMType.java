
package com.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for formMType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="formMType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}INF"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}APP"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}BEN"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}GDS"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}BNK"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}SCA"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}END"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}ITMS"/>
 *         &lt;element name="STA" type="{http://twm.ncs.gov.ng/formm/xsd}statusType"/>
 *         &lt;element name="ATDS" type="{http://twm.ncs.gov.ng/formm/xsd}AttachmentType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="SUP_INF" type="{http://twm.ncs.gov.ng/formm/xsd}SupportInformationType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "formMType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "inf",
    "app",
    "ben",
    "gds",
    "tsp",
    "bnk",
    "sca",
    "end",
    "itms",
    "sta",
    "atds",
    "supinf"
})
public class FormMType {

    @XmlElement(name = "INF", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected INFType inf;
    @XmlElement(name = "APP", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected APPType app;
    @XmlElement(name = "BEN", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected BENType ben;
    @XmlElement(name = "GDS", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected GDSType gds;
    @XmlElement(name = "TSP", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected TSPType tsp;
    @XmlElement(name = "BNK", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected FormMType.BNK bnk;
    @XmlElement(name = "SCA", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected Cod7 sca;
    @XmlElement(name = "END", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected ENDType end;
    @XmlElement(name = "ITMS", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected List<ITMType> itms;
    @XmlElement(name = "STA", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    @XmlSchemaType(name = "string")
    protected StatusType sta;
    @XmlElement(name = "ATDS", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected List<AttachmentType> atds;
    @XmlElement(name = "SUP_INF", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected List<SupportInformationType> supinf;

    /**
     * Gets the value of the inf property.
     * 
     * @return
     *     possible object is
     *     {@link INFType }
     *     
     */
    public INFType getINF() {
        return inf;
    }

    /**
     * Sets the value of the inf property.
     * 
     * @param value
     *     allowed object is
     *     {@link INFType }
     *     
     */
    public void setINF(INFType value) {
        this.inf = value;
    }

    /**
     * Gets the value of the app property.
     * 
     * @return
     *     possible object is
     *     {@link APPType }
     *     
     */
    public APPType getAPP() {
        return app;
    }

    /**
     * Sets the value of the app property.
     * 
     * @param value
     *     allowed object is
     *     {@link APPType }
     *     
     */
    public void setAPP(APPType value) {
        this.app = value;
    }

    /**
     * Gets the value of the ben property.
     * 
     * @return
     *     possible object is
     *     {@link BENType }
     *     
     */
    public BENType getBEN() {
        return ben;
    }

    /**
     * Sets the value of the ben property.
     * 
     * @param value
     *     allowed object is
     *     {@link BENType }
     *     
     */
    public void setBEN(BENType value) {
        this.ben = value;
    }

    /**
     * Gets the value of the gds property.
     * 
     * @return
     *     possible object is
     *     {@link GDSType }
     *     
     */
    public GDSType getGDS() {
        return gds;
    }

    /**
     * Sets the value of the gds property.
     * 
     * @param value
     *     allowed object is
     *     {@link GDSType }
     *     
     */
    public void setGDS(GDSType value) {
        this.gds = value;
    }

    /**
     * Gets the value of the tsp property.
     * 
     * @return
     *     possible object is
     *     {@link TSPType }
     *     
     */
    public TSPType getTSP() {
        return tsp;
    }

    /**
     * Sets the value of the tsp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TSPType }
     *     
     */
    public void setTSP(TSPType value) {
        this.tsp = value;
    }

    /**
     * Gets the value of the bnk property.
     * 
     * @return
     *     possible object is
     *     {@link FormMType.BNK }
     *     
     */
    public FormMType.BNK getBNK() {
        return bnk;
    }

    /**
     * Sets the value of the bnk property.
     * 
     * @param value
     *     allowed object is
     *     {@link FormMType.BNK }
     *     
     */
    public void setBNK(FormMType.BNK value) {
        this.bnk = value;
    }

    /**
     * Gets the value of the sca property.
     * 
     * @return
     *     possible object is
     *     {@link Cod7 }
     *     
     */
    public Cod7 getSCA() {
        return sca;
    }

    /**
     * Sets the value of the sca property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod7 }
     *     
     */
    public void setSCA(Cod7 value) {
        this.sca = value;
    }

    /**
     * Gets the value of the end property.
     * 
     * @return
     *     possible object is
     *     {@link ENDType }
     *     
     */
    public ENDType getEND() {
        return end;
    }

    /**
     * Sets the value of the end property.
     * 
     * @param value
     *     allowed object is
     *     {@link ENDType }
     *     
     */
    public void setEND(ENDType value) {
        this.end = value;
    }

    /**
     * Gets the value of the itms property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the itms property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getITMS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ITMType }
     * 
     * 
     */
    public List<ITMType> getITMS() {
        if (itms == null) {
            itms = new ArrayList<ITMType>();
        }
        return this.itms;
    }

    /**
     * Gets the value of the sta property.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getSTA() {
        return sta;
    }

    /**
     * Sets the value of the sta property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setSTA(StatusType value) {
        this.sta = value;
    }

    /**
     * Gets the value of the atds property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the atds property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getATDS().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link AttachmentType }
     * 
     * 
     */
    public List<AttachmentType> getATDS() {
        if (atds == null) {
            atds = new ArrayList<AttachmentType>();
        }
        return this.atds;
    }

    /**
     * Gets the value of the supinf property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the supinf property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getSUPINF().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link SupportInformationType }
     * 
     * 
     */
    public List<SupportInformationType> getSUPINF() {
        if (supinf == null) {
            supinf = new ArrayList<SupportInformationType>();
        }
        return this.supinf;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="COD" type="{http://twm.ncs.gov.ng/formm/xsd}X5"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "cod"
    })
    public static class BNK {

        @XmlElement(name = "COD", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String cod;

        /**
         * Gets the value of the cod property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOD() {
            return cod;
        }

        /**
         * Sets the value of the cod property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOD(String value) {
            this.cod = value;
        }

    }

}
