
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for GDSType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="GDSType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="GEN">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DSC" type="{http://twm.ncs.gov.ng/formm/xsd}X512"/>
 *                   &lt;element name="WGT" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
 *                   &lt;element name="GRS" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
 *                   &lt;element name="MEA" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3" minOccurs="0"/>
 *                   &lt;element name="TIT" type="{http://twm.ncs.gov.ng/formm/xsd}integer"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="VAL">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="FOB" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
 *                   &lt;element name="FRG" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
 *                   &lt;element name="TAC" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
 *                   &lt;element name="CUR" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3"/>
 *                   &lt;element name="EXC" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
 *                   &lt;element name="INC" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
 *                   &lt;element name="TCF">
 *                     &lt;complexType>
 *                       &lt;complexContent>
 *                         &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                           &lt;sequence>
 *                             &lt;element name="VAL" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
 *                           &lt;/sequence>
 *                         &lt;/restriction>
 *                       &lt;/complexContent>
 *                     &lt;/complexType>
 *                   &lt;/element>
 *                   &lt;element name="SRC" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="PRO">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="INV" type="{http://twm.ncs.gov.ng/formm/xsd}X35"/>
 *                   &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}DAT"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TOD" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3" minOccurs="0"/>
 *         &lt;element name="MOP" type="{http://twm.ncs.gov.ng/formm/xsd}codnam_3" minOccurs="0"/>
 *         &lt;element name="PAY" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DAT" type="{http://twm.ncs.gov.ng/formm/xsd}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="TRF" type="{http://twm.ncs.gov.ng/formm/xsd}codnam_3" minOccurs="0"/>
 *         &lt;element name="MOT" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3" minOccurs="0"/>
 *         &lt;element name="DIS" type="{http://twm.ncs.gov.ng/formm/xsd}cod_5"/>
 *         &lt;element name="DST" type="{http://twm.ncs.gov.ng/formm/xsd}cod_5" minOccurs="0"/>
 *         &lt;element name="CUO" type="{http://twm.ncs.gov.ng/formm/xsd}cod_5"/>
 *         &lt;element name="CTY">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="SUP" type="{http://twm.ncs.gov.ng/formm/xsd}cod_2"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "GDSType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "gen",
    "val",
    "pro",
    "tod",
    "mop",
    "pay",
    "trf",
    "mot",
    "dis",
    "dst",
    "cuo",
    "cty"
})
public class GDSType {

    @XmlElement(name = "GEN", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected GDSType.GEN gen;
    @XmlElement(name = "VAL", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected GDSType.VAL val;
    @XmlElement(name = "PRO", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected GDSType.PRO pro;
    @XmlElement(name = "TOD", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod3 tod;
    @XmlElement(name = "MOP", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Codnam3 mop;
    @XmlElement(name = "PAY", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected GDSType.PAY pay;
    @XmlElement(name = "TRF", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Codnam3 trf;
    @XmlElement(name = "MOT", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod3 mot;
    @XmlElement(name = "DIS", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected Cod5 dis;
    @XmlElement(name = "DST", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod5 dst;
    @XmlElement(name = "CUO", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected Cod5 cuo;
    @XmlElement(name = "CTY", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected GDSType.CTY cty;

    /**
     * Gets the value of the gen property.
     * 
     * @return
     *     possible object is
     *     {@link GDSType.GEN }
     *     
     */
    public GDSType.GEN getGEN() {
        return gen;
    }

    /**
     * Sets the value of the gen property.
     * 
     * @param value
     *     allowed object is
     *     {@link GDSType.GEN }
     *     
     */
    public void setGEN(GDSType.GEN value) {
        this.gen = value;
    }

    /**
     * Gets the value of the val property.
     * 
     * @return
     *     possible object is
     *     {@link GDSType.VAL }
     *     
     */
    public GDSType.VAL getVAL() {
        return val;
    }

    /**
     * Sets the value of the val property.
     * 
     * @param value
     *     allowed object is
     *     {@link GDSType.VAL }
     *     
     */
    public void setVAL(GDSType.VAL value) {
        this.val = value;
    }

    /**
     * Gets the value of the pro property.
     * 
     * @return
     *     possible object is
     *     {@link GDSType.PRO }
     *     
     */
    public GDSType.PRO getPRO() {
        return pro;
    }

    /**
     * Sets the value of the pro property.
     * 
     * @param value
     *     allowed object is
     *     {@link GDSType.PRO }
     *     
     */
    public void setPRO(GDSType.PRO value) {
        this.pro = value;
    }

    /**
     * Gets the value of the tod property.
     * 
     * @return
     *     possible object is
     *     {@link Cod3 }
     *     
     */
    public Cod3 getTOD() {
        return tod;
    }

    /**
     * Sets the value of the tod property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod3 }
     *     
     */
    public void setTOD(Cod3 value) {
        this.tod = value;
    }

    /**
     * Gets the value of the mop property.
     * 
     * @return
     *     possible object is
     *     {@link Codnam3 }
     *     
     */
    public Codnam3 getMOP() {
        return mop;
    }

    /**
     * Sets the value of the mop property.
     * 
     * @param value
     *     allowed object is
     *     {@link Codnam3 }
     *     
     */
    public void setMOP(Codnam3 value) {
        this.mop = value;
    }

    /**
     * Gets the value of the pay property.
     * 
     * @return
     *     possible object is
     *     {@link GDSType.PAY }
     *     
     */
    public GDSType.PAY getPAY() {
        return pay;
    }

    /**
     * Sets the value of the pay property.
     * 
     * @param value
     *     allowed object is
     *     {@link GDSType.PAY }
     *     
     */
    public void setPAY(GDSType.PAY value) {
        this.pay = value;
    }

    /**
     * Gets the value of the trf property.
     * 
     * @return
     *     possible object is
     *     {@link Codnam3 }
     *     
     */
    public Codnam3 getTRF() {
        return trf;
    }

    /**
     * Sets the value of the trf property.
     * 
     * @param value
     *     allowed object is
     *     {@link Codnam3 }
     *     
     */
    public void setTRF(Codnam3 value) {
        this.trf = value;
    }

    /**
     * Gets the value of the mot property.
     * 
     * @return
     *     possible object is
     *     {@link Cod3 }
     *     
     */
    public Cod3 getMOT() {
        return mot;
    }

    /**
     * Sets the value of the mot property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod3 }
     *     
     */
    public void setMOT(Cod3 value) {
        this.mot = value;
    }

    /**
     * Gets the value of the dis property.
     * 
     * @return
     *     possible object is
     *     {@link Cod5 }
     *     
     */
    public Cod5 getDIS() {
        return dis;
    }

    /**
     * Sets the value of the dis property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod5 }
     *     
     */
    public void setDIS(Cod5 value) {
        this.dis = value;
    }

    /**
     * Gets the value of the dst property.
     * 
     * @return
     *     possible object is
     *     {@link Cod5 }
     *     
     */
    public Cod5 getDST() {
        return dst;
    }

    /**
     * Sets the value of the dst property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod5 }
     *     
     */
    public void setDST(Cod5 value) {
        this.dst = value;
    }

    /**
     * Gets the value of the cuo property.
     * 
     * @return
     *     possible object is
     *     {@link Cod5 }
     *     
     */
    public Cod5 getCUO() {
        return cuo;
    }

    /**
     * Sets the value of the cuo property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod5 }
     *     
     */
    public void setCUO(Cod5 value) {
        this.cuo = value;
    }

    /**
     * Gets the value of the cty property.
     * 
     * @return
     *     possible object is
     *     {@link GDSType.CTY }
     *     
     */
    public GDSType.CTY getCTY() {
        return cty;
    }

    /**
     * Sets the value of the cty property.
     * 
     * @param value
     *     allowed object is
     *     {@link GDSType.CTY }
     *     
     */
    public void setCTY(GDSType.CTY value) {
        this.cty = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="SUP" type="{http://twm.ncs.gov.ng/formm/xsd}cod_2"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "sup"
    })
    public static class CTY {

        @XmlElement(name = "SUP", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected Cod2 sup;

        /**
         * Gets the value of the sup property.
         * 
         * @return
         *     possible object is
         *     {@link Cod2 }
         *     
         */
        public Cod2 getSUP() {
            return sup;
        }

        /**
         * Sets the value of the sup property.
         * 
         * @param value
         *     allowed object is
         *     {@link Cod2 }
         *     
         */
        public void setSUP(Cod2 value) {
            this.sup = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DSC" type="{http://twm.ncs.gov.ng/formm/xsd}X512"/>
     *         &lt;element name="WGT" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
     *         &lt;element name="GRS" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
     *         &lt;element name="MEA" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3" minOccurs="0"/>
     *         &lt;element name="TIT" type="{http://twm.ncs.gov.ng/formm/xsd}integer"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dsc",
        "wgt",
        "grs",
        "mea",
        "tit"
    })
    public static class GEN {

        @XmlElement(name = "DSC", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String dsc;
        @XmlElement(name = "WGT", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String wgt;
        @XmlElement(name = "GRS", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String grs;
        @XmlElement(name = "MEA", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected Cod3 mea;
        @XmlElement(name = "TIT", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected int tit;

        /**
         * Gets the value of the dsc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDSC() {
            return dsc;
        }

        /**
         * Sets the value of the dsc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDSC(String value) {
            this.dsc = value;
        }

        /**
         * Gets the value of the wgt property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getWGT() {
            return wgt;
        }

        /**
         * Sets the value of the wgt property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setWGT(String value) {
            this.wgt = value;
        }

        /**
         * Gets the value of the grs property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGRS() {
            return grs;
        }

        /**
         * Sets the value of the grs property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGRS(String value) {
            this.grs = value;
        }

        /**
         * Gets the value of the mea property.
         * 
         * @return
         *     possible object is
         *     {@link Cod3 }
         *     
         */
        public Cod3 getMEA() {
            return mea;
        }

        /**
         * Sets the value of the mea property.
         * 
         * @param value
         *     allowed object is
         *     {@link Cod3 }
         *     
         */
        public void setMEA(Cod3 value) {
            this.mea = value;
        }

        /**
         * Gets the value of the tit property.
         * 
         */
        public int getTIT() {
            return tit;
        }

        /**
         * Sets the value of the tit property.
         * 
         */
        public void setTIT(int value) {
            this.tit = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DAT" type="{http://twm.ncs.gov.ng/formm/xsd}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dat"
    })
    public static class PAY {

        @XmlElement(name = "DAT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String dat;

        /**
         * Gets the value of the dat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDAT() {
            return dat;
        }

        /**
         * Sets the value of the dat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDAT(String value) {
            this.dat = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="INV" type="{http://twm.ncs.gov.ng/formm/xsd}X35"/>
     *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}DAT"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "inv",
        "dat"
    })
    public static class PRO {

        @XmlElement(name = "INV", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String inv;
        @XmlElement(name = "DAT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String dat;

        /**
         * Gets the value of the inv property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINV() {
            return inv;
        }

        /**
         * Sets the value of the inv property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINV(String value) {
            this.inv = value;
        }

        /**
         * Gets the value of the dat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDAT() {
            return dat;
        }

        /**
         * Sets the value of the dat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDAT(String value) {
            this.dat = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="FOB" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
     *         &lt;element name="FRG" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
     *         &lt;element name="TAC" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
     *         &lt;element name="CUR" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3"/>
     *         &lt;element name="EXC" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
     *         &lt;element name="INC" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
     *         &lt;element name="TCF">
     *           &lt;complexType>
     *             &lt;complexContent>
     *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *                 &lt;sequence>
     *                   &lt;element name="VAL" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
     *                 &lt;/sequence>
     *               &lt;/restriction>
     *             &lt;/complexContent>
     *           &lt;/complexType>
     *         &lt;/element>
     *         &lt;element name="SRC" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "fob",
        "frg",
        "tac",
        "cur",
        "exc",
        "inc",
        "tcf",
        "src"
    })
    public static class VAL {

        @XmlElement(name = "FOB", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String fob;
        @XmlElement(name = "FRG", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String frg;
        @XmlElement(name = "TAC", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String tac;
        @XmlElement(name = "CUR", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected Cod3 cur;
        @XmlElement(name = "EXC", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String exc;
        @XmlElement(name = "INC", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String inc;
        @XmlElement(name = "TCF", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected GDSType.VAL.TCF tcf;
        @XmlElement(name = "SRC", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected Cod3 src;

        /**
         * Gets the value of the fob property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFOB() {
            return fob;
        }

        /**
         * Sets the value of the fob property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFOB(String value) {
            this.fob = value;
        }

        /**
         * Gets the value of the frg property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getFRG() {
            return frg;
        }

        /**
         * Sets the value of the frg property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setFRG(String value) {
            this.frg = value;
        }

        /**
         * Gets the value of the tac property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTAC() {
            return tac;
        }

        /**
         * Sets the value of the tac property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTAC(String value) {
            this.tac = value;
        }

        /**
         * Gets the value of the cur property.
         * 
         * @return
         *     possible object is
         *     {@link Cod3 }
         *     
         */
        public Cod3 getCUR() {
            return cur;
        }

        /**
         * Sets the value of the cur property.
         * 
         * @param value
         *     allowed object is
         *     {@link Cod3 }
         *     
         */
        public void setCUR(Cod3 value) {
            this.cur = value;
        }

        /**
         * Gets the value of the exc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getEXC() {
            return exc;
        }

        /**
         * Sets the value of the exc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setEXC(String value) {
            this.exc = value;
        }

        /**
         * Gets the value of the inc property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getINC() {
            return inc;
        }

        /**
         * Sets the value of the inc property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setINC(String value) {
            this.inc = value;
        }

        /**
         * Gets the value of the tcf property.
         * 
         * @return
         *     possible object is
         *     {@link GDSType.VAL.TCF }
         *     
         */
        public GDSType.VAL.TCF getTCF() {
            return tcf;
        }

        /**
         * Sets the value of the tcf property.
         * 
         * @param value
         *     allowed object is
         *     {@link GDSType.VAL.TCF }
         *     
         */
        public void setTCF(GDSType.VAL.TCF value) {
            this.tcf = value;
        }

        /**
         * Gets the value of the src property.
         * 
         * @return
         *     possible object is
         *     {@link Cod3 }
         *     
         */
        public Cod3 getSRC() {
            return src;
        }

        /**
         * Sets the value of the src property.
         * 
         * @param value
         *     allowed object is
         *     {@link Cod3 }
         *     
         */
        public void setSRC(Cod3 value) {
            this.src = value;
        }


        /**
         * <p>Java class for anonymous complex type.
         * 
         * <p>The following schema fragment specifies the expected content contained within this class.
         * 
         * <pre>
         * &lt;complexType>
         *   &lt;complexContent>
         *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
         *       &lt;sequence>
         *         &lt;element name="VAL" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
         *       &lt;/sequence>
         *     &lt;/restriction>
         *   &lt;/complexContent>
         * &lt;/complexType>
         * </pre>
         * 
         * 
         */
        @XmlAccessorType(XmlAccessType.FIELD)
        @XmlType(name = "", propOrder = {
            "val"
        })
        public static class TCF {

            @XmlElement(name = "VAL", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
            protected String val;

            /**
             * Gets the value of the val property.
             * 
             * @return
             *     possible object is
             *     {@link String }
             *     
             */
            public String getVAL() {
                return val;
            }

            /**
             * Sets the value of the val property.
             * 
             * @param value
             *     allowed object is
             *     {@link String }
             *     
             */
            public void setVAL(String value) {
                this.val = value;
            }

        }

    }

}
