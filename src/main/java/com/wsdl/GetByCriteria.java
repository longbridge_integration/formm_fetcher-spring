
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fromDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="toDate" type="{http://www.w3.org/2001/XMLSchema}dateTime" minOccurs="0"/>
 *         &lt;element name="bnkCode" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3" minOccurs="0"/>
 *         &lt;element name="status" type="{http://twm.ncs.gov.ng/formm/xsd}statusType" minOccurs="0"/>
 *         &lt;element name="scaCode" type="{http://twm.ncs.gov.ng/formm/xsd}cod_7" minOccurs="0"/>
 *         &lt;element name="curCode" type="{http://twm.ncs.gov.ng/formm/xsd}cod_3" minOccurs="0"/>
 *         &lt;element name="ctySupply" type="{http://twm.ncs.gov.ng/formm/xsd}cod_2" minOccurs="0"/>
 *         &lt;element name="cuoCode" type="{http://twm.ncs.gov.ng/formm/xsd}cod_5" minOccurs="0"/>
 *         &lt;element name="ctyOrigin" type="{http://twm.ncs.gov.ng/formm/xsd}cod_2" minOccurs="0"/>
 *         &lt;element name="excludeAttachment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fromDate",
    "toDate",
    "bnkCode",
    "status",
    "scaCode",
    "curCode",
    "ctySupply",
    "cuoCode",
    "ctyOrigin",
    "excludeAttachment"
})
@XmlRootElement(name = "getByCriteria", namespace = "http://twm.ncs.gov.ng/formm/xsd")
public class GetByCriteria {

    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fromDate;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar toDate;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod3 bnkCode;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    @XmlSchemaType(name = "string")
    protected StatusType status;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod7 scaCode;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod3 curCode;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod2 ctySupply;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod5 cuoCode;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod2 ctyOrigin;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Boolean excludeAttachment;

    /**
     * Gets the value of the fromDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFromDate() {
        return fromDate;
    }

    /**
     * Sets the value of the fromDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFromDate(XMLGregorianCalendar value) {
        this.fromDate = value;
    }

    /**
     * Gets the value of the toDate property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getToDate() {
        return toDate;
    }

    /**
     * Sets the value of the toDate property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setToDate(XMLGregorianCalendar value) {
        this.toDate = value;
    }

    /**
     * Gets the value of the bnkCode property.
     * 
     * @return
     *     possible object is
     *     {@link Cod3 }
     *     
     */
    public Cod3 getBnkCode() {
        return bnkCode;
    }

    /**
     * Sets the value of the bnkCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod3 }
     *     
     */
    public void setBnkCode(Cod3 value) {
        this.bnkCode = value;
    }

    /**
     * Gets the value of the status property.
     * 
     * @return
     *     possible object is
     *     {@link StatusType }
     *     
     */
    public StatusType getStatus() {
        return status;
    }

    /**
     * Sets the value of the status property.
     * 
     * @param value
     *     allowed object is
     *     {@link StatusType }
     *     
     */
    public void setStatus(StatusType value) {
        this.status = value;
    }

    /**
     * Gets the value of the scaCode property.
     * 
     * @return
     *     possible object is
     *     {@link Cod7 }
     *     
     */
    public Cod7 getScaCode() {
        return scaCode;
    }

    /**
     * Sets the value of the scaCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod7 }
     *     
     */
    public void setScaCode(Cod7 value) {
        this.scaCode = value;
    }

    /**
     * Gets the value of the curCode property.
     * 
     * @return
     *     possible object is
     *     {@link Cod3 }
     *     
     */
    public Cod3 getCurCode() {
        return curCode;
    }

    /**
     * Sets the value of the curCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod3 }
     *     
     */
    public void setCurCode(Cod3 value) {
        this.curCode = value;
    }

    /**
     * Gets the value of the ctySupply property.
     * 
     * @return
     *     possible object is
     *     {@link Cod2 }
     *     
     */
    public Cod2 getCtySupply() {
        return ctySupply;
    }

    /**
     * Sets the value of the ctySupply property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod2 }
     *     
     */
    public void setCtySupply(Cod2 value) {
        this.ctySupply = value;
    }

    /**
     * Gets the value of the cuoCode property.
     * 
     * @return
     *     possible object is
     *     {@link Cod5 }
     *     
     */
    public Cod5 getCuoCode() {
        return cuoCode;
    }

    /**
     * Sets the value of the cuoCode property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod5 }
     *     
     */
    public void setCuoCode(Cod5 value) {
        this.cuoCode = value;
    }

    /**
     * Gets the value of the ctyOrigin property.
     * 
     * @return
     *     possible object is
     *     {@link Cod2 }
     *     
     */
    public Cod2 getCtyOrigin() {
        return ctyOrigin;
    }

    /**
     * Sets the value of the ctyOrigin property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod2 }
     *     
     */
    public void setCtyOrigin(Cod2 value) {
        this.ctyOrigin = value;
    }

    /**
     * Gets the value of the excludeAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExcludeAttachment() {
        return excludeAttachment;
    }

    /**
     * Sets the value of the excludeAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeAttachment(Boolean value) {
        this.excludeAttachment = value;
    }

}
