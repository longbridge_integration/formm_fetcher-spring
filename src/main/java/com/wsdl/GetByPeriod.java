
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="fromTimeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="toTimeStamp" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="excludeAttachment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "fromTimeStamp",
    "toTimeStamp",
    "excludeAttachment"
})
@XmlRootElement(name = "getByPeriod", namespace = "http://twm.ncs.gov.ng/formm/xsd")
public class GetByPeriod {

    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar fromTimeStamp;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar toTimeStamp;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Boolean excludeAttachment;

    /**
     * Gets the value of the fromTimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getFromTimeStamp() {
        return fromTimeStamp;
    }

    /**
     * Sets the value of the fromTimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setFromTimeStamp(XMLGregorianCalendar value) {
        this.fromTimeStamp = value;
    }

    /**
     * Gets the value of the toTimeStamp property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getToTimeStamp() {
        return toTimeStamp;
    }

    /**
     * Sets the value of the toTimeStamp property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setToTimeStamp(XMLGregorianCalendar value) {
        this.toTimeStamp = value;
    }

    /**
     * Gets the value of the excludeAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExcludeAttachment() {
        return excludeAttachment;
    }

    /**
     * Sets the value of the excludeAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeAttachment(Boolean value) {
        this.excludeAttachment = value;
    }

}
