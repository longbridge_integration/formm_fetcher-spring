
package com.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for anonymous complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="formMNumber" type="{http://twm.ncs.gov.ng/formm/xsd}FormM" maxOccurs="unbounded"/>
 *         &lt;element name="excludeAttachment" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "formMNumber",
    "excludeAttachment"
})
@XmlRootElement(name = "getByReference", namespace = "http://twm.ncs.gov.ng/formm/xsd")
public class GetByReference {

    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected List<String> formMNumber;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Boolean excludeAttachment;

    /**
     * Gets the value of the formMNumber property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formMNumber property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormMNumber().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getFormMNumber() {
        if (formMNumber == null) {
            formMNumber = new ArrayList<String>();
        }
        return this.formMNumber;
    }

    /**
     * Gets the value of the excludeAttachment property.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isExcludeAttachment() {
        return excludeAttachment;
    }

    /**
     * Sets the value of the excludeAttachment property.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setExcludeAttachment(Boolean value) {
        this.excludeAttachment = value;
    }

}
