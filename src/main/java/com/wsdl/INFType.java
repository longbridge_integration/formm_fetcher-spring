
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for INFType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="INFType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="FRX" type="{http://twm.ncs.gov.ng/formm/xsd}boolean"/>
 *         &lt;element name="YEA" type="{http://twm.ncs.gov.ng/formm/xsd}year" minOccurs="0"/>
 *         &lt;element name="SER" type="{http://twm.ncs.gov.ng/formm/xsd}FormMSer"/>
 *         &lt;element name="PFX" type="{http://twm.ncs.gov.ng/formm/xsd}PFXType"/>
 *         &lt;element name="BNK" type="{http://twm.ncs.gov.ng/formm/xsd}cod_1015"/>
 *         &lt;element name="VLD" type="{http://twm.ncs.gov.ng/formm/xsd}date" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "INFType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "frx",
    "yea",
    "ser",
    "pfx",
    "bnk",
    "vld"
})
public class INFType {

    @XmlElement(name = "FRX", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String frx;
    @XmlElement(name = "YEA", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String yea;
    @XmlElement(name = "SER", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String ser;
    @XmlElement(name = "PFX", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    @XmlSchemaType(name = "string")
    protected PFXType pfx;
    @XmlElement(name = "BNK", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected Cod1015 bnk;
    @XmlElement(name = "VLD", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String vld;

    /**
     * Gets the value of the frx property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRX() {
        return frx;
    }

    /**
     * Sets the value of the frx property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRX(String value) {
        this.frx = value;
    }

    /**
     * Gets the value of the yea property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getYEA() {
        return yea;
    }

    /**
     * Sets the value of the yea property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setYEA(String value) {
        this.yea = value;
    }

    /**
     * Gets the value of the ser property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSER() {
        return ser;
    }

    /**
     * Sets the value of the ser property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSER(String value) {
        this.ser = value;
    }

    /**
     * Gets the value of the pfx property.
     * 
     * @return
     *     possible object is
     *     {@link PFXType }
     *     
     */
    public PFXType getPFX() {
        return pfx;
    }

    /**
     * Sets the value of the pfx property.
     * 
     * @param value
     *     allowed object is
     *     {@link PFXType }
     *     
     */
    public void setPFX(PFXType value) {
        this.pfx = value;
    }

    /**
     * Gets the value of the bnk property.
     * 
     * @return
     *     possible object is
     *     {@link Cod1015 }
     *     
     */
    public Cod1015 getBNK() {
        return bnk;
    }

    /**
     * Sets the value of the bnk property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod1015 }
     *     
     */
    public void setBNK(Cod1015 value) {
        this.bnk = value;
    }

    /**
     * Gets the value of the vld property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVLD() {
        return vld;
    }

    /**
     * Sets the value of the vld property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVLD(String value) {
        this.vld = value;
    }

}
