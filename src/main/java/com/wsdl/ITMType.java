
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ITMType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ITMType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="RNK" type="{http://twm.ncs.gov.ng/formm/xsd}integer"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}HSC" minOccurs="0"/>
 *         &lt;element name="DSC" type="{http://twm.ncs.gov.ng/formm/xsd}X512"/>
 *         &lt;element name="FRG" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
 *         &lt;element name="SOG" type="{http://twm.ncs.gov.ng/formm/xsd}cod_1" minOccurs="0"/>
 *         &lt;element name="SEP" type="{http://twm.ncs.gov.ng/formm/xsd}cod_5" minOccurs="0"/>
 *         &lt;element name="PKG" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="NBR" type="{http://twm.ncs.gov.ng/formm/xsd}integer" minOccurs="0"/>
 *                   &lt;element name="COD" type="{http://twm.ncs.gov.ng/formm/xsd}X2" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="FOB">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="VAL" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="QTY" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
 *         &lt;element name="UNT">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="PRI" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
 *                   &lt;element name="COD" type="{http://twm.ncs.gov.ng/formm/xsd}X3" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="WGT">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="GRS" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
 *                   &lt;element name="NET" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="CTY">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="ORG" type="{http://twm.ncs.gov.ng/formm/xsd}cod_2"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ITMType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "rnk",
    "hsc",
    "dsc",
    "frg",
    "sog",
    "sep",
    "pkg",
    "fob",
    "qty",
    "unt",
    "wgt",
    "cty"
})
public class ITMType {

    @XmlElement(name = "RNK", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected int rnk;
    @XmlElement(name = "HSC", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String hsc;
    @XmlElement(name = "DSC", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String dsc;
    @XmlElement(name = "FRG", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String frg;
    @XmlElement(name = "SOG", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod1 sog;
    @XmlElement(name = "SEP", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected Cod5 sep;
    @XmlElement(name = "PKG", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected ITMType.PKG pkg;
    @XmlElement(name = "FOB", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected ITMType.FOB fob;
    @XmlElement(name = "QTY", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String qty;
    @XmlElement(name = "UNT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected ITMType.UNT unt;
    @XmlElement(name = "WGT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected ITMType.WGT wgt;
    @XmlElement(name = "CTY", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected ITMType.CTY cty;

    /**
     * Gets the value of the rnk property.
     * 
     */
    public int getRNK() {
        return rnk;
    }

    /**
     * Sets the value of the rnk property.
     * 
     */
    public void setRNK(int value) {
        this.rnk = value;
    }

    /**
     * Gets the value of the hsc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getHSC() {
        return hsc;
    }

    /**
     * Sets the value of the hsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setHSC(String value) {
        this.hsc = value;
    }

    /**
     * Gets the value of the dsc property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDSC() {
        return dsc;
    }

    /**
     * Sets the value of the dsc property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDSC(String value) {
        this.dsc = value;
    }

    /**
     * Gets the value of the frg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFRG() {
        return frg;
    }

    /**
     * Sets the value of the frg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFRG(String value) {
        this.frg = value;
    }

    /**
     * Gets the value of the sog property.
     * 
     * @return
     *     possible object is
     *     {@link Cod1 }
     *     
     */
    public Cod1 getSOG() {
        return sog;
    }

    /**
     * Sets the value of the sog property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod1 }
     *     
     */
    public void setSOG(Cod1 value) {
        this.sog = value;
    }

    /**
     * Gets the value of the sep property.
     * 
     * @return
     *     possible object is
     *     {@link Cod5 }
     *     
     */
    public Cod5 getSEP() {
        return sep;
    }

    /**
     * Sets the value of the sep property.
     * 
     * @param value
     *     allowed object is
     *     {@link Cod5 }
     *     
     */
    public void setSEP(Cod5 value) {
        this.sep = value;
    }

    /**
     * Gets the value of the pkg property.
     * 
     * @return
     *     possible object is
     *     {@link ITMType.PKG }
     *     
     */
    public ITMType.PKG getPKG() {
        return pkg;
    }

    /**
     * Sets the value of the pkg property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITMType.PKG }
     *     
     */
    public void setPKG(ITMType.PKG value) {
        this.pkg = value;
    }

    /**
     * Gets the value of the fob property.
     * 
     * @return
     *     possible object is
     *     {@link ITMType.FOB }
     *     
     */
    public ITMType.FOB getFOB() {
        return fob;
    }

    /**
     * Sets the value of the fob property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITMType.FOB }
     *     
     */
    public void setFOB(ITMType.FOB value) {
        this.fob = value;
    }

    /**
     * Gets the value of the qty property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getQTY() {
        return qty;
    }

    /**
     * Sets the value of the qty property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setQTY(String value) {
        this.qty = value;
    }

    /**
     * Gets the value of the unt property.
     * 
     * @return
     *     possible object is
     *     {@link ITMType.UNT }
     *     
     */
    public ITMType.UNT getUNT() {
        return unt;
    }

    /**
     * Sets the value of the unt property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITMType.UNT }
     *     
     */
    public void setUNT(ITMType.UNT value) {
        this.unt = value;
    }

    /**
     * Gets the value of the wgt property.
     * 
     * @return
     *     possible object is
     *     {@link ITMType.WGT }
     *     
     */
    public ITMType.WGT getWGT() {
        return wgt;
    }

    /**
     * Sets the value of the wgt property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITMType.WGT }
     *     
     */
    public void setWGT(ITMType.WGT value) {
        this.wgt = value;
    }

    /**
     * Gets the value of the cty property.
     * 
     * @return
     *     possible object is
     *     {@link ITMType.CTY }
     *     
     */
    public ITMType.CTY getCTY() {
        return cty;
    }

    /**
     * Sets the value of the cty property.
     * 
     * @param value
     *     allowed object is
     *     {@link ITMType.CTY }
     *     
     */
    public void setCTY(ITMType.CTY value) {
        this.cty = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="ORG" type="{http://twm.ncs.gov.ng/formm/xsd}cod_2"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "org"
    })
    public static class CTY {

        @XmlElement(name = "ORG", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected Cod2 org;

        /**
         * Gets the value of the org property.
         * 
         * @return
         *     possible object is
         *     {@link Cod2 }
         *     
         */
        public Cod2 getORG() {
            return org;
        }

        /**
         * Sets the value of the org property.
         * 
         * @param value
         *     allowed object is
         *     {@link Cod2 }
         *     
         */
        public void setORG(Cod2 value) {
            this.org = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="VAL" type="{http://twm.ncs.gov.ng/formm/xsd}double"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "val"
    })
    public static class FOB {

        @XmlElement(name = "VAL", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String val;

        /**
         * Gets the value of the val property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getVAL() {
            return val;
        }

        /**
         * Sets the value of the val property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setVAL(String value) {
            this.val = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="NBR" type="{http://twm.ncs.gov.ng/formm/xsd}integer" minOccurs="0"/>
     *         &lt;element name="COD" type="{http://twm.ncs.gov.ng/formm/xsd}X2" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "nbr",
        "cod"
    })
    public static class PKG {

        @XmlElement(name = "NBR", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected Integer nbr;
        @XmlElement(name = "COD", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String cod;

        /**
         * Gets the value of the nbr property.
         * 
         * @return
         *     possible object is
         *     {@link Integer }
         *     
         */
        public Integer getNBR() {
            return nbr;
        }

        /**
         * Sets the value of the nbr property.
         * 
         * @param value
         *     allowed object is
         *     {@link Integer }
         *     
         */
        public void setNBR(Integer value) {
            this.nbr = value;
        }

        /**
         * Gets the value of the cod property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOD() {
            return cod;
        }

        /**
         * Sets the value of the cod property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOD(String value) {
            this.cod = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="PRI" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
     *         &lt;element name="COD" type="{http://twm.ncs.gov.ng/formm/xsd}X3" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "pri",
        "cod"
    })
    public static class UNT {

        @XmlElement(name = "PRI", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String pri;
        @XmlElement(name = "COD", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String cod;

        /**
         * Gets the value of the pri property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getPRI() {
            return pri;
        }

        /**
         * Sets the value of the pri property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setPRI(String value) {
            this.pri = value;
        }

        /**
         * Gets the value of the cod property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getCOD() {
            return cod;
        }

        /**
         * Sets the value of the cod property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setCOD(String value) {
            this.cod = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="GRS" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
     *         &lt;element name="NET" type="{http://twm.ncs.gov.ng/formm/xsd}double" minOccurs="0"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "grs",
        "net"
    })
    public static class WGT {

        @XmlElement(name = "GRS", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String grs;
        @XmlElement(name = "NET", namespace = "http://twm.ncs.gov.ng/formm/xsd")
        protected String net;

        /**
         * Gets the value of the grs property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getGRS() {
            return grs;
        }

        /**
         * Sets the value of the grs property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setGRS(String value) {
            this.grs = value;
        }

        /**
         * Gets the value of the net property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getNET() {
            return net;
        }

        /**
         * Sets the value of the net property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setNET(String value) {
            this.net = value;
        }

    }

}
