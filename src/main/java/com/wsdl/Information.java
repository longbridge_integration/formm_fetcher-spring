
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * Type of common personnal informations
 * 
 * <p>Java class for information complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="information">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NAM" type="{http://twm.ncs.gov.ng/formm/xsd}X70" minOccurs="0"/>
 *         &lt;element name="ADR" type="{http://twm.ncs.gov.ng/formm/xsd}X150" minOccurs="0"/>
 *         &lt;element name="CIT" type="{http://twm.ncs.gov.ng/formm/xsd}X35" minOccurs="0"/>
 *         &lt;element name="TEL" type="{http://twm.ncs.gov.ng/formm/xsd}X17" minOccurs="0"/>
 *         &lt;element name="FAX" type="{http://twm.ncs.gov.ng/formm/xsd}X17" minOccurs="0"/>
 *         &lt;element name="MAI" type="{http://twm.ncs.gov.ng/formm/xsd}X128" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "information", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "nam",
    "adr",
    "cit",
    "tel",
    "fax",
    "mai"
})
@XmlSeeAlso({
    BENType.class,
    APPType.class
})
public class Information {

    @XmlElement(name = "NAM", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String nam;
    @XmlElement(name = "ADR", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String adr;
    @XmlElement(name = "CIT", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String cit;
    @XmlElement(name = "TEL", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String tel;
    @XmlElement(name = "FAX", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String fax;
    @XmlElement(name = "MAI", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String mai;

    /**
     * Gets the value of the nam property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNAM() {
        return nam;
    }

    /**
     * Sets the value of the nam property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNAM(String value) {
        this.nam = value;
    }

    /**
     * Gets the value of the adr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getADR() {
        return adr;
    }

    /**
     * Sets the value of the adr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setADR(String value) {
        this.adr = value;
    }

    /**
     * Gets the value of the cit property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCIT() {
        return cit;
    }

    /**
     * Sets the value of the cit property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCIT(String value) {
        this.cit = value;
    }

    /**
     * Gets the value of the tel property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTEL() {
        return tel;
    }

    /**
     * Sets the value of the tel property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTEL(String value) {
        this.tel = value;
    }

    /**
     * Gets the value of the fax property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getFAX() {
        return fax;
    }

    /**
     * Sets the value of the fax property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setFAX(String value) {
        this.fax = value;
    }

    /**
     * Gets the value of the mai property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMAI() {
        return mai;
    }

    /**
     * Sets the value of the mai property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMAI(String value) {
        this.mai = value;
    }

}
