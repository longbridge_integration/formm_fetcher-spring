
package com.wsdl;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSeeAlso;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for ListResponse complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="ListResponse">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="formMs" type="{http://twm.ncs.gov.ng/formm/xsd}formMType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="formMsAlreadyDownloaded" type="{http://twm.ncs.gov.ng/formm/xsd}formMAlreadyDownloadedType" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "ListResponse", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "formMs",
    "formMsAlreadyDownloaded"
})
@XmlSeeAlso({
    GetByPeriodResponse.class,
    GetByCriteriaResponse.class,
    GetByReferenceResponse.class
})
public class ListResponse {

    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected List<FormMType> formMs;
    @XmlElement(namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected List<FormMAlreadyDownloadedType> formMsAlreadyDownloaded;

    /**
     * Gets the value of the formMs property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formMs property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormMs().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormMType }
     * 
     * 
     */
    public List<FormMType> getFormMs() {
        if (formMs == null) {
            formMs = new ArrayList<FormMType>();
        }
        return this.formMs;
    }

    /**
     * Gets the value of the formMsAlreadyDownloaded property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the formMsAlreadyDownloaded property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getFormMsAlreadyDownloaded().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link FormMAlreadyDownloadedType }
     * 
     * 
     */
    public List<FormMAlreadyDownloadedType> getFormMsAlreadyDownloaded() {
        if (formMsAlreadyDownloaded == null) {
            formMsAlreadyDownloaded = new ArrayList<FormMAlreadyDownloadedType>();
        }
        return this.formMsAlreadyDownloaded;
    }

}
