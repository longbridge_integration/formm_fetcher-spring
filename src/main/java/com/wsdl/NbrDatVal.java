
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for nbr_dat_val complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="nbr_dat_val">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="NBR" type="{http://twm.ncs.gov.ng/formm/xsd}X17"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}DAT"/>
 *         &lt;group ref="{http://twm.ncs.gov.ng/formm/xsd}VAL"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "nbr_dat_val", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "nbr",
    "dat",
    "val"
})
public class NbrDatVal {

    @XmlElement(name = "NBR", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String nbr;
    @XmlElement(name = "DAT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String dat;
    @XmlElement(name = "VAL", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String val;

    /**
     * Gets the value of the nbr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNBR() {
        return nbr;
    }

    /**
     * Sets the value of the nbr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNBR(String value) {
        this.nbr = value;
    }

    /**
     * Gets the value of the dat property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDAT() {
        return dat;
    }

    /**
     * Sets the value of the dat property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDAT(String value) {
        this.dat = value;
    }

    /**
     * Gets the value of the val property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVAL() {
        return val;
    }

    /**
     * Sets the value of the val property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVAL(String value) {
        this.val = value;
    }

}
