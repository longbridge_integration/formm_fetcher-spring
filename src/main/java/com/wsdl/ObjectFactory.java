
package com.wsdl;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.wsdl package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.wsdl
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GDSType }
     * 
     */
    public GDSType createGDSType() {
        return new GDSType();
    }

    /**
     * Create an instance of {@link GDSType.VAL }
     * 
     */
    public GDSType.VAL createGDSTypeVAL() {
        return new GDSType.VAL();
    }

    /**
     * Create an instance of {@link ITMType }
     * 
     */
    public ITMType createITMType() {
        return new ITMType();
    }

    /**
     * Create an instance of {@link TSPType }
     * 
     */
    public TSPType createTSPType() {
        return new TSPType();
    }

    /**
     * Create an instance of {@link ENDType }
     * 
     */
    public ENDType createENDType() {
        return new ENDType();
    }

    /**
     * Create an instance of {@link FormMType }
     * 
     */
    public FormMType createFormMType() {
        return new FormMType();
    }

    /**
     * Create an instance of {@link FormMServiceFault }
     * 
     */
    public FormMServiceFault createFormMServiceFault() {
        return new FormMServiceFault();
    }

    /**
     * Create an instance of {@link GetByReferenceResponse }
     * 
     */
    public GetByReferenceResponse createGetByReferenceResponse() {
        return new GetByReferenceResponse();
    }

    /**
     * Create an instance of {@link ListResponse }
     * 
     */
    public ListResponse createListResponse() {
        return new ListResponse();
    }

    /**
     * Create an instance of {@link FormMAlreadyDownloadedType }
     * 
     */
    public FormMAlreadyDownloadedType createFormMAlreadyDownloadedType() {
        return new FormMAlreadyDownloadedType();
    }

    /**
     * Create an instance of {@link GetByCriteria }
     * 
     */
    public GetByCriteria createGetByCriteria() {
        return new GetByCriteria();
    }

    /**
     * Create an instance of {@link Cod3 }
     * 
     */
    public Cod3 createCod3() {
        return new Cod3();
    }

    /**
     * Create an instance of {@link Cod7 }
     * 
     */
    public Cod7 createCod7() {
        return new Cod7();
    }

    /**
     * Create an instance of {@link Cod2 }
     * 
     */
    public Cod2 createCod2() {
        return new Cod2();
    }

    /**
     * Create an instance of {@link Cod5 }
     * 
     */
    public Cod5 createCod5() {
        return new Cod5();
    }

    /**
     * Create an instance of {@link GetByReference }
     * 
     */
    public GetByReference createGetByReference() {
        return new GetByReference();
    }

    /**
     * Create an instance of {@link GetByCriteriaResponse }
     * 
     */
    public GetByCriteriaResponse createGetByCriteriaResponse() {
        return new GetByCriteriaResponse();
    }

    /**
     * Create an instance of {@link GetByPeriod }
     * 
     */
    public GetByPeriod createGetByPeriod() {
        return new GetByPeriod();
    }

    /**
     * Create an instance of {@link GetByPeriodResponse }
     * 
     */
    public GetByPeriodResponse createGetByPeriodResponse() {
        return new GetByPeriodResponse();
    }

    /**
     * Create an instance of {@link SupportInformationType }
     * 
     */
    public SupportInformationType createSupportInformationType() {
        return new SupportInformationType();
    }

    /**
     * Create an instance of {@link NbrDatVal }
     * 
     */
    public NbrDatVal createNbrDatVal() {
        return new NbrDatVal();
    }

    /**
     * Create an instance of {@link Cod1 }
     * 
     */
    public Cod1 createCod1() {
        return new Cod1();
    }

    /**
     * Create an instance of {@link Codnam3 }
     * 
     */
    public Codnam3 createCodnam3() {
        return new Codnam3();
    }

    /**
     * Create an instance of {@link Val }
     * 
     */
    public Val createVal() {
        return new Val();
    }

    /**
     * Create an instance of {@link Codnam17 }
     * 
     */
    public Codnam17 createCodnam17() {
        return new Codnam17();
    }

    /**
     * Create an instance of {@link Cod1015 }
     * 
     */
    public Cod1015 createCod1015() {
        return new Cod1015();
    }

    /**
     * Create an instance of {@link BENType }
     * 
     */
    public BENType createBENType() {
        return new BENType();
    }

    /**
     * Create an instance of {@link APPType }
     * 
     */
    public APPType createAPPType() {
        return new APPType();
    }

    /**
     * Create an instance of {@link INFType }
     * 
     */
    public INFType createINFType() {
        return new INFType();
    }

    /**
     * Create an instance of {@link Flg }
     * 
     */
    public Flg createFlg() {
        return new Flg();
    }

    /**
     * Create an instance of {@link AttachmentType }
     * 
     */
    public AttachmentType createAttachmentType() {
        return new AttachmentType();
    }

    /**
     * Create an instance of {@link Cod15 }
     * 
     */
    public Cod15 createCod15() {
        return new Cod15();
    }

    /**
     * Create an instance of {@link Cod17 }
     * 
     */
    public Cod17 createCod17() {
        return new Cod17();
    }

    /**
     * Create an instance of {@link Information }
     * 
     */
    public Information createInformation() {
        return new Information();
    }

    /**
     * Create an instance of {@link GDSType.GEN }
     * 
     */
    public GDSType.GEN createGDSTypeGEN() {
        return new GDSType.GEN();
    }

    /**
     * Create an instance of {@link GDSType.PRO }
     * 
     */
    public GDSType.PRO createGDSTypePRO() {
        return new GDSType.PRO();
    }

    /**
     * Create an instance of {@link GDSType.PAY }
     * 
     */
    public GDSType.PAY createGDSTypePAY() {
        return new GDSType.PAY();
    }

    /**
     * Create an instance of {@link GDSType.CTY }
     * 
     */
    public GDSType.CTY createGDSTypeCTY() {
        return new GDSType.CTY();
    }

    /**
     * Create an instance of {@link GDSType.VAL.TCF }
     * 
     */
    public GDSType.VAL.TCF createGDSTypeVALTCF() {
        return new GDSType.VAL.TCF();
    }

    /**
     * Create an instance of {@link ITMType.PKG }
     * 
     */
    public ITMType.PKG createITMTypePKG() {
        return new ITMType.PKG();
    }

    /**
     * Create an instance of {@link ITMType.FOB }
     * 
     */
    public ITMType.FOB createITMTypeFOB() {
        return new ITMType.FOB();
    }

    /**
     * Create an instance of {@link ITMType.UNT }
     * 
     */
    public ITMType.UNT createITMTypeUNT() {
        return new ITMType.UNT();
    }

    /**
     * Create an instance of {@link ITMType.WGT }
     * 
     */
    public ITMType.WGT createITMTypeWGT() {
        return new ITMType.WGT();
    }

    /**
     * Create an instance of {@link ITMType.CTY }
     * 
     */
    public ITMType.CTY createITMTypeCTY() {
        return new ITMType.CTY();
    }

    /**
     * Create an instance of {@link TSPType.AIR }
     * 
     */
    public TSPType.AIR createTSPTypeAIR() {
        return new TSPType.AIR();
    }

    /**
     * Create an instance of {@link TSPType.SHP }
     * 
     */
    public TSPType.SHP createTSPTypeSHP() {
        return new TSPType.SHP();
    }

    /**
     * Create an instance of {@link ENDType.APP }
     * 
     */
    public ENDType.APP createENDTypeAPP() {
        return new ENDType.APP();
    }

    /**
     * Create an instance of {@link ENDType.AUT }
     * 
     */
    public ENDType.AUT createENDTypeAUT() {
        return new ENDType.AUT();
    }

    /**
     * Create an instance of {@link FormMType.BNK }
     * 
     */
    public FormMType.BNK createFormMTypeBNK() {
        return new FormMType.BNK();
    }

}
