
package com.wsdl;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for PFXType.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="PFXType">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BA"/>
 *     &lt;enumeration value="CB"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "PFXType", namespace = "http://twm.ncs.gov.ng/formm/xsd")
@XmlEnum
public enum PFXType {

    BA,
    CB;

    public String value() {
        return name();
    }

    public static PFXType fromValue(String v) {
        return valueOf(v);
    }

}
