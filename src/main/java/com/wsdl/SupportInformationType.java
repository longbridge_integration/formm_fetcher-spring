
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;
import javax.xml.datatype.XMLGregorianCalendar;


/**
 * <p>Java class for SupportInformationType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="SupportInformationType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="DAT" type="{http://www.w3.org/2001/XMLSchema}dateTime"/>
 *         &lt;element name="USR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="OPR" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MSG" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SupportInformationType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "dat",
    "usr",
    "opr",
    "msg"
})
public class SupportInformationType {

    @XmlElement(name = "DAT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    @XmlSchemaType(name = "dateTime")
    protected XMLGregorianCalendar dat;
    @XmlElement(name = "USR", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String usr;
    @XmlElement(name = "OPR", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
    protected String opr;
    @XmlElement(name = "MSG", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String msg;

    /**
     * Gets the value of the dat property.
     * 
     * @return
     *     possible object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public XMLGregorianCalendar getDAT() {
        return dat;
    }

    /**
     * Sets the value of the dat property.
     * 
     * @param value
     *     allowed object is
     *     {@link XMLGregorianCalendar }
     *     
     */
    public void setDAT(XMLGregorianCalendar value) {
        this.dat = value;
    }

    /**
     * Gets the value of the usr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getUSR() {
        return usr;
    }

    /**
     * Sets the value of the usr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setUSR(String value) {
        this.usr = value;
    }

    /**
     * Gets the value of the opr property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getOPR() {
        return opr;
    }

    /**
     * Sets the value of the opr property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setOPR(String value) {
        this.opr = value;
    }

    /**
     * Gets the value of the msg property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMSG() {
        return msg;
    }

    /**
     * Sets the value of the msg property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMSG(String value) {
        this.msg = value;
    }

}
