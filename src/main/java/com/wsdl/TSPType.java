
package com.wsdl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for TSPType complex type.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * 
 * <pre>
 * &lt;complexType name="TSPType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="AIR" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="TCK" type="{http://twm.ncs.gov.ng/formm/xsd}X17"/>
 *                   &lt;element name="LIN" type="{http://twm.ncs.gov.ng/formm/xsd}X35"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="SHP" minOccurs="0">
 *           &lt;complexType>
 *             &lt;complexContent>
 *               &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *                 &lt;sequence>
 *                   &lt;element name="DAT" type="{http://twm.ncs.gov.ng/formm/xsd}date"/>
 *                 &lt;/sequence>
 *               &lt;/restriction>
 *             &lt;/complexContent>
 *           &lt;/complexType>
 *         &lt;/element>
 *         &lt;element name="RUT" type="{http://twm.ncs.gov.ng/formm/xsd}X35" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "TSPType", namespace = "http://twm.ncs.gov.ng/formm/xsd", propOrder = {
    "air",
    "shp",
    "rut"
})
public class TSPType {

    @XmlElement(name = "AIR", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected TSPType.AIR air;
    @XmlElement(name = "SHP", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected TSPType.SHP shp;
    @XmlElement(name = "RUT", namespace = "http://twm.ncs.gov.ng/formm/xsd")
    protected String rut;

    /**
     * Gets the value of the air property.
     * 
     * @return
     *     possible object is
     *     {@link TSPType.AIR }
     *     
     */
    public TSPType.AIR getAIR() {
        return air;
    }

    /**
     * Sets the value of the air property.
     * 
     * @param value
     *     allowed object is
     *     {@link TSPType.AIR }
     *     
     */
    public void setAIR(TSPType.AIR value) {
        this.air = value;
    }

    /**
     * Gets the value of the shp property.
     * 
     * @return
     *     possible object is
     *     {@link TSPType.SHP }
     *     
     */
    public TSPType.SHP getSHP() {
        return shp;
    }

    /**
     * Sets the value of the shp property.
     * 
     * @param value
     *     allowed object is
     *     {@link TSPType.SHP }
     *     
     */
    public void setSHP(TSPType.SHP value) {
        this.shp = value;
    }

    /**
     * Gets the value of the rut property.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRUT() {
        return rut;
    }

    /**
     * Sets the value of the rut property.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRUT(String value) {
        this.rut = value;
    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="TCK" type="{http://twm.ncs.gov.ng/formm/xsd}X17"/>
     *         &lt;element name="LIN" type="{http://twm.ncs.gov.ng/formm/xsd}X35"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "tck",
        "lin"
    })
    public static class AIR {

        @XmlElement(name = "TCK", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String tck;
        @XmlElement(name = "LIN", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String lin;

        /**
         * Gets the value of the tck property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getTCK() {
            return tck;
        }

        /**
         * Sets the value of the tck property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setTCK(String value) {
            this.tck = value;
        }

        /**
         * Gets the value of the lin property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getLIN() {
            return lin;
        }

        /**
         * Sets the value of the lin property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setLIN(String value) {
            this.lin = value;
        }

    }


    /**
     * <p>Java class for anonymous complex type.
     * 
     * <p>The following schema fragment specifies the expected content contained within this class.
     * 
     * <pre>
     * &lt;complexType>
     *   &lt;complexContent>
     *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
     *       &lt;sequence>
     *         &lt;element name="DAT" type="{http://twm.ncs.gov.ng/formm/xsd}date"/>
     *       &lt;/sequence>
     *     &lt;/restriction>
     *   &lt;/complexContent>
     * &lt;/complexType>
     * </pre>
     * 
     * 
     */
    @XmlAccessorType(XmlAccessType.FIELD)
    @XmlType(name = "", propOrder = {
        "dat"
    })
    public static class SHP {

        @XmlElement(name = "DAT", namespace = "http://twm.ncs.gov.ng/formm/xsd", required = true)
        protected String dat;

        /**
         * Gets the value of the dat property.
         * 
         * @return
         *     possible object is
         *     {@link String }
         *     
         */
        public String getDAT() {
            return dat;
        }

        /**
         * Sets the value of the dat property.
         * 
         * @param value
         *     allowed object is
         *     {@link String }
         *     
         */
        public void setDAT(String value) {
            this.dat = value;
        }

    }

}
