
package com;

import com.longbridge.formmfetcher.Entities.AppProperty;
import com.longbridge.formmfetcher.Entities.FormMAttachment;
import com.longbridge.formmfetcher.controllers.Helper;
import com.longbridge.formmfetcher.model.ServiceResponse;
import com.longbridge.formmfetcher.repository.*;
import com.wsdl.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.client.core.SoapActionCallback;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.StringWriter;
import java.net.Authenticator;
import java.net.PasswordAuthentication;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import org.apache.log4j.Logger;
public class wsdlClient extends WebServiceGatewaySupport {

    protected static final Logger logger = Logger.getLogger(wsdlClient.class);


    protected static AppPropRepository appPropRepository;
    protected static FormMRepository formMRepository;
    protected static FormMAttchRepository formMAttchRepository;
    protected static FinFormMItmsRepository finFormMItmsRepository;
    protected static FinFormMMasterRepository finFormMMasterRepository;
    protected static FinFormMMasterValRepository finFormMMasterValRepository;
    protected static FinFormMMasterAuditRepository finFormMMasterAuditRepository;
    protected static RefCodeRepository refCodeRepository;

    protected static final SimpleDateFormat formatter1 = new SimpleDateFormat("yyyy-MM-dd");
    protected static final SimpleDateFormat formatter2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss.0");
    protected static final SimpleDateFormat formatter3 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");


    @Autowired
    public void setFinFormMItmsRepository(AppPropRepository appPropRepository) {
        this.appPropRepository = appPropRepository;
    }
    @Autowired
    public void setFinFormMItmsRepository(FormMRepository formMRepository) {
        this.formMRepository = formMRepository;
    }
    @Autowired
    public void setFinFormMItmsRepository(FinFormMItmsRepository finFormMItmsRepository) {
        this.finFormMItmsRepository = finFormMItmsRepository;
    }
    @Autowired
    public void setFinFormMMasterRepository(FinFormMMasterRepository finFormMMasterRepository) {
        this.finFormMMasterRepository = finFormMMasterRepository;
    }
    @Autowired
    public void setFinFormMMasterValRepository(FinFormMMasterValRepository finFormMMasterValRepository) {
        this.finFormMMasterValRepository = finFormMMasterValRepository;
    }
    @Autowired
    public void setFinFormMMasterAuditRepository(FinFormMMasterAuditRepository finFormMMasterAuditRepository) {
        this.finFormMMasterAuditRepository = finFormMMasterAuditRepository;
    }
    @Autowired
    public void setFormMAttchRepository(FormMAttchRepository formMAttchRepository) {
        this.formMAttchRepository = formMAttchRepository;
    }
    @Autowired
    public void setRefCodeRepository(RefCodeRepository refCodeRepository) {
        this.refCodeRepository = refCodeRepository;
    }

    private String validateResp = "VformM found";
    private GetByCriteria request;
    private GetByCriteriaResponse response;
    JAXBContext jaxbContext;
    Marshaller jaxbMarshaller;
    ArrayList<FormMType> formM ;
    ArrayList<FormMAlreadyDownloadedType> formMalreadyDownloadType;
    ServiceResponse serviceResponse;
    private GregorianCalendar cal;
    private GregorianCalendar cal2;
    private List<FormMAttachment> checker;
    private java.io.StringWriter sw;





    //@Scheduled(fixedRate = 20000*1)
    public String getQuote() {

        Authenticator myAuth = new Authenticator() {
            @Override
            protected PasswordAuthentication getPasswordAuthentication() {
                return new PasswordAuthentication("BNK-559", "11nxkrbl".toCharArray());//password
            }
        };
        Authenticator.setDefault(myAuth);
        AppProperty appProperty = appPropRepository.findByBankCode("BNK-559");
        String BankCode = "559";
        Timestamp startDateTS;
        Timestamp nextFetchDate;
        Calendar calendar;
        String endDate;
        String startDate;
        String fileGenStatus;


        if(appProperty!=null) {
            int fetchInterval = appProperty.getFormFetchInterval();
            if (appProperty.getLastPersistDate() == null) {
                nextFetchDate = appProperty.getFirstPersistDate();
            }else{
                nextFetchDate = appProperty.getLastPersistDate();
            }
            boolean excludedocAttched =  appProperty.getExcludeAttchmnt().toUpperCase().equals("TRUE")?true:false;
            Timestamp sysDate = (new Timestamp(new Date().getTime()));
            while(nextFetchDate.getTime() <= sysDate.getTime()){
                calendar = Calendar.getInstance();
                calendar.setTime(nextFetchDate);
                calendar.add(Calendar.MINUTE,-fetchInterval);
                startDateTS = new Timestamp(calendar.getTime().getTime());
                startDate = formatter2.format(startDateTS);//"2017-03-23 00:00:00.0";//
                endDate = formatter2.format(nextFetchDate);//"2017-03-24 23:59:59.0";//
                logger.info("*****Web Fountain Request for Date :"+startDate+"   to  "+endDate+"********\n");
                serviceResponse = fireRequest(startDate,endDate,BankCode,excludedocAttched);
                if(serviceResponse.isSuccessFull()){
                    appProperty.setStatus("SUCCESS");
                    calendar.setTime(nextFetchDate);
                    calendar.add(Calendar.MINUTE,fetchInterval);
                    nextFetchDate = new Timestamp(calendar.getTime().getTime());
                    appProperty.setLastPersistDate(nextFetchDate);
                    appPropRepository.save(appProperty);
                }else{
                    appProperty.setStatus("FAILED");
                    appProperty.setLastPersistDate(nextFetchDate);
                    appProperty.setLastResponse(serviceResponse.getRespMessage());
                    appPropRepository.save(appProperty);
                    break;
                }
                logger.info("*****Web Fountain Response for Date :"+startDate+"   to  "+endDate+" *******\n"
                        +serviceResponse.getRespMessage());
                appProperty.setLastResponse(serviceResponse.getRespMessage());
                fileGenStatus = generateDoc();
                fileGenStatus = "Document Generation "+ fileGenStatus;
                appProperty.setDocGenStatus(fileGenStatus);
                appPropRepository.save(appProperty);
            }
            if(nextFetchDate.getTime() > sysDate.getTime()){
                logger.error("*****FAILED: Future Date used as NextFetch  Date"+nextFetchDate.getTime()+" *******\n");
                appProperty.setStatus("FAILED");
                appProperty.setLastResponse("FAILED: Future Date used as Start Date");
                return "FAILED: Future Date used as Start Date";
            }
        }else{
            return "FAILED: App Property Table not Found";
        }
        return "EOF";
    }

    public ServiceResponse fireRequest(String startDate,String endDate,String BankCode,boolean docAttched) {
        String respMsg="";
        serviceResponse = new ServiceResponse();
//        logger.info("********************** Firing Request for Forms That are Validated *********************************");
//        String formStatus = "VALIDATED";
//        respMsg = respMsg+"|"+parseRequest(startDate,endDate,BankCode,docAttched,formStatus)+"|";
//         if(!respMsg.contains("formM found")){
//             serviceResponse.setSuccessFull(false);
//             serviceResponse.setRespMessage(respMsg);
//             return serviceResponse;
//         }
         logger.info("********************** Firing Request for Forms That are Registered *********************************");
        String formStatus = "REGISTERED";
        respMsg = respMsg+"|"+parseRequest(startDate, endDate, BankCode, docAttched, formStatus);
        if(!respMsg.contains("formM found")){
            serviceResponse.setSuccessFull(false);
            serviceResponse.setRespMessage(respMsg);
            return serviceResponse;
        }
//        logger.info("********************** Firing Request for Forms That are Cancelled *********************************");
//         formStatus = "CANCELLED";
//        respMsg = respMsg+"|"+parseRequest(startDate,endDate,BankCode,docAttched,formStatus)+"|";
//        if(!respMsg.contains("formM found")){
//            serviceResponse.setSuccessFull(false);
//            serviceResponse.setRespMessage(respMsg);
//            return serviceResponse;
//        }
//
//        logger.info("********************** Firing Request for Forms That are Submitted *********************************");
//        formStatus = "SUBMITTED";
//        respMsg =  respMsg+"|"+parseRequest(startDate,endDate,BankCode,docAttched,formStatus)+"|";
//        if(!respMsg.contains("formM found")){
//            serviceResponse.setSuccessFull(false);
//            serviceResponse.setRespMessage(respMsg);
//            return serviceResponse;
//        }
//
//        logger.info("********************** Firing Request for Forms That are Queried *********************************");
//        formStatus = "QUERY";
//        respMsg = respMsg+"|"+parseRequest(startDate,endDate,BankCode,docAttched,formStatus)+"|";
//        if(!respMsg.contains("formM found")){
//            serviceResponse.setSuccessFull(false);
//            serviceResponse.setRespMessage(respMsg);
//            return serviceResponse;
//        }
//
//        logger.info("********************** Firing Request for Forms That are Rar_Locked *********************************");
//        formStatus = "RAR_LOCKED";
//        respMsg = respMsg+"|"+parseRequest(startDate,endDate,BankCode,docAttched,formStatus)+"|";
//        if(!respMsg.contains("formM found")){
//            serviceResponse.setSuccessFull(false);
//            serviceResponse.setRespMessage(respMsg);
//            return serviceResponse;
//        }
        serviceResponse.setSuccessFull(true);
        serviceResponse.setRespMessage(respMsg);
        return serviceResponse;
    }

    private String parseRequest(String startDate,String endDate,String BankCode,boolean attchDocs,String formStatus)   {
        cal = new GregorianCalendar();
        cal2 = new GregorianCalendar();
        request = new GetByCriteria();
        try {
            cal.setTime(formatter2.parse( startDate ));
            cal2.setTime(formatter2.parse( endDate));
        } catch (ParseException e) {
            logger.error("There was an error in converting the TimeStamp to Calendar Format");
            e.printStackTrace();
        }
        try {
            request.setFromDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal));
            request.setToDate(DatatypeFactory.newInstance().newXMLGregorianCalendar(cal2));
        } catch (DatatypeConfigurationException e) {
            logger.error("There was an error in converting the LocalDate object into xml");
            e.printStackTrace();
        }
        Cod3 cod3 = new Cod3();
        cod3.setCOD(BankCode);
        request.setBnkCode(cod3);
        request.setExcludeAttachment(attchDocs);
        request.setCtyOrigin(null);request.setCtySupply(null);request.setCuoCode(null);request.setCurCode(null);request.setScaCode(null);
        if(formStatus.equals("VALIDATED")){
            request.setStatus(StatusType.VALIDATED);
        }
        else if(formStatus.equals("REGISTERED")){
            request.setStatus(StatusType.REGISTERED);
        }else if(formStatus.equals("CANCELLED")){
            request.setStatus(StatusType.CANCELLED);
        }else if(formStatus.equals("SUBMITTED")){
            request.setStatus(StatusType.SUBMITTED);
        }else if(formStatus.equals("QUERY")){
            request.setStatus(StatusType.QUERY);
        }else{
            request.setStatus(StatusType.RAR_LOCKED);
        }


        try {
            jaxbContext = JAXBContext.newInstance(GetByCriteria.class);
            jaxbMarshaller = jaxbContext.createMarshaller();
            jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
            sw = new StringWriter();
            jaxbMarshaller.marshal(request, sw);
            logger.info("Request XML sent: \n "+sw.toString());

        } catch (JAXBException e) {
            e.printStackTrace();
        }
//        System.setProperty("https.proxyHost", "172.27.15.74");
//        System.setProperty("https.proxyPort", "80");
        //Sending Request
        try{response = (GetByCriteriaResponse) getWebServiceTemplate()
                .marshalSendAndReceive("https://app.ncs.gov.ng/axis2/services/FormMService",
                        request,new SoapActionCallback("") );
        }
        catch(Exception e){
            if(!e.toString().contains("formM found")){
                 validateResp = e.toString();
                 return validateResp;
             }else{
                 validateResp = "formM found";
             }
            logger.error("Error is : \n "+validateResp);
        }
        if(response!=null){
            try {
                jaxbContext = JAXBContext.newInstance(GetByCriteriaResponse.class);
                jaxbMarshaller = jaxbContext.createMarshaller();
                jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
                sw = new StringWriter();
                jaxbMarshaller.marshal(response,sw);
                logger.info("Response XML Recieved : \n "+sw.toString());
            } catch (JAXBException e) {
                e.printStackTrace();
            }
            //Capture Response from Service
            formM = (ArrayList<FormMType>) response.getFormMs();
            formMalreadyDownloadType  = (ArrayList<FormMAlreadyDownloadedType>) response.getFormMsAlreadyDownloaded();
            for(FormMType formMType:formM){
                Helper helper = new Helper();
                helper.generateEntities(formMType,formStatus);
                logger.info("*****************FormMs****************************");
            }
            for(FormMAlreadyDownloadedType formMDownloaded:formMalreadyDownloadType){
                Helper helper = new Helper();
                helper.generateEntities(formMDownloaded,formStatus);
                logger.info("*****************FormMsAlreadyDownloaded****************************");
            }
            validateResp = "formM found";
        }
        return validateResp;

    }

    public String generateDoc() {
        checker = formMAttchRepository.findByIsDocGenerated("N");
        String docGenStatus = "No Docs Generated";
        for(FormMAttachment cont : checker){
            byte[] bytepushed = cont.getAtdBin();
            Boolean dirCreated = false;
            if(bytepushed!=null){
                
                String fileName = cont.getAtdType().replace(" ","").trim();
                String fileIndex = cont.getAtdRef().replace("/","_").replace(" ","").trim();
                String fileType = cont.getAtdBinTyp();
                String fileDirectory = "C://User//Public//Documents//"+cont.getFormMNbr()+"//"+fileName+"//";
                File fileDir = new File(fileDirectory);
                if(!fileDir.exists()){
                    if(fileDir.mkdirs()){;
                        dirCreated = true;
                    }
                }
                if(fileDir.exists()||dirCreated){
                    try {
                        String fileName2 = fileDirectory+fileIndex+"."+fileType;
                        System.out.println(fileName2);
                        FileOutputStream fop = new FileOutputStream(new File(fileName2));
                        fop.write(bytepushed);
                        fop.flush();
                        fop.close();
                        cont.setIsDocGenerated("Y");
                        formMAttchRepository.save(cont);
                        System.out.println("Saved");
                    } catch (IOException e) {
                        e.printStackTrace();
                        docGenStatus = "FAILED: "+e;
                        break;
                    }
                }
                docGenStatus = "SUCCESS";
            }else{
                docGenStatus = "FAILED: Found Empty Byte Value";
                break;
            }
        }
        return docGenStatus;
    }
}

